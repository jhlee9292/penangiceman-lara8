<ul class="nav flex-column">
    <li class="nav-item">
        <a class="nav-link active" href="#"><i class="fi-rs-settings-sliders mr-10"></i>Dashboard</a>
    </li>
	<li class="nav-item">
        <a class="nav-link" href="#"><i class="fi fi-rs-label mr-10"></i>My Vouchers</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"><i class="fi-rs-shopping-bag mr-10"></i>Orders and Track</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"><i class="fi-rs-marker mr-10"></i>My Address</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"><i class="fi-rs-user mr-10"></i>Account details</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{ route('logout') }}" 
			onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
			<i class="fi-rs-sign-out mr-10"></i>
			Logout
		</a>
		<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
			@csrf
		</form>
    </li>
</ul>
