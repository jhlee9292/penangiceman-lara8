{{-- My Account Page Layout --}}

@extends("layouts.main")

@section('content')
    
	<div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="{{ route('home.index') }}" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> My Account
            </div>
        </div>
    </div>


    <div class="page-content pt-150 pb-150">
        <div class="container">
            <div class="row">
                <div class="col-lg-10 m-auto">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="dashboard-menu">

                                {{-- my account page sidebar --}}
                                @include('myaccount.partials.sidebar')

                            </div>
                        </div>



                        <div class="col-md-9">


                            @yield('my-account-content')


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
