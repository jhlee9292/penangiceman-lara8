@extends('myaccount.layouts.index')


@section('my-account-content')
    <div class="tab-content account dashboard-content pl-50">
        <div class="tab-pane fade active show" id="dashboard" role="tabpanel" aria-labelledby="dashboard-tab">
            <div class="card">

                <div class="card-header">
                    <h3 class="mb-0">Welcome {{ Auth::user()->name }}!</h3>
                </div>

                <div class="card-body">
                    <p>
                        From your account dashboard. you can easily check &amp; view your
                        <a href="#" class="text-underline">recent orders</a>, manage your <br>
                        <a href="#" class="text-underline">shipping and billing addresses</a> and 
						<a href="#" class="text-underline">edit your account details.</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
