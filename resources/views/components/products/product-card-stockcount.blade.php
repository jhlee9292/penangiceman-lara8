<div class="product-cart-wrap">

    <div class="product-img-action-wrap">
        <div class="product-img product-img-zoom">
            <a href="{{ route('home.index') }}">
                <img class="default-img" src="{{ asset('assets/imgs/shop/product-1-1.jpg') }}" alt="" />
                <img class="hover-img" src="{{ asset('assets/imgs/shop/product-1-2.jpg') }}" alt="" />
            </a>
        </div>
        <div class="product-badges product-badges-position product-badges-mrg">
            <span class="hot">Save 15%</span>
        </div>
    </div>

    <div class="product-content-wrap">
        <div class="product-category">
            <a href="{{ route('home.index') }}">Lazy Chicken</a>
        </div>
        <h2 class="two-lines"><a href="{{ route('home.index') }}">Seeds of Change Organic Quinoa, Brown</a></h2>
        <div class="product-rate d-inline-block">
            <div class="product-rating" style="width: 80%"></div>
        </div>
        <div class="product-price mt-10">
            <span>RM 238.85 </span>
            <span class="old-price">RM 245.8</span>
        </div>
        <div class="sold mt-15 mb-15">
            <div class="progress mb-5">
                <div class="progress-bar" role="progressbar" style="width: 50%" aria-valuemin="0" aria-valuemax="100">
                </div>
            </div>
            <span class="font-xs text-heading"> Sold: 90/120</span>
        </div>
        <a href="{{ route('home.index') }}" class="btn w-100 hover-up btn-min-padding"><i class="fi-rs-shopping-cart mr-5"></i>Add To Cart</a>
    </div>

</div>
