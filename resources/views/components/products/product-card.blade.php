{{-- Product Card --}}
<div class="product-cart-wrap mb-30 wow animate__animated animate__fadeIn" data-wow-delay=".1s">
    <div class="product-img-action-wrap">
        <div class="product-img product-img-zoom">
            <a href="{{ route('products.single.index') }}">
                <img class="default-img" src="{{ asset('assets/imgs/shop/product-1-1.jpg') }}" alt="" />
                <img class="hover-img" src="{{ asset('assets/imgs/shop/product-1-2.jpg') }}" alt="" />
            </a>
        </div>
        <div class="product-badges product-badges-position product-badges-mrg">
            <span class="hot">Hot</span>
            {{-- <span class="new">Save 35%</span> --}}
        </div>
    </div>
    <div class="product-content-wrap">
        <div class="product-category">
            <a href="{{ route('home.index') }}">Snack</a>
        </div>
        <h2 class="two-lines">
            <a href="{{ route('home.index') }}">Seeds of Change Organic Quinoa, Brown, & Red Rice</a>
        </h2>
        <div class="product-rate-cover">
            <div class="product-rate d-inline-block">
                <div class="product-rating" style="width: 90%"></div>
            </div>
            <span class="font-small ml-5 text-muted"> (4.0)</span>
        </div>
        <div>
            <span class="font-small text-muted">By <a href="{{ route('home.index') }}">PenangIceman</a></span>
        </div>
        <div class="product-card-bottom">
            <div class="product-price">
                <span>RM 28.80</span>
                <span class="old-price">RM 32.90</span>
            </div>
            <div class="add-cart">
                <a class="add" href={{ route('home.index') }}"><i class="fi-rs-shopping-cart mr-5"></i>Add </a>
            </div>
        </div>
    </div>
</div>
{{-- <!--end Product Card--> --}}
