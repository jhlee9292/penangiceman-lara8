<article class="row align-items-center hover-up">
    <figure class="col-md-4 mb-0">
        <a href="{{ route('home.index') }}"><img src="{{ asset('assets/imgs/shop/thumbnail-1.jpg') }}" alt="" /></a>
    </figure>
    <div class="col-md-8 mb-0">
        <h6 class="two-lines">
            <a href="{{ route('home.index') }}">Nestle Original Coffee-Mate Coffee Creamer</a>
        </h6>
        <div class="product-rate-cover">
            <div class="product-rate d-inline-block">
                <div class="product-rating" style="width: 90%"></div>
            </div>
            <span class="font-small ml-5 text-muted"> (4.0)</span>
        </div>
        <div class="product-price">
            <span>RM 32.85</span>
            <span class="old-price">RM 33.80</span>
        </div>
    </div>
</article>
