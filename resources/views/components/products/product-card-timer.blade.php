{{-- single product card :: timer --}}
<div class="product-cart-wrap style-2 wow animate__animated animate__fadeInUp" data-wow-delay="0">
    <div class="product-img-action-wrap">
        <div class="product-img">
            <a href="{{ route('home.index') }}">
                <img src="{{ asset('assets/imgs/banner/banner-5.png') }}" alt="" />
            </a>
        </div>
    </div>
    <div class="product-content-wrap">
        <div class="deals-countdown-wrap">
            <div class="deals-countdown" data-countdown="2022/03/31 00:00:00"></div>
        </div>
        <div class="deals-content">
            <h2 class="two-lines">
                <a href="{{ route('home.index') }}">Seeds of Change Organic Quinoa, Brown, & Red Rice</a>
            </h2>
            <div class="product-rate-cover">
                <div class="product-rate d-inline-block">
                    <div class="product-rating" style="width: 90%"></div>
                </div>
                <span class="font-small ml-5 text-muted"> (4.0)</span>
            </div>
            <div>
                <span class="font-small text-muted">By <a href="{{ route('home.index') }}">Penangiceman</a></span>
            </div>
            <div class="product-card-bottom">
                <div class="product-price">
                    <span>RM 32.00</span>
                    <span class="old-price">RM 33.8</span>
                </div>
                <div class="add-cart">
                    <a class="add" href="{{ route('home.index') }}"><i class="fi-rs-shopping-cart mr-5"></i>Add </a>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- end single product card :: timer --}}
