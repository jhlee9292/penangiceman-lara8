@extends('layouts.dashboard.index')


@section('content')

{{-- FACEBOOK --}}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0&appId=248411993036876&autoLogAppEvents=1" nonce="2STzpikU"></script>

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.products.variants.index') }}">Variation Products</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $variant_product->name }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">{{ $variant_product->name }}</h1>
      </div>
      <div class="col-auto d-flex">
        <a class="btn btn-primary mt-5" href="{{ route('dashboard.products.variants.edit', $variant_product->id) }}">Edit</a>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
    <div class="sa-entity-layout__body">

      {{-- MAIN FORM --}}
      <div class="sa-entity-layout__main">

        {{-- BASIC INFORMATION --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Basic information</h3></div>

            <div class="row mb-4">
              <div class="col-3"><strong>Product name</strong></div>
              <div class="col">{{ $variant_product->name }}</div>
            </div>

            <div class="row mb-4">
              <div class="col-3"><strong>Slug</strong></div>
              <div class="col">{{ $variant_product->slug }}</div>
            </div>

            <div class="row mb-4">
              <div class="col">{!! $variant_product->content !!}</div>
            </div>
          </div>
        </div>

        {{-- VARIATIONS --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h2 class="mb-0 fs-exact-18">Variations</h2></div>

            @foreach($variant_product->variants as $variant)
            <div class="mb-4">
              <div class="row mb-4">
                <div class="col-3"><img src="{{ $variant->the_image }}" class="w-25 h-auto" /></div>
              </div>

              <div class="row mb-4">
                <div class="col-2"><strong>Name</strong></div>
                <div class="col-4">{{ $variant->sku->name }}</div>

                <div class="col-2"><strong>Variant</strong></div>
                <div class="col-4">{{ $variant->variant }}</div>
              </div>

              <div class="row mb-4">
                <div class="col-2"><strong>Selling price</strong></div>
                <div class="col-4">RM {{ number_format($variant->sku->selling_price, 2) }}</div>

                <div class="col-2"><strong>Sale price</strong></div>
                <div class="col-4">RM {{ number_format($variant->sku->sale_price, 2) }}</div>
              </div>

              <div class="row mb-4">
                <div class="col-2"><strong>SKU</strong></div>
                <div class="col-4">{{ $variant->sku->sku }}</div>

                <div class="col-2"><strong>MFG#</strong></div>
                <div class="col-4">{{ $variant->sku->variant_mfg_part_number }}</div>
              </div>

              <div class="row mb-4">
                <div class="col-2"><strong>Stock</strong></div>
                <div class="col-4">{{ $variant->sku->stock }}</div>

                <div class="col-2"><strong>Weight (g)</strong></div>
                <div class="col-4">{{ $variant->sku->weight }}</div>
              </div>
            </div>
            @endforeach
            
          </div>
        </div>

        {{-- ATTRIBUTES --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h2 class="mb-0 fs-exact-18">Variation attributes</h2></div>

            {{-- ADDED ATTRIBUTES --}}
            <div id="added-attributes">
              @foreach($variant_product->attributes as $attribute)
              <div class="row mb-4">
                <div class="col-3"><strong>{{ $attribute->option }}</strong></div>
                <div class="col">{{ $attribute->value }}</div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
          
        {{-- FACEBOOK PLUGIN --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Facebook comments</h3></div>

            <div class="fb-comments" data-href="https://myletrik.com/product/{{ $variant_product->slug }}" data-width="775"></div>
          </div>
        </div>
        
      </div>

      {{-- SIDEBAR --}}
      <div class="sa-entity-layout__sidebar">

        {{-- STATS --}}
        <div class="card w-100">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Stats</h3></div>

            <div class="row text-center">
              <div class="col">
                <i class="fas fa-eye me-3"></i>
                {{ $variant_product->views->sum('count') }}
              </div>
              <div class="col">
                <i class="fas fa-shopping-basket me-3"></i>
                {{ $variant_product->user_relations->sum('sold') }}
              </div>
              <div class="col">
                <i class="fas fa-star me-3"></i>
                {{ number_format($variant_product->user_relations->average('rate'), 1) }}
              </div>
            </div>
          </div>
        </div>

        {{-- DETAILS --}}
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="row mb-4">
              <div class="col-4"><strong>Status</strong></div>
              <div class="col">
                <div class="badge
                            @if($variant_product->status == "draft") badge-sa-secondary
                            @elseif($variant_product->status == "published") badge-sa-success
                            @endif">
                  @if($variant_product->status == "draft") Draft
                  @elseif($variant_product->status == "published") Published
                  @endif
                </div>
              </div>
            </div>
            
            @if($variant_product->sku->brand)
            <div class="row mb-4">
              <div class="col-4"><strong>Brand</strong></div>
              <div class="col">
                {{ $variant_product->sku->brand->name }}
              </div>
            </div>
            @endif
            
            @if(!empty($variant_product->categories))
            <div class="row mb-4">
              <div class="col-4"><strong>Categories</strong></div>
              <div class="col">
                @foreach($variant_product->categories as $category)
                <span class="badge badge-sa-secondary">{{ $category->name }}</span>
                @endforeach
              </div>
            </div>
            @endif
            
            @if(!empty($variant_product->tags))
            <div class="row mb-4">
              <div class="col-4"><strong>Tags</strong></div>
              <div class="col">
                @foreach($variant_product->tags as $tag)
                <span class="badge badge-sa-secondary">{{ $tag->name }}</span>
                @endforeach
              </div>
            </div>
            @endif
          </div>
        </div>

        {{-- IMAGE --}}
        @if($variant_product->image)
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product image</h3></div>
            
            <img id="image-preview" class="w-100" src="{{ $variant_product->the_image }}" />
          </div>
        </div>
        @endif

        {{-- GALLERY --}}
        @if(!empty($variant_product->galleries))
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product gallery</h3></div>
            
            @foreach($variant_product->galleries as $gallery)
            <img id="image-preview" class="w-25 me-3" src="{{ $gallery->the_image }}" />
            @endforeach
          </div>
        </div>
        @endif

      </div>
    </div>
  </div>
  
</div>

@endsection