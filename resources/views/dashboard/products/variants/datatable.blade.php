@if(!$variant_products->isEmpty())
  
  @foreach($variant_products as $product)
  <tr class="align-middle">
    <td>
      <div class="d-flex align-items-center">
        <a href="{{ route('dashboard.products.variants.show', $product->id) }}" class="me-4">
          <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
            <img src="{{ $product->the_image}}" width="40" height="40" alt="" />
          </div>
        </a>
        <div>
          <a href="{{ route('dashboard.products.variants.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
        </div>
      </div>
    </td>
    <td>
      <div class="badge
                  @if($product->variants->count() > 0) badge-sa-success
                  @else badge-sa-danger
                  @endif">
        {{ $product->variants->count() }} Variant{{ ($product->variants->count() > 1) ? "s" : "" ; }}
      </div>
    </td>
    <td>
      <div class="sa-price">
        <?php
        $prices = array();

        foreach($product->variants as $variant) {
          array_push($prices, number_format($variant->sku->selling_price, 2));
        }
        ?>

        RM {{ array_reduce($prices, 'min', reset($prices)); }} -
        RM {{ array_reduce($prices, 'max', reset($prices)); }}
      </div>
    </td>
    <td>
      {{ $product->views->sum('count') }}
    </td>
    <td>
      {{ $product->user_relations->sum('sold') }}
    </td>
    <td>
      {{ number_format($product->user_relations->sum('rate'), 1) }}
    </td>
    <td>
      <div class="badge
                  @if($product->status == "draft") badge-sa-secondary
                  @elseif($product->status == "published") badge-sa-success
                  @endif">
        @if($product->status == "draft") Draft
        @elseif($product->status == "published") Published
        @endif
      </div>
    </td>
    <td>
      <a class="btn btn-primary" href="{{ route('dashboard.products.variants.edit', $product->id) }}">
        <i class="fas fa-pen"></i>
      </a>
      <a class="btn btn-danger"
         href="{{ route('dashboard.products.variants.destroy', $product->id) }}"
         onclick="return confirm('Are you sure you want to delete this product?')">
        <i class="fas fa-trash"></i>
      </a>
    </td>
  </tr>
  @endforeach
  
  <tr>
    <td colspan="8">
      <div class="p-0 pt-4">
        {{ $variant_products->links() }}
      </div>
    </td>
  </tr>
  
@else

  <tr>
    <td colspan="8">
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif