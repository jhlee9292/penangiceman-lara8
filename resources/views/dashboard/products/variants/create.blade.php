@extends('layouts.dashboard.index')


@push('header-scripts')
<style>
  .select2-container--default .select2-selection--multiple .select2-selection__rendered {
    padding: 10px !important;
  }
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    margin-right: 5px !important;
  }
</style>
@endpush


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.products.variants.index') }}">Variation Products</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add New</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Add New Variation Product</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.products.variants.store') }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Basic information</h2></div>
              
              <div class="mb-4">
                <label for="name" class="form-label">Product name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" />
              </div>
              
              <div class="mb-4">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug') }}" />
                <div class="form-text">Slug will be auto-generated.</div>
              </div>
              
              <div class="mb-4">
                <label for="content" class="form-label">Description</label>
                <textarea id="content" name="content" class="form-control">{{ old('content') }}</textarea>
              </div>
            </div>
          </div>
          
          {{-- CATEGORIES --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Categories</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col">
                  <a class="categories-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#categories-modal" data-input="#include_categories">
                    Add/Remove categories
                  </a>
                  
                  <input type="hidden" id="include_categories" name="include_categories" value="{{ old('include_categories') }}" />
                  
                  <table class="table mt-4 d-none">
                    <thead>
                      <tr>
                        <th>Selected category</th>
                      </tr>
                    </thead>

                    <tbody>
                      {{-- CATEGORIES --}}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
          {{-- ATTRIBUTES --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Add variation attributes</h2></div>
              
              <div class="row mb-4">
                <div class="col-4">
                  <label class="form-label">Options</label>
                  <div class="form-text">One option per field.</div>
                </div>
                
                <div class="col">
                  <label class="form-label">Values</label>
                  <div class="form-text">May enter multiple values, separate by "," commas.</div>
                </div>
              </div>
              
              {{-- NEW ATTRIBUTES --}}
              <div id="new-attributes">
                
              </div>
              
              {{-- ATTRIBUTE TEMPLATE --}}
              <div id="attribute-template" class="d-none">
                <div class="attribute row mb-4">
                  <div class="col-4">
                    <input type="text"
                           name="option[]"
                           class="form-control"
                           placeholder="Eg. Colour" />
                  </div>
                  <div class="col">
                    <input type="text"
                           name="value[]"
                           class="form-control"
                           placeholder="Eg. White,Black,Blue" />
                  </div>
                  <div class="col-1 my-auto">
                    <a class="remove-attribute text-danger" href="">
                      <i class="fas fa-times mt-3"></i>
                    </a>
                  </div>
                </div>
              </div>
              
              <button type="button" id="add-attribute" class="btn btn-primary mt-4">Add attribute</button>
              <div class="form-text">Save product to generate variations from the attributes you have entered.</div>
            </div>
          </div>
          
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- STATUS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Status</h2></div>
              
              <div class="mb-4">
                <label class="form-check">
                  <input type="radio" class="form-check-input" name="status" value="draft" checked />
                  <span class="form-check-label">Draft</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="published"
                         @if(old('status') == "published") checked @endif />
                  <span class="form-check-label">Publish</span>
                </label>
              </div>
            </div>
          </div>
          
          {{-- BRAND --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Brand</h3></div>
              
              <select id="brand" name="brand" class="sa-select form-select">
                <option value="">Select brand</option>
                @foreach($brands as $brand)
                <option value="{{ $brand->id }}" @if(old('brand') == $brand->id) selected @endif>{{ $brand->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          {{-- PRODUCT TAGS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product tags</h3></div>
              
              <select id="tags" name="tags[]" class="form-control" multiple>
                @foreach($tags as $tag)
                <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          
          {{-- IMAGE --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product image</h3></div>
              
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="d-none w-100 mb-4" src="" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Add image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-none">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
              </div>
            </div>
          </div>
          
          {{-- GALLERY --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product gallery</h3></div>
              
              <div id="new-gallery" class="w-100">
                
              </div>

              <div id="gallery-template" class="d-none">
                <div class="gallery position-relative mb-4">
                  <img class="w-25 me-3" src="" />
                  
                  <a class="remove-gallery d-none text-danger" role="button">Remove image</a>

                  <input type="file" name="gallery[]" class="form-control gallery-add" accept="image/*" />
                </div>
              </div>

              <a id="gallery-add" class="image-link" role="button">Add image</a>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
  </form>
  
</div>

{{-- CATEGORIES MODAL --}}
<div id="categories-modal" class="modal fade" tabindex="-1" aria-labelledby="categories-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Categories</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="category-search" class="form-control" placeholder="Search categories" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Category</th>
            </tr>
          </thead>

          <tbody>
            {{-- CATEGORIES --}}
          </tbody>
        </table>

        <input type="hidden" id="category-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="category-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

@endsection


@push('footer-scripts')
<script>
  // SLUGIFY
  $('#name').change(function(e) {
    var url = '{{ route('dashboard.slugify', "name") }}';
    url = url.replace('name', $(this).val());

    $.get(url,
      function(data) {
        $('#slug').val(data);
      }
    );
  });

  // SUMMERNOTE
  $('#content').summernote({
    minHeight: 200,
    toolbar: [
      ['para', ['ul', 'ol']],
      ['insert', ['picture'] ],
    ],
    popover: {
      image: [], link: [], table: [], air: []
    },
  });
  
  // SELECT2
  $('#tags').select2({
    tags: true,
    width: '100%',
    tokenSeparators: [','],
  });
  
  // PREVIEW IMAGE
  $('#image').change(function() {
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // REMOVE PREVIEW
  $('#image-remove a').click(function() {
    $('#image').val(null);
    $('#image-remove').attr('class', 'd-none');
    $('#image-add').html('Add image');
    $('#image-preview').addClass('d-none').attr('src', '');
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // ADD GALLERY
  $('#gallery-add').click(function() {
    var template = $('#gallery-template').html();
    $('#new-gallery').append(template);
  });
  
  // GALLERY IMAGE
  $('body').on('change','.gallery-add', function() {
    var obj = $(this);
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        obj.closest('.gallery').find('img').attr('src', reader.result);
        obj.closest('.gallery').find('.remove-gallery').removeClass('d-none');
        obj.addClass('d-none');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // REMOVE GALLERY
  $('body').on('click','.remove-gallery', function(e) {
    e.preventDefault();
    $(this).closest('.gallery').remove();
  });
  
  // ADD ATTRIBUTE
  $('#add-attribute').click(function() {
    var template = $('#attribute-template').html();
    $('#new-attributes').append(template);  
  });
  
  // REMOVE ATTRIBUTE
  $('body').on('click','.remove-attribute', function(e) {
    e.preventDefault();
    $(this).closest('.attribute').remove();
  });
  
  
  /********* DATATABLE *********/
  let input = "";
  
  /*** CATEGORIES ***/
  let include_categories = [];
  
    // FETCH CATEGORIES
  $('.categories-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#categories-modal #category-page').val();
    let query = $('#categories-modal #category-search').val();
    input = $(this).data('input');
    
    fetch_categories(page, query);    
  });
  
  // SEARCH CATEGORIES
  $('#category-search').keyup(function() {
    let page = $('#categories-modal #category-page').val();
    let query = $(this).val();

    fetch_categories(page, query);    
  });
  
  // PAGINATE CATEGORIES
  $('body').on('click', '#categories-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#categories-modal #category-search').val();
    
    $('#categories-modal #category-page').val(page);

    fetch_categories(page, query);    
  });
  
  // FETCH CATEGORIES VIA AJAX
  function fetch_categories(page, query) {
    let selected = include_categories;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_categories') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#categories-modal .modal-body table tbody').html('');
        $('#categories-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED CATEGORIES
  $('body').on('change', '#categories-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      include_categories.push($(this).val());
    }
    else { // DESELECT
      let index = include_categories.indexOf($(this).val());
      include_categories.splice(index, 1);
    }
  });
  
  // SAVE AND RENDER SELECTED CATEGORIES
  $('body').on('click', '#categories-modal #category-save', function() {
    let saved = include_categories;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_categories') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR CATEGORIES
  $('body').on('click', '.clear-categories', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.categories-modal-button').data('input');
    
    include_categories = [];
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
</script>
@endpush