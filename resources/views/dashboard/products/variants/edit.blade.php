@extends('layouts.dashboard.index')


@push('header-scripts')
<style>
  .select2-container--default .select2-selection--multiple .select2-selection__rendered {
    padding: 10px !important;
  }
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    margin-right: 5px !important;
  }
</style>
@endpush


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.products.variants.index') }}">Variation Products</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $variant_product->name }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Edit Product</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.products.variants.update', $variant_product->id) }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Basic information</h2></div>
              
              <div class="mb-4">
                <label for="name" class="form-label">Product name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $variant_product->name) }}" />
              </div>
              
              <div class="mb-4">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', $variant_product->slug) }}" />
                <div class="form-text">Slug will be auto-generated.</div>
              </div>
              
              <div class="mb-4">
                <label for="content" class="form-label">Description</label>
                <textarea id="content" name="content" class="form-control">{{ old('content', $variant_product->content) }}</textarea>
              </div>
            </div>
          </div>
          
          {{-- CATEGORIES --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Categories</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col">
                  <a class="categories-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#categories-modal" data-input="#include_categories">
                    Add/Remove categories
                  </a>
                  
                  <?php
                  $selected_include_categories = $variant_product->categories;
                  $include_categories = "";
                  
                  foreach($selected_include_categories as $category) {
                    $include_categories .= $category->id . ",";
                  }
                  
                  $include_categories = rtrim($include_categories, ",")
                  ?>
                  <input type="hidden" id="include_categories" name="include_categories" value="{{ old('include_categories', $include_categories) }}" />
                  
                  <table class="table mt-4 @if($selected_include_categories->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected categories</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_include_categories->isEmpty())
                        @foreach($selected_include_categories as $category)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $category->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $category->the_name }}
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-categories text-danger" role="button">
                            Clear categories
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
          {{-- VARIATIONS --}}
          @if($combinations > 1)
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Variations</h2></div>
              
              {{-- ADDED VARIATIONS --}}
              <div id="added-variations" class="accordion card">
                
                <?php $loop_var = 0; // ARRAY KEY FOR VARIANT ?>
                @foreach($variant_product->variants as $variant)
                <div class="accordion-item">
                  <h2 class="accordion-header">
                    <button type="button"
                            class="accordion-button sa-hover-area"
                            data-bs-toggle="collapse"
                            data-bs-target="collapse-next"
                            aria-expanded="false">
                      <span class="accordion-sa-icon"></span>
                      {{ $variant->variant }}
                    </button>
                  </h2>
                  <div class="accordion-collapse collapse" data-bs-parent="#added-variations">
                    <div class="accordion-body bg-light p-5">
                      
                      <div class="row mb-4">
                        <?php
                        $var_values = explode(",", $variant->variant); // VARIANT VALUES
                        $loop_val = 0 // ARRAY KEY FOR VARIANT VALUE
                        ?>
                        
                        @foreach($attributes as $attribute)
                        <div class="col">  
                          <label class="form-label">{{ $attribute->option }}</label>
                          <select name="variant_value[{{ $loop_var }}][]" class="sa-select form-select variant-value">
                            <!--option value="">Select {{ $attribute->option }}</option-->
                            <?php
                            // GET ATTRIBUTE VALUES
                            $attr_values = explode(",", $attribute->value); // ATTRIBUTE VALUES
                            ?>

                            @foreach($attr_values as $attr_value)
                            <option value="{{ trim($attr_value, " ") }}"
                                    @if(old('variant_value[$loop_var][]', $var_values[$loop_val]) == trim($attr_value, " ")) selected @endif
                                    >{{ trim($attr_value, " ") }}</option>
                            @endforeach
                          </select>
                        </div>
                        <?php $loop_val++; ?>
                        @endforeach
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">Name</label>
                          <input type="text"
                                 name="variant_name[{{ $loop_var }}]"
                                 class="form-control variant-sku"
                                 value="{{ old('variant_name[$loop_var]', $variant->sku->name) }}" />
                        </div>
                        <div class="col-2 text-center">
                          @if($variant->image)
                          <div class="max-w-20x">
                            <img src="{{ $variant->the_image }}" class="w-100 h-auto" />
                          </div>
                          <div class="mt-3">
                            <a id="remove-image" class="text-danger">Remove</a>
                          </div>
                          @else
                          <label class="form-label">Variation image</label>
                          <input type="file" name="variant_image[{{ $loop_var }}]" class="form-control variant-image" />
                          @endif
                        </div>
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">Selling price (RM)</label>
                          <input type="text"
                                 name="variant_selling_price[{{ $loop_var }}]"
                                 class="form-control variant-selling-price"
                                 value="{{ old('variant_selling_price', $variant->sku->selling_price) }}" />
                        </div>
                        <div class="col">
                          <label class="form-label">Sale price (RM)</label>
                          <input type="text"
                                 name="variant_sale_price[{{ $loop_var }}]"
                                 class="form-control variant-sale-price"
                                 value="{{ old('variant_sale_price', $variant->sku->sale_price) }}" />
                        </div>
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">SKU</label>
                          <input type="text"
                                 name="variant_sku[{{ $loop_var }}]"
                                 class="form-control variant-sku"
                                 value="{{ old('variant_sku', $variant->sku->sku) }}" />
                        </div>

                        <div class="col">
                          <label class="form-label">MFG#</label>
                          <input type="text"
                                 name="variant_mfg_part_number[{{ $loop_var }}]"
                                 class="form-control variant-mfg-part-number"
                                 value="{{ old('variant_mfg_part_number', $variant->sku->variant_mfg_part_number) }}" />
                        </div>
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">Stock</label>
                          <input type="number"
                                 name="variant_stock[{{ $loop_var }}]"
                                 class="form-control variant-stock"
                                 value="{{ old('variant_stock', $variant->sku->stock) }}" />
                        </div>

                        <div class="col">
                          <label class="form-label">Weight (g)</label>
                          <input type="number"
                                 name="variant_weight[{{ $loop_var }}]"
                                 class="form-control variant-weight"
                                 value="{{ old('variant_weight', $variant->sku->weight) }}" />
                        </div>
                      </div>
                      
                      <div class="row">
                        <div class="col">
                          <a class="remove-variation text-danger"
                             href="{{ route('dashboard.products.variations.destroy', $variant->id) }}"
                             onclick="return confirm('Are you sure you want to delete this variation?')">
                            Remove
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="variant_id[{{ $loop_var }}]" value="{{ $variant->id }}" />
                <?php $loop_var++; ?>
                @endforeach
                
              </div>
              
              {{-- MEW VARIATION --}}
              <div id="new-variations" class="accordion card">
                
              </div>
              
              {{-- VARIATION TEMPLATE --}}
              <div id="variations-template" class="accordion card d-none">
                <div class="accordion-item variation">
                  <h2 class="accordion-header">
                    <button type="button"
                            class="accordion-button sa-hover-area"
                            data-bs-toggle="collapse"
                            data-bs-target="collapse-next"
                            aria-expanded="false">
                      <span class="accordion-sa-icon"></span>
                      New variation
                    </button>
                  </h2>
                  <div class="accordion-collapse collapse" data-bs-parent="#new-variations">
                    <div class="accordion-body bg-light p-5">
                      
                      <div class="row mb-4">
                        @foreach($attributes as $attribute)
                        <div class="col">  
                          <label class="form-label">{{ $attribute->option }}</label>
                          <select name="" class="sa-select form-select new-variant-value">
                            <!--option value="">Select {{ $attribute->option }}</option-->
                            <?php
                            // GET ATTRIBUTE VALUES
                            $values = explode(",", $attribute->value);
                            ?>

                            @foreach($values as $value)
                            <option value="{{ trim($value, " ") }}"
                                    @if(old('variant_value') == trim($value, " ")) selected @endif>{{ trim($value, " ") }}</option>
                            @endforeach
                          </select>
                        </div>
                        @endforeach
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">Variation name</label>
                          <input type="text" name="" class="form-control new-variant-name"  />
                        </div>
                        <div class="col-2">
                          <label class="form-label">Variation image</label>
                          <input type="file" name="" class="form-control new-variant-image" accept="image/*" />
                        </div>
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">Selling price (RM)</label>
                          <input type="text" name="" class="form-control new-variant-selling-price" />
                        </div>
                        <div class="col">
                          <label class="form-label">Sale price (RM)</label>
                          <input type="text" name="" class="form-control new-variant-sale-price" />
                        </div>
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">SKU</label>
                          <input type="text" name="" class="form-control new-variant-sku" />
                        </div>
                        <div class="col">
                          <label class="form-label">MFG#</label>
                          <input type="text" name="" class="form-control new-variant-mfg-part-number" />
                        </div>
                      </div>
                      
                      <div class="row mb-4">
                        <div class="col">
                          <label class="form-label">Stock</label>
                          <input type="number" name="" class="form-control new-variant-stock" />
                        </div>
                        <div class="col">
                          <label class="form-label">Weight (g)</label>
                          <input type="number" name="" class="form-control new-variant-weight" />
                        </div>
                      </div>
                      
                      <!--div class="row">
                        <div class="col">
                          <a class="remove-variation text-danger" href="">Remove</a>
                        </div>
                      </div-->
                    </div>
                  </div>
                </div>
              </div>
              
              <button type="button"
                      id="add-variation"
                      class="btn btn-primary mt-4"
                      data-added="{{ $variant_product->variants->count() }}"
                      data-max="{{ $combinations }}">Add variation</button>
            </div>
          </div>
          @endif
          
          {{-- ATTRIBUTES --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Add/Remove variation attributes</h2></div>
              
              <div class="row g-4 mb-4">
                <div class="col-4">
                  <label class="form-label">Options</label>
                  <div class="form-text">One option per field.</div>
                </div>
                
                <div class="col">
                  <label class="form-label">Values</label>
                  <div class="form-text">May enter multiple values, separate by "," commas.</div>
                </div>
              </div>
              
              {{-- ADDED ATTRIBUTES --}}
              <div id="added-attributes">
                @foreach($attributes as $attribute)
                <div class="attribute row mb-4">
                  <div class="col-4">
                    <input type="text"
                           name="option[]"
                           class="form-control"
                           value="{{ $attribute->option }}"
                           placeholder="Eg. Colour" />
                  </div>
                  <div class="col">
                    <input type="text"
                           name="value[]"
                           class="form-control"
                           value="{{ $attribute->value }}"
                           placeholder="Eg. White,Black,Blue" />
                  </div>
                  <div class="col-1 my-auto">
                    <a class="remove-attribute text-danger" href="">
                      <i class="fas fa-times mt-3"></i>
                    </a>
                  </div>
                </div>
                @endforeach
              </div>
              
              {{-- NEW ATTRIBUTES --}}
              <div id="new-attributes">
                
              </div>
              
              {{-- ATTRIBUTE TEMPLATE --}}
              <div id="attribute-template" class="d-none">
                <div class="attribute row mb-4">
                  <div class="col-4">
                    <input type="text"
                           name="new_option[]"
                           class="form-control"
                           placeholder="Eg. Colour" />
                  </div>
                  <div class="col">
                    <input type="text"
                           name="new_value[]"
                           class="form-control"
                           placeholder="Eg. White,Black,Blue" />
                  </div>
                  <div class="col-1 my-auto">
                    <a class="remove-attribute text-danger" href="">
                      <i class="fas fa-times mt-3"></i>
                    </a>
                  </div>
                </div>
              </div>
              
              <button type="button" id="add-attribute" class="btn btn-primary mt-4">Add attribute</button>
              <div class="form-text">Save product to generate variations from the attributes you have entered.</div>
            </div>
          </div>
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- STATUS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Status</h2></div>
              
              <div class="mb-4">
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="draft"
                         @if($variant_product->status == "draft") checked @endif />
                  <span class="form-check-label">Draft</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="published"
                         @if(old('status', $variant_product->status) == "published") checked @endif />
                  <span class="form-check-label">@if($variant_product->status == "draft") Publish @else Published @endif</span>
                </label>
              </div>
            </div>
          </div>
          
          {{-- BRAND --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Brand</h3></div>
              
              <select id="brand" name="brand" class="sa-select form-select">
                <option value="">Select brand</option>
                @foreach($brands as $brand)
                <option value="{{ $brand->id }}"
                        @if($variant_product->sku->brand_id == $brand->id) selected @endif>
                  {{ $brand->name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          
          {{-- PRODUCT TAGS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product tags</h3></div>
              
              <select id="tags" name="tags[]" class="form-control" multiple>
                @foreach($tags as $tag)
                <option value="{{ $tag->name }}"
                        @foreach($variant_product->tags as $selected)
                        {{ $selected->name == $tag->name ? "selected" : "" }}
                        @endforeach>
                  {{ $tag->name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          
          {{-- IMAGE --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product image</h3></div>
              
              @if(!$variant_product->image)
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="d-none w-100 mb-4" src="" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Add image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-none">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
              </div>
              @else
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="w-100 mb-4" src="{{ $variant_product->the_image }}" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Change image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-inline">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
                
                <input type="hidden" id="image-action" name="image_action" value="" />
              </div>
              @endif
              
            </div>
          </div>
          
          {{-- GALLERY --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product gallery</h3></div>

              <div id="added-gallery">
                @foreach($variant_product->galleries as $gallery)
                <div class="gallery position-relative mb-4">
                  <img class="w-25 me-3" src="{{ $gallery->the_image }}" />
                  
                  <a class="text-danger"
                     href="{{ route('dashboard.products.galleries.destroy', $gallery->id) }}"
                     onclick="return confirm('Are you sure you want to delete this image?')">
                    Remove image
                  </a>
                </div>
                @endforeach
              </div>
              
              <div id="new-gallery" class="w-100">
                
              </div>

              <div id="gallery-template" class="d-none">
                <div class="gallery position-relative mb-4">
                  <img class="w-25 me-3" src="" />
                  
                  <a class="remove-gallery d-none text-danger" role="button">Remove image</a>

                  <input type="file" name="gallery[]" class="form-control gallery-add" accept="image/*" />
                </div>
              </div>

              <a id="gallery-add" class="image-link" role="button">Add image</a>
            </div>
          </div>
          
        </div>
      </div>
    </div>
    
  </form>
  
</div>

{{-- CATEGORIES MODAL --}}
<div id="categories-modal" class="modal fade" tabindex="-1" aria-labelledby="categories-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Categories</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="category-search" class="form-control" placeholder="Search categories" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Category</th>
            </tr>
          </thead>

          <tbody>
            {{-- CATEGORIES --}}
          </tbody>
        </table>

        <input type="hidden" id="category-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="category-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

@endsection


@push('footer-scripts')
<script>
  // SUMMERNOTE
  $('#content').summernote({
    minHeight: 200,
    toolbar: [
      ['para', ['ul', 'ol']],
      ['insert', ['picture'] ],
    ],
    popover: {
      image: [], link: [], table: [], air: []
    },
  });
  
  // SELECT2
  $('#tags').select2({
    tags: true,
    width: '100%',
    tokenSeparators: [','],
  });
  
  // PREVIEW IMAGE
  $('#image').change(function() {
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
        $('#image-action').val('DELETE');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // REMOVE PREVIEW
  $('#image-remove a').click(function() {
    $('#image').val(null);
    $('#image-remove').attr('class', 'd-none');
    $('#image-add').html('Add image');
    $('#image-preview').addClass('d-none').attr('src', '');
    $('#image-action').val('DELETE');
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // ADD GALLERY
  $('#gallery-add').click(function() {
    var template = $('#gallery-template').html();
    $('#new-gallery').append(template);
  });
  
  // GALLERY IMAGE
  $('body').on('change','.gallery-add', function() {
    var obj = $(this);
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        obj.closest('.gallery').find('img').attr('src', reader.result);
        obj.closest('.gallery').find('.remove-gallery').removeClass('d-none');
        obj.addClass('d-none');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // REMOVE GALLERY
  $('body').on('click','.remove-gallery', function(e) {
    e.preventDefault();
    $(this).closest('.gallery').remove();
  });

  // VARIATION
  var max = $('#add-variation').data('added'); // MAX VARIATION COMBINATIONS
  
  $('#add-variation').click(function() {
    if($(this).data('max') != max) {
      var template = $('#variations-template').html();
      
      $('#new-variations').append(template);
      
      //$('.accordion-item').eq(max).removeClass('d-none');
      $('.accordion-item').eq(max).find(".new-variant-value").attr('name', 'new_variant_value[' + max + '][]');
      $('.accordion-item').eq(max).find(".new-variant-name").attr('name', 'new_variant_name[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-image").attr('name', 'new_variant_image[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-selling-price").attr('name', 'new_variant_selling_price[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-sale-price").attr('name', 'new_variant_sale_price[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-sku").attr('name', 'new_variant_sku[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-mfg-part-number").attr('name', 'new_variant_mfg_part_number[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-stock").attr('name', 'new_variant_stock[' + max + ']');
      $('.accordion-item').eq(max).find(".new-variant-weight").attr('name', 'new_variant_weight[' + max + ']');
      
      max++;
    }
    else {
      alert("Max variation combinations achieved!");
    }
  });
  
  // VARIATION TOGGLE
  $('body').on('click', '.accordion-button', function() {
    var target = $(this).parent().next();
    target.data('collapse') ? target.collapse('collapse') : target.collapse('toggle') ;
  })
  
  // ADD ATTRIBUTE
  $('#add-attribute').click(function() {
    var template = $('#attribute-template').html();
    $('#new-attributes').append(template);  
  });
  
  // REMOVE ATTRIBUTE
  $('body').on('click','.remove-attribute', function(e) {
    e.preventDefault();
    $(this).closest('.attribute').remove();
  });
  
  
  /********* DATATABLE *********/
  let input = "";
  
  /*** CATEGORIES ***/
  let include_categories = ($('#include_categories').val()) ? $('#include_categories').val().split(",") : [] ;
  
  // FETCH CATEGORIES
  $('.categories-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#categories-modal #category-page').val();
    let query = $('#categories-modal #category-search').val();
    input = $(this).data('input');
    
    fetch_categories(page, query);    
  });
  
  // SEARCH CATEGORIES
  $('#category-search').keyup(function() {
    let page = $('#categories-modal #category-page').val();
    let query = $(this).val();

    fetch_categories(page, query);    
  });
  
  // PAGINATE CATEGORIES
  $('body').on('click', '#categories-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#categories-modal #category-search').val();
    
    $('#categories-modal #category-page').val(page);

    fetch_categories(page, query);    
  });
  
  // FETCH CATEGORIES VIA AJAX
  function fetch_categories(page, query) {
    let selected = include_categories;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_categories') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#categories-modal .modal-body table tbody').html('');
        $('#categories-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED CATEGORIES
  $('body').on('change', '#categories-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      include_categories.push($(this).val());
    }
    else { // DESELECT
      let index = include_categories.indexOf($(this).val());
      include_categories.splice(index, 1);
    }
  });
  
  // SAVE AND RENDER SELECTED CATEGORIES
  $('body').on('click', '#categories-modal #category-save', function() {
    let saved = include_categories;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_categories') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR CATEGORIES
  $('body').on('click', '.clear-categories', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.categories-modal-button').data('input');
    
    include_categories = [];
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
</script>
@endpush