@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Simple Products</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Simple Products</h1>
      </div>
      <div class="col-auto d-flex">
        <a href="{{ route('dashboard.products.simples.create') }}" class="btn btn-primary mt-5">Add New</a>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- DATA TABLE --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- SEARCH --}}
  <div class="card p-4">
    <input type="text"
           id="search"
           class="form-control form-control--search mx-auto"
           placeholder="Search products" />
  </div>
  
  {{-- TABLE --}}
  <div id="data-table" class="card mt-5 p-4">
    <table class="table">
      <thead>
        <tr>
          <th class="sorting" data-sort_by="name" data-sort_type="asc" role="button">
            Product
            <i class="fas fa-sort text-muted"></i>
          </th>
          <th>Stock</th>
          <th>Price</th>
          <th class="sorting" data-sort_by="view" data-sort_type="asc" role="button">
            <i class="fas fa-eye"></i>
            <i class="fas fa-sort text-muted"></i>
          </th>
          <th class="sorting" data-sort_by="sold" data-sort_type="asc" role="button">
            <i class="fas fa-shopping-basket"></i>
            <i class="fas fa-sort text-muted"></i>
          </th>
          <th class="sorting" data-sort_by="rate" data-sort_type="asc" role="button">
            <i class="fas fa-star"></i>
            <i class="fas fa-sort text-muted"></i>
          </th>
          <th class="sorting" data-sort_by="status" data-sort_type="asc" role="button">
            Status
            <i class="fas fa-sort text-muted"></i>
          </th>
          <th>Action</th>
        </tr>
      </thead>

      <tbody>
        @include('dashboard.products.simples.datatable')
      </tbody>
    </table>

    <input type="hidden" id="page" name="page" value="1" />
    <input type="hidden" id="sort_by" name="sort_by" value="" />
    <input type="hidden" id="sort_type" name="sort_type" value="" />
  </div>
  
</div>

@endsection


@push('footer-scripts')
<script>
  // PAGINATION
  $('body').on('click', '.pagination a', function(e) {
    e.preventDefault();
    
    var page = $(this).attr('href').split("?page=")[1];
    var sort_by = $('#sort_by').val();
    var sort_type = $('#sort_type').val();
    var query = $('#search').val();
    
    $('#page').val(page);
    
    fetch_data(page, sort_by, sort_type, query);
  });
  
  // SORT
  $('.sorting').click(function() {
    var page = $('#page').val();
    var sort_by = $(this).data('sort_by');
    var sort_type = $(this).data('sort_type');
    var query = $('#search').val();
    var reverse_order = "";
    
    // CHANGE SORTING ORDER
    if(sort_type == "asc") {
      reverse_order = "desc";
      $(this).data('sort_type', reverse_order);
    }
    else {
      reverse_order = "asc";
      $(this).data('sort_type', reverse_order);
    }
    
    $('#sort_by').val(sort_by);
    $('#sort_type').val(reverse_order);
    
    fetch_data(page, sort_by, sort_type, query);
  });
  
  // SEARCH
  $('#search').keyup(function() {
    var page = $('#page').val();
    var sort_by = $('#sort_by').val();
    var sort_type = $('#sort_type').val();
    var query = $(this).val();
    
    fetch_data(page, sort_by, sort_type, query);
  });
  
  // RENDER DATA TABLE
  function fetch_data(page, sort_by, sort_type, query) {
    $.ajax({
      url: "{{ route('dashboard.products.simples.fetch_data') }}",
      data: "page=" + page + "&sort_by=" + sort_by + "&sort_type=" + sort_type + "&query=" + query,
      success: function(data) {
        $('#data-table tbody').html('');
        $('#data-table tbody').html(data);
      }
    });
  }
</script>
@endpush