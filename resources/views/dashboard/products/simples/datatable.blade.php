@if(!$simple_products->isEmpty())
  
  @foreach($simple_products as $product)
  <tr class="align-middle">
    <td>
      <div class="d-flex align-items-center">
        <a href="{{ route('dashboard.products.simples.show', $product->id) }}" class="me-4">
          <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
            <img src="{{ $product->the_image}}" width="40" height="40" alt="" />
          </div>
        </a>
        <div>
          <a href="{{ route('dashboard.products.simples.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
          <div class="sa-meta mt-0">
            <ul class="sa-meta__list">
              <li class="sa-meta__item">SKU: {{ $product->sku->sku }}</li>
              <li class="sa-meta__item">MFG#: {{ $product->sku->mfg_part_number }}</li>
            </ul>
          </div>
        </div>
      </div>
    </td>
    <td>
      <div class="badge
                  @if($product->sku->stock >= 10) badge-sa-success
                  @elseif($product->sku->stock < 10 && $product->sku->stock > 0) badge-sa-warning
                  @elseif($product->sku->stock == 0) badge-sa-danger
                  @endif" >
        @if($product->sku->stock > 0) {{ $product->sku->stock }} In Stock
        @else Out Of Stock
        @endif
      </div>
    </td>
    <td>
      <div class="sa-price">
        RM
        @if($product->sku->sale_price > 0)
          <del>{{ number_format($product->sku->selling_price, 2) }}</del>
          {{ number_format($product->sku->sale_price, 2) }}
        @else
          {{ number_format($product->sku->selling_price, 2) }}
        @endif
      </div>
    </td>
    <td>
      {{ $product->views->sum('count') }}
    </td>
    <td>
      {{ $product->user_relations->sum('sold') }}
    </td>
    <td>
      {{ number_format($product->user_relations->average('rate'), 1) }}
    </td>
    <td>
      <div class="badge
                  @if($product->status == "draft") badge-sa-secondary
                  @elseif($product->status == "published") badge-sa-success
                  @endif">
        @if($product->status == "draft") Draft
        @elseif($product->status == "published") Published
        @endif
      </div>
    </td>
    <td>
      <a class="btn btn-primary" href="{{ route('dashboard.products.simples.edit', $product->id) }}">
        <i class="fas fa-pen"></i>
      </a>
      <a class="btn btn-danger"
         href="{{ route('dashboard.products.simples.destroy', $product->id) }}"
         onclick="return confirm('Are you sure you want to delete this product?')">
        <i class="fas fa-trash"></i>
      </a>
    </td>
  </tr>
  @endforeach

  <tr>
    <td colspan="8">
      <div class="p-0 pt-4">
        {{ $simple_products->links() }}
      </div>
    </td>
  </tr>
  
@else

  <tr>
    <td colspan="8">
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif