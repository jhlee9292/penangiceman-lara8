@extends('layouts.dashboard.index')


@push('header-scripts')
<meta property="fb:app_id" content="248411993036876" />
@endpush

@push('body-scripts')
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0&appId=248411993036876&autoLogAppEvents=1" nonce="2STzpikU"></script>
@endpush


@section('content')

{{-- FACEBOOK --}}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0&appId=248411993036876&autoLogAppEvents=1" nonce="2STzpikU"></script>

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.products.simples.index') }}">Simple Products</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $simple_product->name }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">{{ $simple_product->name }}</h1>
      </div>
      <div class="col-auto d-flex">
        <a class="btn btn-primary mt-5" href="{{ route('dashboard.products.simples.edit', $simple_product->id) }}">Edit</a>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
    <div class="sa-entity-layout__body">

      {{-- MAIN FORM --}}
      <div class="sa-entity-layout__main">

        {{-- BASIC INFORMATION --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Basic information</h3></div>

            <div class="row mb-4">
              <div class="col-3"><strong>Product name</strong></div>
              <div class="col">{{ $simple_product->name }}</div>
            </div>

            <div class="row mb-4">
              <div class="col-3"><strong>Slug</strong></div>
              <div class="col">{{ $simple_product->slug }}</div>
            </div>

            <div class="row mb-4">
              <div class="col">{!! $simple_product->content !!}</div>
            </div>
          </div>
        </div>
          
        {{-- PRICING --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Pricing</h3></div>

            <div class="row mb-4">
              <div class="col-2"><strong>Selling price</strong></div>
              <div class="col-4">RM {{ number_format($simple_product->sku->selling_price, 2) }}</div>
              
              <div class="col-2"><strong>Sale price</strong></div>
              <div class="col-4">RM {{ number_format($simple_product->sku->sale_price, 2) }}</div>
            </div>
          </div>
        </div>

        {{-- INVENTORY --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Inventory</h3></div>

            <div class="row mb-4">
              <div class="col-2"><strong>SKU</strong></div>
              <div class="col-4">{{ $simple_product->sku->sku }}</div>
              
              <div class="col-2"><strong>MFG#</strong></div>
              <div class="col-4">{{ $simple_product->sku->mfg_part_number }}</div>
            </div>

            <div class="row mb-4">
              <div class="col-2"><strong>Stock</strong></div>
              <div class="col-4">{{ $simple_product->sku->stock }}</div>
              
              <div class="col-2"><strong>Weight (g)</strong></div>
              <div class="col-4">{{ $simple_product->sku->weight }}</div>
            </div>
          </div>
        </div>
          
        {{-- FACEBOOK PLUGIN --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Facebook comments</h3></div>

            <div class="fb-comments" data-href="https://myletrik.com/product/{{ $simple_product->slug }}" data-width="100%"></div>
          </div>
        </div>
        
      </div>

      {{-- SIDEBAR --}}
      <div class="sa-entity-layout__sidebar">

        {{-- STATS --}}
        <div class="card w-100">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Stats</h3></div>

            <div class="row text-center">
              <div class="col">
                <i class="fas fa-eye me-3"></i>
                {{ $simple_product->views->sum('count') }}
              </div>
              <div class="col">
                <i class="fas fa-shopping-basket me-3"></i>
                {{ $simple_product->user_relations->sum('sold') }}
              </div>
              <div class="col">
                <i class="fas fa-star me-3"></i>
                {{ number_format($simple_product->user_relations->average('rate'), 1) }}
              </div>
            </div>
          </div>
        </div>

        {{-- DETAILS --}}
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="row mb-4">
              <div class="col-4"><strong>Status</strong></div>
              <div class="col">
                <div class="badge
                            @if($simple_product->status == "draft") badge-sa-secondary
                            @elseif($simple_product->status == "published") badge-sa-success
                            @endif">
                  @if($simple_product->status == "draft") Draft
                  @elseif($simple_product->status == "published") Published
                  @endif
                </div>
              </div>
            </div>
            
            @if($simple_product->sku->brand)
            <div class="row mb-4">
              <div class="col-4"><strong>Brand</strong></div>
              <div class="col">
                {{ $simple_product->sku->brand->name }}
              </div>
            </div>
            @endif
            
            @if(!empty($simple_product->categories))
            <div class="row mb-4">
              <div class="col-4"><strong>Categories</strong></div>
              <div class="col">
                @foreach($simple_product->categories as $category)
                <span class="badge badge-sa-secondary">{{ $category->name }}</span>
                @endforeach
              </div>
            </div>
            @endif
            
            @if(!empty($simple_product->tags))
            <div class="row mb-4">
              <div class="col-4"><strong>Tags</strong></div>
              <div class="col">
                @foreach($simple_product->tags as $tag)
                <span class="badge badge-sa-secondary">{{ $tag->name }}</span>
                @endforeach
              </div>
            </div>
            @endif
          </div>
        </div>

        {{-- IMAGE --}}
        @if($simple_product->image)
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product image</h3></div>
            
            <img id="image-preview" class="w-100" src="{{ $simple_product->the_image }}" />
          </div>
        </div>
        @endif

        {{-- GALLERY --}}
        @if(!empty($simple_product->galleries))
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product gallery</h3></div>
            
            @foreach($simple_product->galleries as $gallery)
            <img id="image-preview" class="w-25 me-3" src="{{ $gallery->the_image }}" />
            @endforeach
          </div>
        </div>
        @endif
        
      </div>

    </div>
  </div>
    
</div>

@endsection