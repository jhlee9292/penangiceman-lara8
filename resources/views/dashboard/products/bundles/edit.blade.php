@extends('layouts.dashboard.index')


@push('header-scripts')
<style>
  .select2-container--default .select2-selection--multiple .select2-selection__rendered {
    padding: 10px !important;
  }
  .select2-container--default .select2-selection--multiple .select2-selection__choice {
    margin-right: 5px !important;
  }
</style>
@endpush


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.products.bundles.index') }}">Bundled Products</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $bundled_product->name }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Edit Product</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.products.bundles.update', $bundled_product->id) }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Basic information</h2></div>
              
              <div class="mb-4">
                <label for="name" class="form-label">Product name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $bundled_product->name) }}" />
              </div>
              
              <div class="mb-4">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', $bundled_product->slug) }}" />
                <div class="form-text">Slug will be auto-generated.</div>
              </div>
              
              <div class="mb-4">
                <label for="content" class="form-label">Description</label>
                <textarea id="content" name="content" class="form-control">{{ old('content', $bundled_product->content) }}</textarea>
              </div>
            </div>
          </div>
          
          {{-- CATEGORIES --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Categories</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col">
                  <a class="categories-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#categories-modal" data-input="#include_categories">
                    Add/Remove categories
                  </a>
                  
                  <?php
                  $selected_include_categories = $bundled_product->categories;
                  $include_categories = "";
                  
                  foreach($selected_include_categories as $category) {
                    $include_categories .= $category->id . ",";
                  }
                  
                  $include_categories = rtrim($include_categories, ",")
                  ?>
                  <input type="hidden" id="include_categories" name="include_categories" value="{{ old('include_categories', $include_categories) }}" />
                  
                  <table class="table mt-4 @if($selected_include_categories->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected categories</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_include_categories->isEmpty())
                        @foreach($selected_include_categories as $category)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $category->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $category->the_name }}
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-categories text-danger" role="button">
                            Clear categories
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
          {{-- PRICING --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Pricing</h2></div>
              
              <div class="row g-4">
                <div class="col">
                  <label for="selling-price" class="form-label">Selling price (RM)</label>
                  <input type="text"
                         id="selling-price"
                         name="selling_price"
                         class="form-control"
                         value="{{ old('selling_price', number_format($bundled_product->sku->selling_price, 2)) }}" />
                </div>
                
                <div class="col">
                  <label for="sale-price" class="form-label">Sale price (RM)</label>
                  <input type="text"
                         id="sale-price"
                         name="sale_price"
                         class="form-control"
                         value="{{ old('sale_price', number_format($bundled_product->sku->sale_price, 2)) }}" />
                </div>
              </div>
            </div>
          </div>
          
          {{-- INVENTORY --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Inventory</h2></div>
              
              <div class="row g-4 mb-4">
                <div class="col">
                  <label for="sku" class="form-label">SKU</label>
                  <input type="text"
                         id="sku"
                         name="sku"
                         class="form-control"
                         value="{{ old('sku', $bundled_product->sku->sku) }}" />
                </div>
                
                <div class="col">
                  <label for="mfg-part-number" class="form-label">MFG#</label>
                  <input type="text"
                         id="mfg-part-number"
                         name="mfg_part_number"
                         class="form-control"
                         value="{{ old('mfg_part_number', $bundled_product->sku->mfg_part_number) }}" />
                </div>
              </div>
              
              <div class="row g-4">
                <div class="col">
                  <label for="stock" class="form-label">Stock</label>
                  <input type="number"
                         id="stock"
                         name="stock"
                         class="form-control"
                         step="1"
                         value="{{ old('stock', $bundled_product->sku->stock) }}" />
                </div>
                
                <div class="col">
                  <label for="weight" class="form-label">Weight (g)</label>
                  <input type="number"
                         id="weight"
                         name="weight"
                         class="form-control"
                         step="1"
                         value="{{ old('weight', $bundled_product->sku->weight) }}" />
                </div>
              </div>
            </div>
          </div>
          
          {{-- BUNDLED PRODUCTS --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Bundled products</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col">
                  <a class="products-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#products-modal" data-input="#include_products">
                    Add/Remove products
                  </a>
                  
                  <?php
                  $selected_include_products = $bundled_product->bundles;
                  $include_products = "";
                  
                  foreach($selected_include_products as $product) {
                    $include_products .= $product->bundled_product_id . ",";
                  }

                  $include_products = rtrim($include_products, ",");
                  ?>
                  <input type="hidden" id="include_products" name="include_products" value="{{ old('include_products', $include_products) }}" />
                  
                  <table class="table mt-4 @if($selected_include_products->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected product</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_include_products->isEmpty())
                        @foreach($selected_include_products as $bundled)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $bundled->product->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $bundled->product->name }}
                                <div class="sa-meta mt-0">
                                  <ul class="sa-meta__list">
                                    <li class="sa-meta__item">SKU: {{ $bundled->product->sku->sku }}</li>
                                    <li class="sa-meta__item">MFG#: {{ $bundled->product->sku->mfg_part_number }}</li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-products text-danger" role="button">
                            Clear products
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- STATUS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Status</h2></div>
              
              <div class="mb-4">
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="draft"
                         @if($bundled_product->status == "draft") checked @endif />
                  <span class="form-check-label">Draft</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="published"
                         @if(old('status', $bundled_product->status) == "published") checked @endif />
                  <span class="form-check-label">@if($bundled_product->status == "draft") Publish @else Published @endif</span>
                </label>
              </div>
            </div>
          </div>
          
          {{-- PRODUCT TAGS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product tags</h3></div>
              
              <select id="tags" name="tags[]" class="form-control" multiple>
                @foreach($tags as $tag)
                <option value="{{ $tag->name }}"
                        @foreach($bundled_product->tags as $selected)
                        {{ $selected->name == $tag->name ? "selected" : "" }}
                        @endforeach>
                  {{ $tag->name }}
                </option>
                @endforeach
              </select>
            </div>
          </div>
          
          {{-- IMAGE --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product image</h3></div>
              
              @if(!$bundled_product->image)
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="d-none w-100 mb-4" src="" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Add image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-none">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
              </div>
              @else
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="w-100 mb-4" src="{{ $bundled_product->the_image }}" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Change image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-inline">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
                
                <input type="hidden" id="image-action" name="image_action" value="" />
              </div>
              @endif
              
            </div>
          </div>
          
          {{-- GALLERY --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product gallery</h3></div>

              <div id="added-gallery">
                @foreach($bundled_product->galleries as $gallery)
                <div class="gallery position-relative mb-4">
                  <img class="w-25 me-3" src="{{ $gallery->the_image }}" />
                  
                  <a class="text-danger"
                     href="{{ route('dashboard.products.galleries.destroy', $gallery->id) }}"
                     onclick="return confirm('Are you sure you want to delete this image?')">
                    Remove image
                  </a>
                </div>
                @endforeach
              </div>
              
              <div id="new-gallery" class="w-100">
                
              </div>

              <div id="gallery-template" class="d-none">
                <div class="gallery position-relative mb-4">
                  <img class="w-25 me-3" src="" />
                  
                  <a class="remove-gallery d-none text-danger" role="button">Remove image</a>

                  <input type="file" name="gallery[]" class="form-control gallery-add" accept="image/*" />
                </div>
              </div>

              <a id="gallery-add" class="image-link" role="button">Add image</a>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
  </form>
  
</div>

{{-- CATEGORIES MODAL --}}
<div id="categories-modal" class="modal fade" tabindex="-1" aria-labelledby="categories-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Categories</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="category-search" class="form-control" placeholder="Search categories" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Category</th>
            </tr>
          </thead>

          <tbody>
            {{-- CATEGORIES --}}
          </tbody>
        </table>

        <input type="hidden" id="category-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="category-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

{{-- PRODUCTS MODAL --}}
<div id="products-modal" class="modal fade" tabindex="-1" aria-labelledby="products-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Products</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="product-search" class="form-control" placeholder="Search products" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Product</th>
            </tr>
          </thead>

          <tbody>
            {{-- PRODUCTS --}}
          </tbody>
        </table>

        <input type="hidden" id="product-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="product-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

@endsection


@push('footer-scripts')
<script>
  // SUMMERNOTE
  $('#content').summernote({
    minHeight: 200,
    toolbar: [
      ['para', ['ul', 'ol']],
      ['insert', ['picture'] ],
    ],
    popover: {
      image: [], link: [], table: [], air: []
    },
  });
  
  // SELECT2
  $('#tags').select2({
    tags: true,
    width: '100%',
    tokenSeparators: [','],
  });
  
  // PREVIEW IMAGE
  $('#image').change(function() {
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
        $('#image-action').val('DELETE');
      }
      reader.readAsDataURL(file);
      
    }
  });
  
  // REMOVE PREVIEW
  $('#image-remove a').click(function() {
    $('#image').val(null);
    $('#image-remove').attr('class', 'd-none');
    $('#image-add').html('Add image');
    $('#image-preview').addClass('d-none').attr('src', '');
    $('#image-action').val('DELETE');
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
      
    }
  });
  
  // ADD GALLERY
  $('#gallery-add').click(function() {
    var template = $('#gallery-template').html();
    $('#new-gallery').append(template);  
  });
  
  // GALLERY IMAGE
  $('body').on('change','.gallery-add', function() {
    var obj = $(this);
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        obj.closest('.gallery').find('img').attr('src', reader.result);
        obj.closest('.gallery').find('.remove-gallery').removeClass('d-none');
        obj.addClass('d-none');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // REMOVE GALLERY
  $('body').on('click','.remove-gallery', function(e) {
    e.preventDefault();
    $(this).closest('.gallery').remove();
  });
  
  
  /********* DATATABLE *********/
  let input = "";
  
  /*** CATEGORIES ***/
  let include_categories = ($('#include_categories').val()) ? $('#include_categories').val().split(",") : [] ;
  
  // FETCH CATEGORIES
  $('.categories-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#categories-modal #category-page').val();
    let query = $('#categories-modal #category-search').val();
    input = $(this).data('input');
    
    fetch_categories(page, query);    
  });
  
  // SEARCH CATEGORIES
  $('#category-search').keyup(function() {
    let page = $('#categories-modal #category-page').val();
    let query = $(this).val();

    fetch_categories(page, query);    
  });
  
  // PAGINATE CATEGORIES
  $('body').on('click', '#categories-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#categories-modal #category-search').val();
    
    $('#categories-modal #category-page').val(page);

    fetch_categories(page, query);    
  });
  
  // FETCH CATEGORIES VIA AJAX
  function fetch_categories(page, query) {
    let selected = include_categories;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_categories') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#categories-modal .modal-body table tbody').html('');
        $('#categories-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED CATEGORIES
  $('body').on('change', '#categories-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      include_categories.push($(this).val());
    }
    else { // DESELECT
      let index = include_categories.indexOf($(this).val());
      include_categories.splice(index, 1);
    }
  });
  
  // SAVE AND RENDER SELECTED CATEGORIES
  $('body').on('click', '#categories-modal #category-save', function() {
    let saved = include_categories;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_categories') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR CATEGORIES
  $('body').on('click', '.clear-categories', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.categories-modal-button').data('input');
    
    include_categories = [];
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
  
  
  /*** PRODUCTS ***/
  let include_products = ($('#include_products').val()) ? $('#include_products').val().split(",") : [] ;
  
  // FETCH PRODUCTS
  $('.products-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#products-modal #product-page').val();
    let query = $('#products-modal #product-search').val();
    input = $(this).data('input');
    
    fetch_products(page, query);    
  });
  
  // SEARCH PRODUCTS
  $('#product-search').keyup(function() {
    let page = $('#products-modal #product-page').val();
    let query = $(this).val();

    fetch_products(page, query);    
  });
  
  // PAGINATE PRODUCTS
  $('body').on('click', '#products-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#products-modal #product-search').val();
    
    $('#products-modal #product-page').val(page);

    fetch_products(page, query);    
  });
  
  // FETCH PRODUCTS VIA AJAX
  function fetch_products(page, query) {
    let selected = include_products;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_products') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#products-modal .modal-body table tbody').html('');
        $('#products-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED PRODUCTS
  $('body').on('change', '#products-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      include_products.push($(this).val());
    }
    else { // DESELECT
      let index = include_products.indexOf($(this).val());
      include_products.splice(index, 1);
    }
  });
  
  // SAVE AND RENDER SELECTED PRODUCTS
  $('body').on('click', '#products-modal #product-save', function() {
    let saved = include_products;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_products') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR PRODUCTS
  $('body').on('click', '.clear-products', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.products-modal-button').data('input');
    
    include_products = [];
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
</script>
@endpush