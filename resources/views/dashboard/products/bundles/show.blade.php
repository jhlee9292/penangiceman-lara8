@extends('layouts.dashboard.index')


@section('content')

{{-- FACEBOOK --}}
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v12.0&appId=248411993036876&autoLogAppEvents=1" nonce="2STzpikU"></script>

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.products.bundles.index') }}">Bundled Products</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $bundled_product->name }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">{{ $bundled_product->name }}</h1>
      </div>
      <div class="col-auto d-flex">
        <a class="btn btn-primary mt-5" href="{{ route('dashboard.products.bundles.edit', $bundled_product->id) }}">Edit</a>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
    <div class="sa-entity-layout__body">

      {{-- MAIN FORM --}}
      <div class="sa-entity-layout__main">

        {{-- BASIC INFORMATION --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Basic information</h3></div>

            <div class="row mb-4">
              <div class="col-3"><strong>Product name</strong></div>
              <div class="col">{{ $bundled_product->name }}</div>
            </div>

            <div class="row mb-4">
              <div class="col-3"><strong>Slug</strong></div>
              <div class="col">{{ $bundled_product->slug }}</div>
            </div>

            <div class="row mb-4">
              <div class="col">{!! $bundled_product->content !!}</div>
            </div>
          </div>
        </div>
          
        {{-- PRICING --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Pricing</h3></div>

            <div class="row mb-4">
              <div class="col-2"><strong>Selling price</strong></div>
              <div class="col-4">RM {{ number_format($bundled_product->sku->selling_price, 2) }}</div>
              
              <div class="col-2"><strong>Sale price</strong></div>
              <div class="col-4">RM {{ number_format($bundled_product->sku->sale_price, 2) }}</div>
            </div>
          </div>
        </div>

        {{-- INVENTORY --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Inventory</h3></div>

            <div class="row mb-4">
              <div class="col-2"><strong>SKU</strong></div>
              <div class="col-4">{{ $bundled_product->sku->sku }}</div>
              
              <div class="col-2"><strong>MFG#</strong></div>
              <div class="col-4">{{ $bundled_product->sku->mfg_part_number }}</div>
            </div>

            <div class="row mb-4">
              <div class="col-2"><strong>Stock</strong></div>
              <div class="col-4">{{ $bundled_product->sku->stock }}</div>
              
              <div class="col-2"><strong>Weight (g)</strong></div>
              <div class="col-4">{{ $bundled_product->sku->weight }}</div>
            </div>
          </div>
        </div>

        {{-- BUNDLED PRODUCTS --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h2 class="mb-0 fs-exact-18">Bundled products</h2></div>

            <table class="table table-borderless">
              <thead>
                <tr>
                  <th class="">Item</th>
                  <th class="">Stock</th>
                  <th class="">Selling price</th>
                  <th class="">Sale price</th>
                </tr>
              </thead>

              <tbody>
                @foreach($bundled_product->bundles as $bundled_item)
                <?php $product = $bundled_item->product; ?>
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg me-4">
                        <img src="{{ $product->the_image}}" width="40" height="40" alt="" />
                      </div>
                      
                      <div>
                        @if($product->type == "simple")
                        <a href="{{ route('dashboard.products.simples.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                        @elseif($product->type == "variant")
                        <a href="{{ route('dashboard.products.variants.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                        @elseif($product->type == "bundle")
                        <a href="{{ route('dashboard.products.bundles.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                        @endif
                      </div>
                    </div>
                  </td>
                  <td class="align-middle">
                    <div class="badge
                                @if($product->sku->stock >= 10) badge-sa-success
                                @elseif($product->sku->stock < 10 && $product->sku->stock > 0) badge-sa-warning
                                @elseif($product->sku->stock == 0) badge-sa-danger
                                @endif">
                      @if($product->sku->stock > 0) {{ $product->sku->stock }} In Stock
                      @else Out Of Stock
                      @endif
                    </div>
                  </td>
                  <td class="align-middle">
                    <div class="sa-price">
                      RM
                      {{ number_format($product->sku->selling_price, 2) }}
                    </div>
                  </td>
                  <td class="align-middle">
                    <div class="sa-price">
                      RM
                      {{ number_format($product->sku->sale_price, 2) }}
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
          
        {{-- FACEBOOK PLUGIN --}}
        <div class="card mb-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Facebook comments</h3></div>

            <div class="fb-comments" data-href="https://myletrik.com/product/{{ $bundled_product->slug }}" data-width="100%"></div>
          </div>
        </div>

      </div>

      {{-- SIDEBAR --}}
      <div class="sa-entity-layout__sidebar">

        {{-- STATS --}}
        <div class="card w-100">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Stats</h3></div>

            <div class="row text-center">
              <div class="col">
                <i class="fas fa-eye me-3"></i>
                {{ $bundled_product->views->sum('count') }}
              </div>
              <div class="col">
                <i class="fas fa-shopping-basket me-3"></i>
                {{ $bundled_product->user_relations->sum('sold') }}
              </div>
              <div class="col">
                <i class="fas fa-star me-3"></i>
                {{ number_format($bundled_product->user_relations->average('rate'), 1) }}
              </div>
            </div>
          </div>
        </div>

        {{-- DETAILS --}}
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="row mb-4">
              <div class="col-4"><strong>Status</strong></div>
              <div class="col">
                <div class="badge
                            @if($bundled_product->status == "draft") badge-sa-secondary
                            @elseif($bundled_product->status == "published") badge-sa-success
                            @endif">
                  @if($bundled_product->status == "draft") Draft
                  @elseif($bundled_product->status == "published") Published
                  @endif
                </div>
              </div>
            </div>
            
            @if(!empty($bundled_product->categories))
            <div class="row mb-4">
              <div class="col-4"><strong>Categories</strong></div>
              <div class="col">
                @foreach($bundled_product->categories as $category)
                <span class="badge badge-sa-secondary">{{ $category->name }}</span>
                @endforeach
              </div>
            </div>
            @endif
            
            @if(!empty($bundled_product->tags))
            <div class="row mb-4">
              <div class="col-4"><strong>Tags</strong></div>
              <div class="col">
                @foreach($bundled_product->tags as $tag)
                <span class="badge badge-sa-secondary">{{ $tag->name }}</span>
                @endforeach
              </div>
            </div>
            @endif
          </div>
        </div>

        {{-- IMAGE --}}
        @if($bundled_product->image)
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product image</h3></div>
            
            <img id="image-preview" class="w-100" src="{{ $bundled_product->the_image }}" />
          </div>
        </div>
        @endif

        {{-- GALLERY --}}
        @if(!empty($bundled_product->galleries))
        <div class="card w-100 mt-5">
          <div class="card-body p-5">
            <div class="mb-5"><h3 class="mb-0 fs-exact-18">Product gallery</h3></div>
            
            @foreach($bundled_product->galleries as $gallery)
            <img id="image-preview" class="w-25 me-3" src="{{ $gallery->the_image }}" />
            @endforeach
          </div>
        </div>
        @endif

      </div>

    </div>
  </div>
  
</div>

@endsection