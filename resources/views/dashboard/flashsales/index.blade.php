@extends('layouts.dashboard.index')


@push('header-scripts')
  {{-- Datatable min css --}}
  <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/datatables/css/dataTables.bootstrap5.min.css') }}" />
@endpush


@section('content')

<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Simple Products</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Products</h1>
      </div>
      <div class="col-auto d-flex">
        <a href="{{ route('dashboard.products.create') }}" class="btn btn-primary mt-5">Add New</a>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(!session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success">
    {{ session()->get('success') }}
  </div>
</div>
@endif

{{-- DATATABLE --}}
<div class="sa-layout__content mx-xxl-3 px-4 px-sm-5">
  <div class="card">
    
    {{-- SEARCH --}}
    <div class="p-4">
      <div class="row g-4">
        <div class="col">
          <input type="text"
                 placeholder="Start typing to search for products"
                 class="form-control form-control--search mx-auto"
                 id="table-search" />
        </div>
      </div>
    </div>

    <div class="sa-divider"></div>
    
    {{-- TABLE --}}
    <table class="sa-datatables-init" data-sa-search-input="#table-search">
      
      <thead>
        <tr>
          <th class="min-w-20x">Product</th>
          <th>Stock</th>
          <th>Price</th>
          <th>Views</th>
          <th>Sold</th>
          <th>Rating</th>
          <th>Status</th>
          <th data-orderable="false">Action</th>
        </tr>
      </thead>
      
      <tbody>
        @foreach($products as $product)
        <tr>
          <td>
            <div class="d-flex align-items-center">
              <a href="{{ route('dashboard.products.edit', $product->id) }}" class="me-4">
                <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                  <img src="{{ $product->the_image }}" width="40" height="40" alt="" />
                </div>
              </a>
              <div>
                <a href="{{ route('dashboard.products.edit', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                <div class="sa-meta mt-0">
                  <ul class="sa-meta__list">
                    <li class="sa-meta__item">
                      Type:
                      <span class="st-copy">
                        @if($product->type == "simple") {{ "Simple Product" }}
                        @elseif($product->type == "bundle") {{ "Bundled Product" }}
                        @else {{ "Variation Product" }}
                        @endif
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </td>
          <td><div class="badge badge-sa-success">{{ mt_rand(10,100) }}</div></td>
          <td>
            <div class="sa-price">
              <span class="sa-price__symbol">RM</span>
              <span class="sa-price__integer">{{ number_format($product->selling_price, 2, '.', '') }}</span>
            </div>
          </td>
          <td>
            {{-- make this action button --}}
            <a class="btn btn-primary" href="{{ route('dashboard.products.edit', $product->id) }}">
              <span><i class="fas fa-pen me-1"></i> Edit</span>
            </a>
          </td>
        </tr>
        @endforeach
      </tbody>
  </table>
    
  </div>
</div>

{{-- PAGINATION --}}
<div class="sa-layout__content mx-xxl-3 px-4 px-sm-5">
  <div class="card">
    <div class="p-4">
      <div class="row g-4">
        <div class="col">
          {!! $products->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>

@endsection


@push('footer-scripts')
  {{-- Datatable scripts --}}
  <script src="{{ asset('dashboard/assets/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('dashboard/assets/vendor/datatables/js/dataTables.bootstrap5.min.js') }}"></script>

  {{-- Init Datatable --}}
  <script>
    (function () {
      $.fn.DataTable.ext.pager.numbers_length = 5;
      //$.fn.DataTable.defaults.oLanguage.sInfo = "Showing _START_ to _END_ of _TOTAL_";
      $.fn.DataTable.defaults.oLanguage.sInfo = "";
      $.fn.DataTable.defaults.oLanguage.sLengthMenu = "Rows per page _MENU_";
      const template =
        "" +
        '<"sa-datatables"' +
        '<"sa-datatables__table"t>' +
        '<"sa-datatables__footer"' +
        '<"sa-datatables__pagination"p>' +
        '<"sa-datatables__controls"' +
        '<"sa-datatables__legend"i>' +
        '<"sa-datatables__divider">' +
        '<"sa-datatables__page-size"l>' +
        ">" +
        ">" +
        ">";
      $(".sa-datatables-init").each(function () {
        const tableSearchSelector = $(this).data("sa-search-input");
        const table = $(this).DataTable({
          dom: template,
          paging: false,
          ordering: true,
          drawCallback: function () {
            $(this.api().table().container())
              .find(".pagination")
              .addClass("pagination-sm");
          },
        });

        if (tableSearchSelector) {
          $(tableSearchSelector).on("input", function () {
            table.search(this.value).draw();
          });
        }
      });
    })();
  </script>
@endpush