@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.vouchers.index') }}">Vouchers</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $voucher->code }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Edit Voucher</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.vouchers.update', $voucher->id) }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Basic information</h3></div>
              
              <div class="mb-4">
                <label for="code" class="form-label">Voucher code</label>
                <input type="text" id="code" name="code" class="form-control" value="{{ old('code', $voucher->code) }}" />
              </div>
              
              <div class="mb-4">
                <label for="description" class="form-label">Description (optional)</label>
                <textarea id="description" name="description" class="form-control">{{ old('description', $voucher->description) }}</textarea>
              </div>
            </div>
          </div>
          
          {{-- VOUCHER DATA --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Voucher data</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="type" class="form-label">Voucher type</label>
                </div>
                
                <div class="col">
                  <select id="type" name="type" class="form-select">
                    <option value="discount" @if(old('type', $voucher->type) == "discount") selected @endif>Discount</option>
                    <option value="free_shipping" @if(old('type', $voucher->type) == "free_shipping") selected @endif>Free shipping</option>
                  </select>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="value" class="form-label">Voucher value (RM)</label>
                </div>
                
                <div class="col">
                  <input type="number"
                         id="value"
                         name="value"
                         class="form-control"
                         step="0.01"
                         placeholder="0"
                         value="{{ old('value', number_format($voucher->value, 2)) }}" />
                  <div class="form-text">
                    Value of the voucher.
                  </div>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="start_date" class="form-label">Start date</label>
                </div>
                
                <div class="col">
                  <input type="text"
                         id="start_date"
                         name="start_date"
                         class="form-control datepicker-here"
                         placeholder="YYYY-MM-DD"
                         value="{{ old('start_date', $voucher->start_date) }}"
                         data-auto-close="true"
                         data-language="en"
                         data-date-format="yyyy-mm-dd"
                         aria-label="Datepicker" />

                  <div class="form-text">
                    The voucher will start at 00:00:00 of this date.
                    Leave blank to start immediately.
                  </div>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="end_date" class="form-label">End date</label>
                </div>
                
                <div class="col">
                  <input type="text"
                         id="end_date"
                         name="end_date"
                         class="form-control datepicker-here"
                         placeholder="YYYY-MM-DD"
                         value="{{ old('end_date', $voucher->end_date) }}"
                         data-auto-close="true"
                         data-language="en"
                         data-date-format="yyyy-mm-dd"
                         aria-label="Datepicker" />

                  <div class="form-text">
                    The voucher will expire at 00:00:00 of this date.
                    Leave blank for no expiry.
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          {{-- USAGE LIMITS --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Usage limits</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="minimum_spend" class="form-label">Minimum spend (RM)</label>
                </div>
                
                <div class="col">
                  <input type="number"
                         id="minimum_spend"
                         name="minimum_spend"
                         class="form-control"
                         step="0.01"
                         placeholder="No minimum"
                         value="{{ old('minimum_spend', $voucher->minimum_spend) }}" />
                  
                  <div class="form-text">
                    Set the minimum spending in cart total allowed to use this voucher.
                    Leave blank for no minimum.
                  </div>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="limit_per_coupon" class="form-label">Usage limit per coupon</label>
                </div>
                
                <div class="col">
                  <input type="number"
                         id="limit_per_coupon"
                         name="limit_per_coupon"
                         class="form-control"
                         placeholder="Unlimited usage"
                         value="{{ old('limit_per_coupon', $voucher->limit_per_coupon) }}" />
                  
                  <div class="form-text">
                    Set how many times this voucher can be used.
                    Leave blank for unlimited usage.
                  </div>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="limit_per_user" class="form-label">Usage limit per user</label>
                </div>
                
                <div class="col">
                  <input type="number"
                         id="limit_per_user"
                         name="limit_per_user"
                         class="form-control"
                         placeholder="Unlimited usage"
                         value="{{ old('limit_per_user', $voucher->limit_per_user) }}" />
                  
                  <div class="form-text">
                    Set how many times this voucher can be used by ONE user.
                    Leave blank for unlimited usage.
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          {{-- PRODUCT RESTRICTIONS --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Include/Exclude products</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="include_products" class="form-label">Include products</label>
                </div>
                
                <div class="col">
                  <a class="products-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#products-modal" data-input="#include_products">
                    Add/Remove products
                  </a>
                  
                  <?php
                  $selected_include_products = $voucher->product_restrictions->where('include', 1);
                  $include_products = "";
                  
                  foreach($selected_include_products as $product) {
                    $include_products .= $product->product_id . ",";
                  }

                  $include_products = rtrim($include_products, ",");
                  ?>
                  <input type="hidden" id="include_products" name="include_products" value="{{ old('include_products', $include_products) }}" />
                  
                  <div class="form-text">
                    If set, voucher can only be applied to the selected products.
                  </div>
                  
                  <table class="table mt-4 @if($selected_include_products->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected product</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_include_products->isEmpty())
                        @foreach($selected_include_products as $selected)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $selected->product->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $selected->product->name }}
                                <div class="sa-meta mt-0">
                                  <ul class="sa-meta__list">
                                    <li class="sa-meta__item">SKU: {{ $selected->product->sku->sku }}</li>
                                    <li class="sa-meta__item">MFG#: {{ $selected->product->sku->mfg_part_number }}</li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-products text-danger" role="button">
                            Clear products
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="exclude_products" class="form-label">Exclude products</label>
                </div>
                
                <div class="col">
                  <a class="products-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#products-modal" data-input="#exclude_products">
                    Add/Remove products
                  </a>
                  
                  <?php
                  $selected_exclude_products = $voucher->product_restrictions->where('include', 0);
                  $exclude_products = "";
                  
                  foreach($selected_exclude_products as $product) {
                    $exclude_products .= $product->product_id . ",";
                  }
                  
                  $exclude_products = rtrim($exclude_products, ",");
                  ?>
                  <input type="hidden" id="exclude_products" name="exclude_products" value="{{ old('exclude_products', $exclude_products) }}" />
                  
                  <div class="form-text">
                    If set, voucher will not be applied to the selected products.
                  </div>
                  
                  <table class="table mt-4 @if($selected_exclude_products->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected product</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_exclude_products->isEmpty())
                        @foreach($selected_exclude_products as $selected)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $selected->product->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $selected->product->name }}
                                <div class="sa-meta mt-0">
                                  <ul class="sa-meta__list">
                                    <li class="sa-meta__item">SKU: {{ $selected->product->sku->sku }}</li>
                                    <li class="sa-meta__item">MFG#: {{ $selected->product->sku->mfg_part_number }}</li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-products text-danger" role="button">
                            Clear products
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
          {{-- CATEGORY RESTRICTIONS --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Include/Exclude categories</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="include_categories" class="form-label">Include categories</label>
                </div>
                
                <div class="col">
                  <a class="categories-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#categories-modal" data-input="#include_categories">
                    Add/Remove categories
                  </a>
                  
                  <?php
                  $selected_include_categories = $voucher->category_restrictions->where('include', 1);
                  $include_categories = "";
                  
                  foreach($selected_include_categories as $category) {
                    $include_categories .= $category->category_id . ",";
                  }
                  
                  $include_categories = rtrim($include_categories, ",")
                  ?>
                  <input type="hidden" id="include_categories" name="include_categories" value="{{ old('include_categories', $include_categories) }}" />
                  
                  <div class="form-text">
                    If set, voucher can only be applied to the selected categories.
                  </div>
                  
                  <table class="table mt-4 @if($selected_include_categories->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected categories</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_include_categories->isEmpty())
                        @foreach($selected_include_categories as $selected)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $selected->category->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $selected->category->the_name }}
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-categories text-danger" role="button">
                            Clear categories
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="exclude_categories" class="form-label">Exclude categories</label>
                </div>
                
                <div class="col">
                  <a class="categories-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#categories-modal" data-input="#exclude_categories">
                    Add/Remove categories
                  </a>
                  
                  <?php
                  $selected_exclude_categories = $voucher->category_restrictions->where('include', 0);
                  $exclude_categories = "";
                  
                  foreach($selected_exclude_categories as $category) {
                    $exclude_categories .= $category->category_id . ",";
                  }
                  
                  $exclude_categories = rtrim($exclude_categories, ",");
                  ?>
                  <input type="hidden" id="exclude_categories" name="exclude_categories" value="{{ old('exclude_categories', $exclude_categories) }}" />
                  
                  <div class="form-text">
                    If set, voucher will not be applied to the selected categories.
                  </div>
                  
                  <table class="table mt-4 @if($selected_exclude_categories->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected categories</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_exclude_categories->isEmpty())
                        @foreach($selected_exclude_categories as $selected)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $selected->category->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $selected->category->the_name }}
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-categories text-danger" role="button">
                            Clear categories
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
          {{-- BRAND RESTRICTIONS --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Include/Exclude brands</h3></div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="include_brands" class="form-label">Include brands</label>
                </div>
                
                <div class="col">
                  <a class="brands-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#brands-modal" data-input="#include_brands">
                    Add/Remove brands
                  </a>
                  
                  <?php
                  $selected_include_brands = $voucher->brand_restrictions->where('include', 1);
                  $include_brands = "";
                  
                  foreach($selected_include_brands as $brand) {
                    $include_brands .= $brand->brand_id . ",";
                  }
                  
                  $include_brands = rtrim($include_brands, ",")
                  ?>
                  <input type="hidden" id="include_brands" name="include_brands" value="{{ old('include_brands', $include_brands) }}" />
                  
                  <div class="form-text">
                    If set, voucher can only be applied to the selected brands.
                  </div>
                  
                  <table class="table mt-4 @if($selected_include_brands->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected brands</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_include_brands->isEmpty())
                        @foreach($selected_include_brands as $selected)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $selected->brand->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $selected->brand->name }}
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-brands text-danger" role="button">
                            Clear brands
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              
              <div class="form-group row mb-4">
                <div class="col-3">
                  <label for="exclude_brands" class="form-label">Exclude brands</label>
                </div>
                
                <div class="col">
                  <a class="brands-modal-button" role="button" data-bs-toggle="modal" data-bs-target="#brands-modal" data-input="#exclude_brands">
                    Add/Remove brands
                  </a>
                  
                  <?php
                  $selected_exclude_brands = $voucher->brand_restrictions->where('include', 0);
                  $exclude_brands = "";
                  
                  foreach($selected_exclude_brands as $brand) {
                    $exclude_brands .= $brand->brand_id . ",";
                  }
                  
                  $exclude_brands = rtrim($exclude_brands, ",");
                  ?>
                  <input type="hidden" id="exclude_brands" name="exclude_brands" value="{{ old('exclude_brands', $exclude_brands) }}" />
                  
                  <div class="form-text">
                    If set, voucher will not be applied to the selected brands.
                  </div>
                  
                  <table class="table mt-4 @if($selected_exclude_brands->isEmpty()) d-none @endif">
                    <thead>
                      <tr>
                        <th>Selected brands</th>
                      </tr>
                    </thead>

                    <tbody>
                      @if(!$selected_exclude_brands->isEmpty())
                        @foreach($selected_exclude_brands as $selected)
                        <tr>
                          <td>
                            <div class="d-flex align-items-center">
                              <img class="me-4" src="{{ $selected->brand->the_image }}" width="40" height="40" alt="" />

                              <div>
                                {{ $selected->brand->name }}
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                      @endif

                      <tr>
                        <td>
                          <a class="clear-brands text-danger" role="button">
                            Clear brands
                          </a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- STATUS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Status</h2></div>
              
              <div class="mb-4">
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="draft"
                         @if(old('status', $voucher->status) == "draft") checked @endif />
                  <span class="form-check-label">Draft</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="published"
                         @if(old('status', $voucher->status) == "published") checked @endif />
                  <span class="form-check-label">Published</span>
                </label>
                
                <label class="form-check @if($voucher->status) != "scheduled") d-none @endif">
                  <input type="radio"
                         class="form-check-input"
                         name="status"
                         value="scheduled"
                         @if(old('status', $voucher->status) == "scheduled") checked @endif />
                  <span class="form-check-label">Scheduled</span>
                </label>
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
  </form>
</div>

{{-- PRODUCTS MODAL --}}
<div id="products-modal" class="modal fade" tabindex="-1" aria-labelledby="products-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Products</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="product-search" class="form-control" placeholder="Search products" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Product</th>
            </tr>
          </thead>

          <tbody>
            {{-- PRODUCTS --}}
          </tbody>
        </table>

        <input type="hidden" id="product-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="product-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

{{-- CATEGORIES MODAL --}}
<div id="categories-modal" class="modal fade" tabindex="-1" aria-labelledby="categories-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Categories</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="category-search" class="form-control" placeholder="Search categories" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Category</th>
            </tr>
          </thead>

          <tbody>
            {{-- CATEGORIES --}}
          </tbody>
        </table>

        <input type="hidden" id="category-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="category-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

{{-- BRANDS MODAL --}}
<div id="brands-modal" class="modal fade" tabindex="-1" aria-labelledby="brands-modal" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content">

      <div class="modal-header">
        <h5 class="modal-title">Brands</h5>
        <button type="button" class="sa-close sa-close--modal" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>

      <div class="modal-body">
        <input type="text" id="brand-search" class="form-control" placeholder="Search brands" />

        <table class="table mt-5">
          <thead>
            <tr>
              <th style="width: 50px;"></th>
              <th>Category</th>
            </tr>
          </thead>

          <tbody>
            {{-- BRANDS --}}
          </tbody>
        </table>

        <input type="hidden" id="brand-page" value="1" />
      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" id="brand-save" class="btn btn-primary" data-bs-dismiss="modal">Save changes</button>
      </div>

    </div>
  </div>
</div>

@endsection


@push('footer-scripts')
<script>
  // TYPE
  $('#type').change(function() {
    if($(this).val() == "free_shipping") {
      $('#value').closest('.form-group').addClass('d-none');
    }
    else {
      $('#value').closest('.form-group').removeClass('d-none');
    }
  });
  
  // SCHEDULE
  $('#start_date').datepicker({
    onSelect: function(dateText) {
      var pick = new Date(dateText);
      var today = new Date();
      
      if(pick > today) {
        $('input[name=status][value="draft"]').attr('checked', false); // HAVE TO UNCHECK THIS DUE TO CHECKED BY DEFAULT
        $('input[name=status][value="scheduled"]').closest('label').removeClass('d-none');
        $('input[name=status][value="scheduled"]').next('span').text("Schedule: " + dateText);
        $('input[name=status][value="scheduled"]').attr('checked', true);
      }
      else {
        $('input[name=status][value="draft"]').attr('checked', true);
        $('input[name=status][value="scheduled"]').closest('label').addClass('d-none');
      }
    }
  });
  
  
  /********* RESTRICTIONS *********/
  let input = "";
  
  /*** PRODUCTS ***/
  let include_products = ($('#include_products').val()) ? $('#include_products').val().split(",") : [] ;
  let exclude_products = ($('#exclude_products').val()) ? $('#exclude_products').val().split(",") : [] ;
  
  // FETCH PRODUCTS
  $('.products-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#products-modal #product-page').val();
    let query = $('#products-modal #product-search').val();
    input = $(this).data('input');
    
    fetch_products(page, query);    
  });
  
  // SEARCH PRODUCTS
  $('#product-search').keyup(function() {
    let page = $('#products-modal #product-page').val();
    let query = $(this).val();

    fetch_products(page, query);    
  });
  
  // PAGINATE PRODUCTS
  $('body').on('click', '#products-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#products-modal #product-search').val();
    
    $('#products-modal #product-page').val(page);

    fetch_products(page, query);    
  });
  
  // FETCH PRODUCTS VIA AJAX
  function fetch_products(page, query) {
    let selected = (input == "#include_products") ? include_products : exclude_products ;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_products') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#products-modal .modal-body table tbody').html('');
        $('#products-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED PRODUCTS
  $('body').on('change', '#products-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      if(input == "#include_products") { // INCLUDE PRODUCTS
        include_products.push($(this).val());
      }
      else { // EXCLUDE PRODUCTS
        exclude_products.push($(this).val());
      }
    }
    else { // DESELECT
      if(input == "#include_products") { // INCLUDE PRODUCTS
        let index = include_products.indexOf($(this).val());
        include_products.splice(index, 1);
      }
      else { // EXCLUDE PRODUCTS
        let index = exclude_products.indexOf($(this).val());
        exclude_products.splice(index, 1);
      }
    }
  });
  
  // SAVE AND RENDER SELECTED PRODUCTS
  $('body').on('click', '#products-modal #product-save', function() {
    let saved = (input == "#include_products") ? include_products : exclude_products ;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_products') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR PRODUCTS
  $('body').on('click', '.clear-products', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.products-modal-button').data('input');
    
    if(target_input == "#include_products") { // INCLUDE PRODUCTS
      include_products = [];
    }
    else { // EXCLUDE PRODUCTS
      exclude_products = [];
    }
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
  
  
  /*** CATEGORIES ***/
  let include_categories = ($('#include_categories').val()) ? $('#include_categories').val().split(",") : [] ;
  let exclude_categories = ($('#exclude_categories').val()) ? $('#exclude_categories').val().split(",") : [] ;
  
  // FETCH CATEGORIES
  $('.categories-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#categories-modal #category-page').val();
    let query = $('#categories-modal #category-search').val();
    input = $(this).data('input');
    
    fetch_categories(page, query);    
  });
  
  // SEARCH CATEGORIES
  $('#category-search').keyup(function() {
    let page = $('#categories-modal #category-page').val();
    let query = $(this).val();

    fetch_categories(page, query);    
  });
  
  // PAGINATE CATEGORIES
  $('body').on('click', '#categories-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#categories-modal #category-search').val();
    
    $('#categories-modal #category-page').val(page);

    fetch_categories(page, query);    
  });
  
  // FETCH CATEGORIES VIA AJAX
  function fetch_categories(page, query) {
    let selected = (input == "#include_categories") ? include_categories : exclude_categories ;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_categories') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#categories-modal .modal-body table tbody').html('');
        $('#categories-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED CATEGORIES
  $('body').on('change', '#categories-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      if(input == "#include_categories") { // INCLUDE CATEGORIES
        include_categories.push($(this).val());
      }
      else { // EXCLUDE CATEGORIES
        exclude_categories.push($(this).val());
      }
    }
    else { // DESELECT
      if(input == "#include_categories") { // INCLUDE CATEGORIES
        let index = include_categories.indexOf($(this).val());
        include_categories.splice(index, 1);
      }
      else { // EXCLUDE CATEGORIES
        let index = exclude_categories.indexOf($(this).val());
        exclude_categories.splice(index, 1);
      }
    }
  });
  
  // SAVE AND RENDER SELECTED CATEGORIES
  $('body').on('click', '#categories-modal #category-save', function() {
    let saved = (input == "#include_categories") ? include_categories : exclude_categories ;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_categories') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR CATEGORIES
  $('body').on('click', '.clear-categories', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.categories-modal-button').data('input');
    
    if(target_input == "#include_categories") { // INCLUDE CATEGORIES
      include_categories = [];
    }
    else { // EXCLUDE CATEGORIES
      exclude_categories = [];
    }
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
  
  
  /*** BRANDS ***/
  let include_brands = ($('#include_brands').val()) ? $('#include_brands').val().split(",") : [] ;
  let exclude_brands = ($('#exclude_brands').val()) ? $('#exclude_brands').val().split(",") : [] ;
  
  // FETCH BRANDS
  $('.brands-modal-button').click(function(e) {
    e.preventDefault();
    
    let page = $('#brands-modal #brand-page').val();
    let query = $('#brands-modal #brand-search').val();
    input = $(this).data('input');
    
    fetch_brands(page, query);    
  });
  
  // SEARCH BRANDS
  $('#brand-search').keyup(function() {
    let page = $('#brands-modal #brand-page').val();
    let query = $(this).val();

    fetch_brands(page, query);    
  });
  
  // PAGINATE BRANDS
  $('body').on('click', '#brands-modal .pagination a', function(e) {
    e.preventDefault();
    
    let page = $(this).attr('href').split("?page=")[1];
    let query = $('#brands-modal #brand-search').val();
    
    $('#brands-modal #brand-page').val(page);

    fetch_brands(page, query);    
  });
  
  // FETCH BRANDS VIA AJAX
  function fetch_brands(page, query) {
    let selected = (input == "#include_brands") ? include_brands : exclude_brands ;
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_brands') }}",
      data: "page=" + page + "&query=" + query + "&selected=" + selected,
      success: function(data) {
        $('#brands-modal .modal-body table tbody').html('');
        $('#brands-modal .modal-body table tbody').html(data);
      }
    });
  }
  
  // CAPTURE SELECTED BRANDS
  $('body').on('change', '#brands-modal .form-check-input', function() {
    if(this.checked) { // SELECT
      if(input == "#include_brands") { // INCLUDE BRANDS
        include_brands.push($(this).val());
      }
      else { // EXCLUDE BRANDS
        exclude_brands.push($(this).val());
      }
    }
    else { // DESELECT
      if(input == "#include_brands") { // INCLUDE BRANDS
        let index = include_brands.indexOf($(this).val());
        include_brands.splice(index, 1);
      }
      else { // EXCLUDE BRANDS
        let index = exclude_brands.indexOf($(this).val());
        exclude_brands.splice(index, 1);
      }
    }
  });
  
  // SAVE AND RENDER SELECTED BRANDS
  $('body').on('click', '#brands-modal #brand-save', function() {
    let saved = (input == "#include_brands") ? include_brands : exclude_brands ;
    
    $(input).val(saved);
    
    $.ajax({
      url: "{{ route('dashboard.datatables.fetch_selected_brands') }}",
      data: "saved=" + saved,
      success: function(data) {
        $(input).siblings('table').removeClass('d-none');
        $(input).siblings('table').find('tbody').html('');
        $(input).siblings('table').find('tbody').html(data);
      }
    });
  });
  
  // CLEAR BRANDS
  $('body').on('click', '.clear-brands', function(e) {
    e.preventDefault();
    
    let target_input = $(this).closest('.table').siblings('.brands-modal-button').data('input');
    
    if(target_input == "#include_brands") { // INCLUDE BRANDS
      include_brands = [];
    }
    else { // EXCLUDE BRANDS
      exclude_brands = [];
    }
    
    $(target_input).val("");
    $(target_input).siblings('table').find('tbody').html('');
    $(target_input).siblings('table').addClass('d-none');
  });
</script>
@endpush