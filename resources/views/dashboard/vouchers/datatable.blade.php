@if(!$vouchers->isEmpty())
  
  @foreach($vouchers as $voucher)
  <tr class="align-middle">
    <td>
      <a href="{{ route('dashboard.vouchers.show', $voucher->id) }}" class="text-reset">{{ $voucher->code }}</a>
    </td>
    <td>
      <div class="badge badge-sa-success">
        @if($voucher->type == "discount" && empty($voucher->end_date)) Fixed Discount
        @elseif($voucher->type == "discount" && !empty($voucher->end_date)) Time Limited Discount
        @elseif($voucher->type == "free_shipping") Free Shipping
        @endif
      </div>
    </td>
    <td>
      <div class="sa-price">
        RM {{ number_format($voucher->value, 2) }}
      </div>
    </td>
    <td>
      {{ $voucher->start_date }}
    </td>
    <td>
      {{ $voucher->end_date }}
    </td>
    <td>
      <div class="badge
                  @if($voucher->status == "draft") badge-sa-secondary
                  @elseif($voucher->status == "scheduled") badge-sa-warning
                  @elseif($voucher->status == "published") badge-sa-success
                  @endif">
        @if($voucher->status == "draft") Draft
        @elseif($voucher->status == "scheduled") Scheduled
        @elseif($voucher->status == "published") Published
        @endif
      </div>
    </td>
    <td>
      <a class="btn btn-primary" href="{{ route('dashboard.vouchers.edit', $voucher->id) }}">
        <i class="fas fa-pen"></i>
      </a>
      <a class="btn btn-danger"
         href="{{ route('dashboard.vouchers.destroy', $voucher->id) }}"
         onclick="return confirm('Are you sure you want to delete this voucher?')">
        <i class="fas fa-trash"></i>
      </a>
    </td>
  </tr>
  @endforeach

  <tr>
    <td colspan="7">
      <div class="p-0 pt-4">
        {{ $vouchers->links() }}
      </div>
    </td>
  </tr>
  
@else

  <tr>
    <td colspan="7">
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif