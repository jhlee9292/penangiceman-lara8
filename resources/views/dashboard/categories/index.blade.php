@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Categories</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Categories</h1>
      </div>
      <div class="col-auto d-flex">
        <a href="{{ route('dashboard.categories.create') }}" class="btn btn-primary mt-5">Add New</a>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- DATATABLE --}}
<div class="mx-xxl-3 px-4 px-sm-5">

  {{-- SEARCH --}}
  <div class="card p-4">
    <div class="row g-4">
      <div class="col">
        <input type="text"
               id="table-search"
               placeholder="Search categories"
               class="form-control form-control--search mx-auto" />
      </div>
    </div>
  </div>

  {{-- TABLE --}}
  <div class="card mt-5 p-4">
    <table class="sa-datatables-init" data-sa-search-input="#table-search">
    <thead>
      <tr>
        <th class="min-w-20x">Category</th>
        <th class="w-min">Product</th>
        <th class="" data-orderable="false">Action</th>
      </tr>
    </thead>

    <tbody>

      {{-- TIER 1 CATEGORY --}}
      @foreach($t1_categories as $t1_category)
      <tr>
        <td>
          <div class="d-flex align-items-center mb-1">                  
            <a href="{{ route('dashboard.categories.edit', $t1_category->id) }}" class="me-4">
              <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                <img src="{{ $t1_category->the_image }}" width="40" height="40" alt="" style="border:1px solid #eaeaea;"/>
              </div>
            </a>
            <div>
              <a href="{{ route('dashboard.categories.edit', $t1_category->id) }}" class="text-reset">{{ $t1_category->name }}</a>
            </div>
          </div>
        </td>
        <td>
          <div class="badge
                      @if($t1_category->products()->count() > 0) badge-sa-success
                      @else badge-sa-danger
                      @endif">
            {{ $t1_category->products()->count() }} Product{{ ($t1_category->products()->count() > 1) ? "s" : "" ; }}
          </div>
        </td>
        <td>
          <a class="btn btn-primary" href="{{ route('dashboard.categories.edit', $t1_category->id) }}">
            <i class="fas fa-pen"></i>
          </a>
          <a class="btn btn-danger"
             href="{{ route('dashboard.categories.destroy', $t1_category->id) }}"
             onclick="return confirm('Are you sure you want to delete this category?')">
            <i class="fas fa-trash"></i>
          </a>
        </td>
      </tr>

      {{-- TIER 2 CATEGORY --}}
      <?php $t2_categories = App\Models\Category::where('parent_id', $t1_category->id)->get(); ?>
      @foreach($t2_categories as $t2_category)
      <tr>
        <td>
          <div class="d-flex align-items-center ms-5 mb-1">                  
            <a href="{{ route('dashboard.categories.edit', $t2_category->id) }}" class="me-4">
              <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                <img src="{{ $t2_category->the_image}}" width="40" height="40" alt="" style="border:1px solid #eaeaea;"/>
              </div>
            </a>
            <div>
              <a href="{{ route('dashboard.categories.edit', $t2_category->id) }}" class="text-reset">{{ $t2_category->name }}</a>
            </div>
          </div>
        </td>
        <td>
          <div class="badge
                      @if($t2_category->products()->count() > 0) badge-sa-success
                      @else badge-sa-danger
                      @endif">
            {{ $t2_category->products()->count() }} Product{{ ($t2_category->products()->count() > 1) ? "s" : "" ; }}
          </div>
        </td>
        <td>
          <a class="btn btn-primary" href="{{ route('dashboard.categories.edit', $t2_category->id) }}">
            <i class="fas fa-pen"></i>
          </a>
          <a class="btn btn-danger"
             href="{{ route('dashboard.categories.destroy', $t2_category->id) }}"
             onclick="return confirm('Are you sure you want to delete this category?')">
            <i class="fas fa-trash"></i>
          </a>
        </td>
      </tr>

      {{-- TIER 3 CATEGORY --}}
      <?php $t3_categories = App\Models\Category::where('parent_id', $t2_category->id)->get(); ?>
      @foreach($t3_categories as $t3_category)
      <tr>
        <td>
          <div class="ms-5">
            <div class="d-flex align-items-center ms-5 mb-1">                  
              <a href="{{ route('dashboard.categories.edit', $t3_category->id) }}" class="me-4">
                <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                  <img src="{{ $t3_category->the_image}}" width="40" height="40" alt="" style="border:1px solid #eaeaea;"/>
                </div>
              </a>
              <div>
                <a href="{{ route('dashboard.categories.edit', $t3_category->id) }}" class="text-reset">{{ $t3_category->name }}</a>
              </div>
            </div>
          </div>
        </td>
        <td>
          <div class="badge
                      @if($t3_category->products()->count() > 0) badge-sa-success
                      @else badge-sa-danger
                      @endif">
            {{ $t3_category->products()->count() }} Product{{ ($t3_category->products()->count() > 1) ? "s" : "" ; }}
          </div>
        </td>
        <td>
          <a class="btn btn-primary" href="{{ route('dashboard.categories.edit', $t3_category->id) }}">
            <i class="fas fa-pen"></i>
          </a>
          <a class="btn btn-danger"
             href="{{ route('dashboard.categories.destroy', $t3_category->id) }}"
             onclick="return confirm('Are you sure you want to delete this category?')">
            <i class="fas fa-trash"></i>
          </a>
        </td>
      </tr>
      @endforeach

      @endforeach

      @endforeach

    </tbody>
  </table>
  </div>

</div>

@endsection


@push('footer-scripts')
<script>
  // DATATABLE
  (function () {
    $.fn.DataTable.ext.pager.numbers_length = 5;
    $.fn.DataTable.defaults.oLanguage.sInfo = "Showing _START_ to _END_ of _TOTAL_";
    $.fn.DataTable.defaults.oLanguage.sLengthMenu = "Rows per page _MENU_";
    const template =
      "" +
      '<"sa-datatables"' +
      '<"sa-datatables__table"t>' +
      '<"sa-datatables__footer"' +
      '<"sa-datatables__pagination"p>' +
      '<"sa-datatables__controls"' +
      '<"sa-datatables__legend"i>' +
      '<"sa-datatables__divider">' +
      '<"sa-datatables__page-size"l>' +
      ">" +
      ">" +
      ">";
    $(".sa-datatables-init").each(function () {
      const tableSearchSelector = $(this).data("sa-search-input");
      const table = $(this).DataTable({
        dom: template,
        paging: false,
        ordering: false,
        drawCallback: function () {
          $(this.api().table().container())
            .find(".pagination")
            .addClass("pagination-sm");
        },
      });

      if (tableSearchSelector) {
        $(tableSearchSelector).on("input", function () {
          table.search(this.value).draw();
        });
      }
    });
  })();
</script>
@endpush