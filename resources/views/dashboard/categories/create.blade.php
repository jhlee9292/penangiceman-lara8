@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.categories.index') }}">Categories</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add New</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Add New Category</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.categories.store') }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Basic information</h2></div>
              
              <div class="mb-4">
                <label for="name" class="form-label">Category name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" />
              </div>
              
              <div class="mb-4">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug') }}" />
                <div class="form-text">Slug will be auto-generated.</div>
              </div>
              
              <div class="mb-4">
                <label for="parent-category" class="form-label">Parent category</label>
                <select id="parent-category" name="parent_category" class="sa-select form-select">
                  <option value="0">None</option>
                  
                  {{-- TIER 1 CATEGORY --}}
                  @foreach($t1_categories as $t1_category)
                  <option value="{{ $t1_category->id }}"
                          @if(old('parent_category') == $t1_category->id) selected @endif>{{ $t1_category->name }}</option>
                  
                  {{-- TIER 2 CATEGORY --}}
                  <?php $t2_categories = App\Models\Category::select('id', 'name')->where('parent_id', $t1_category->id)->get(); ?>
                  @foreach($t2_categories as $t2_category)
                  <option value="{{ $t2_category->id }}"
                          @if(old('parent_category') == $t2_category->id) selected @endif> — {{ $t2_category->name }}</option>
                  
                  {{-- TIER 3 CATEGORY --}}
                  <?php $t3_categories = App\Models\Category::select('id', 'name')->where('parent_id', $t2_category->id)->get(); ?>
                  @foreach($t3_categories as $t3_category)
                  <option value="{{ $t3_category->id }}"
                          @if(old('parent_category') == $t3_category->id) selected @endif> —— {{ $t3_category->name }}</option>
                  @endforeach
                  @endforeach
                  @endforeach
                  
                </select>
              </div>
            </div>
          </div>
          
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- IMAGE --}}
          <div class="card w-100">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Category image</h3></div>
              
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="d-none w-100 mb-4" src="" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Add image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-none">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
  </form>
</div>

@endsection


@push('footer-scripts')
<script>
  // SLUGIFY
  $('#name').change(function(e) {
    var url = '{{ route('dashboard.slugify', "name") }}';
    url = url.replace('name', $(this).val());

    $.get(url,
      function(data) {
        $('#slug').val(data);
      }
    );
  });
  
  // PREVIEW IMAGE
  $('#image').change(function() {
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
      
    }
  });
  
  // REMOVE PREVIEW
  $('#image-remove a').click(function() {
    $('#image').val(null);
    $('#image-remove').attr('class', 'd-none');
    $('#image-add').html('Add image');
    $('#image-preview').addClass('d-none').attr('src', '');
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
      
    }
  });
</script>
@endpush