@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Vimeo</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Vimeo</h1>
      </div>
    </div>
  </div>
</div>

{{-- SANDBOX --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  <form id="form" action="{{ route('dashboard.vimeo.upload') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        <div class="sa-entity-layout__main">
          <div class="card p-5">
            <div class="input-group">
              <!--input type="file" name="file" class="form-control" accept="video/*" /-->
              <input type="text" name="url" class="form-control" value="https://mdev.asia/baakua/wp-content/uploads/Test.mp4" />
              <button type="submit" class="btn btn-primary w-25">Upload</button>
            </div>
            <div class="input-group mt-4">
              @if($response)
              <a href="{{ $response['body']['link'] }}" target="_blank">{{ $response['body']['link'] }}</a>
              @endif
            </div>
          </div>
        </div>
        
        <div class="sa-entity-layout__sidebar">
          
        </div>
        
      </div>
    </div>
  </form>
  
</div>

@endsection
