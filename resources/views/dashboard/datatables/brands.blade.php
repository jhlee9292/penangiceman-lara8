@if(!$brands->isEmpty())
  
  @foreach($brands as $brand)
  <tr>
    <td class="align-middle text-center">
      <input type="checkbox"
             class="form-check-input"
             value="{{ $brand->id }}"
             @foreach($selected as $id)
               @if($id == $brand->id) checked @endif
             @endforeach />
    </td>
    <td>
      <div class="d-flex align-items-center">
        <img class="me-4" src="{{ $brand->the_image}}" width="40" height="40" alt="" />
        
        <div>
          {{ $brand->name }}
        </div>
      </div>
    </td>
  </tr>
  @endforeach

  <tr>
    <td colspan="2">
      <div class="p-0 pt-4">
        {{ $brands->links() }}
      </div>
    </td>
  </tr>

@else

  <tr>
    <td colspan="2">
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif