@if(!$categories->isEmpty())
  
  @foreach($categories as $category)
  <tr>
    <td class="align-middle text-center">
      <input type="checkbox"
             class="form-check-input"
             value="{{ $category->id }}"
             @foreach($selected as $id)
               @if($id == $category->id) checked @endif
             @endforeach />
    </td>
    <td>
      <div class="d-flex align-items-center">
        <img class="me-4" src="{{ $category->the_image}}" width="40" height="40" alt="" />
        
        <div>
          {{ $category->the_name }}
        </div>
      </div>
    </td>
  </tr>
  @endforeach

  <tr>
    <td colspan="2">
      <div class="p-0 pt-4">
        {{ $categories->links() }}
      </div>
    </td>
  </tr>

@else

  <tr>
    <td colspan="2">
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif