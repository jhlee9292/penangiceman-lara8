@if(!$brands->isEmpty())
  
  @foreach($brands as $brand)
  <tr>
    <td>
      <div class="d-flex align-items-center">
        <img class="me-4" src="{{ $brand->the_image}}" width="40" height="40" alt="" />
        
        <div>
          {{ $brand->name }}
        </div>
      </div>
    </td>
  </tr>
  @endforeach
  
  <tr>
    <td>
      <a class="clear-brands text-danger" role="button">
        Clear brands
      </a>
    </td>
  </tr>

@else

  <tr>
    <td>
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif