@if(!$products->isEmpty())
  
  @foreach($products as $product)
  <tr>
    <td>
      <div class="d-flex align-items-center">
        <img class="me-4" src="{{ $product->the_image }}" width="40" height="40" alt="" />
        
        <div>
          {{ $product->name }}
          <div class="sa-meta mt-0">
            <ul class="sa-meta__list">
              <li class="sa-meta__item">SKU: {{ $product->sku->sku }}</li>
              <li class="sa-meta__item">MFG#: {{ $product->sku->mfg_part_number }}</li>
            </ul>
          </div>
        </div>
      </div>
    </td>
  </tr>
  @endforeach
  
  <tr>
    <td>
      <a class="clear-products text-danger" role="button">
        Clear products
      </a>
    </td>
  </tr>

@else

  <tr>
    <td>
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif