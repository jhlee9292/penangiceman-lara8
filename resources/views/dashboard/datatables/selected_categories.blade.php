@if(!$categories->isEmpty())
  
  @foreach($categories as $category)
  <tr>
    <td>
      <div class="d-flex align-items-center">
        <img class="me-4" src="{{ $category->the_image}}" width="40" height="40" alt="" />
        
        <div>
          {{ $category->the_name }}
        </div>
      </div>
    </td>
  </tr>
  @endforeach
  
  <tr>
    <td>
      <a class="clear-categories text-danger" role="button">
        Clear categories
      </a>
    </td>
  </tr>

@else

  <tr>
    <td>
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif