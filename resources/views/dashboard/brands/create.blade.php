@extends('layouts.dashboard.index')

@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.brands.index') }}">Brands</a></li>
            <li class="breadcrumb-item active" aria-current="page">Add New</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Add New Brand</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.brands.store') }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Basic information</h2></div>
              
              <div class="mb-4">
                <label for="name" class="form-label">Brand name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name') }}" />
              </div>
              
              <div class="mb-4">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug') }}" />
                <div class="form-text">Slug will be auto-generated.</div>
              </div>
            </div>
          </div>
          
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- STATUS --}}
          <div class="card w-100">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Status</h2></div>
              
              <div class="mb-4">
                <label class="form-check">
                  <input type="radio" name="status" class="form-check-input" value="ACTIVE" checked />
                  <span class="form-check-label">Active</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         name="status"
                         class="form-check-input"
                         value="INACTIVE"
                         @if(old('status') == "INACTIVE") checked @endif />
                  <span class="form-check-label">Inactive</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         name="status"
                         class="form-check-input"
                         value="OBSOLETE"
                         @if(old('status') == "OBSOLETE") checked @endif />
                  <span class="form-check-label">Obsolete</span>
                </label>
              </div>
            </div>
          </div>
          
          {{-- IMAGE --}}
          <div class="card w-100 mt-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Brand image</h3></div>
              
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="d-none w-100 mb-4" src="" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Add image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-none">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
              </div>
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
  </form>
</div>

@endsection


@push('footer-scripts')
<script>
  // SLUGIFY
  $('#name').change(function(e) {
    var url = '{{ route('dashboard.slugify', "name") }}';
    url = url.replace('name', $(this).val());

    $.get(url,
      function(data) {
        $('#slug').val(data);
      }
    );
  });
  
  // PREVIEW IMAGE
  $('#image').change(function() {
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
      
    }
  });
  
  // REMOVE PREVIEW
  $('#image-remove a').click(function() {
    $('#image').val(null);
    $('#image-remove').attr('class', 'd-none');
    $('#image-add').html('Add image');
    $('#image-preview').addClass('d-none').attr('src', '');
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
      
    }
  });
</script>
@endpush