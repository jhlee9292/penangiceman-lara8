@if(!$brands->isEmpty())
  
  @foreach($brands as $brand)
  <tr class="align-middle">
    <td>
      <div class="d-flex align-items-center">
        <a href="{{ route('dashboard.brands.edit', $brand->id) }}" class="me-4">
          <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
            <img src="{{ $brand->the_image}}" width="40" height="40" alt="" />
          </div>
        </a>
        <div>
          <a href="{{ route('dashboard.brands.edit', $brand->id) }}" class="text-reset">{{ $brand->name }}</a>
        </div>
      </div>
    </td>
    <td>
      <div class="badge
                  @if($brand->skus->count() > 0) badge-sa-success
                  @else badge-sa-danger
                  @endif">
        {{ $brand->skus->count() }} Product{{ ($brand->skus->count() > 1) ? "s" : "" ; }}
      </div>
    </td>
    <td>
      <div class="badge
                  @if($brand->status == "ACTIVE") badge-sa-success
                  @elseif($brand->status == "INACTIVE") badge-sa-warning
                  @elseif($brand->status == "OBSOLETE") badge-sa-secondary
                  @endif">
        @if($brand->status == "ACTIVE") Active
        @elseif($brand->status == "INACTIVE") Inactive
        @elseif($brand->status == "OBSOLETE") Obsolete
        @endif
      </div>
    </td>
    <td>
      <a class="btn btn-primary" href="{{ route('dashboard.brands.edit', $brand->id) }}">
        <i class="fas fa-pen"></i>
      </a>
      <a class="btn btn-danger"
         href="{{ route('dashboard.brands.destroy', $brand->id) }}"
         onclick="return confirm('Are you sure you want to delete this brand?')">
        <i class="fas fa-trash"></i>
      </a>
    </td>
  </tr>
  @endforeach

  <tr>
    <td colspan="4">
      <div class="p-0 pt-4">
        {{ $brands->links() }}
      </div>
    </td>
  </tr>
  
@else

  <tr>
    <td colspan="4">
      <div class="p-5 text-center">
        Item not found.
      </div>
    </td>
  </tr>

@endif