@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('dashboard.brands.index') }}">Brands</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $brand->name }}</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Edit Brand</h1>
      </div>
      <div class="col-auto d-flex">
        <button class="btn btn-primary mt-5" onClick="document.forms['form'].submit();">Save</button>
      </div>
    </div>
  </div>
</div>

{{-- ALERT --}}
@if(session()->get('success'))
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="alert alert-success alert-dismissible fade show">
    {{ session()->get('success') }}
    <a href="" class="close" data-bs-dismiss="alert">&times;</a>
  </div>
</div>
@endif

{{-- FORM --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  {{-- ERRORS --}}
  @if($errors->any())
  <div class="alert alert-danger">
    @foreach($errors->all() as $error)
    {{ $error }}<br />
    @endforeach
  </div>
  @endif
  
  <form id="form"
        action="{{ route('dashboard.brands.update', $brand->id) }}"
        method="post"
        enctype="multipart/form-data">
    @csrf
    @method('PUT')
    
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        {{-- MAIN FORM --}}
        <div class="sa-entity-layout__main">
          
          {{-- BASIC INFORMATION --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Basic information</h2></div>
              
              <div class="mb-4">
                <label for="name" class="form-label">Brand name</label>
                <input type="text" id="name" name="name" class="form-control" value="{{ old('name', $brand->name) }}" />
              </div>
              
              <div class="mb-4">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" id="slug" name="slug" class="form-control" value="{{ old('slug', $brand->slug) }}" />
              </div>
            </div>
          </div>
          
          {{-- LINKED PRODUCTS --}}
          <div class="card mb-5">
            <div class="card-body p-5">
              <h2 class="mb-0 fs-exact-18">Linked products</h2>
            </div>
            
            <table class="sa-datatables-init">
              <thead>
                <tr>
                  <th class="min-w-20x">Product</th>
                  <th class="w-min">Type</th>
                  <th class="w-min">Status</th>
                  <th class="min-w-10x" data-orderable="false">Action</th>
                </tr>
              </thead>

              <tbody>

                @foreach($brand->products as $product)
                <tr>
                  <td>
                    <div class="d-flex align-items-center">
                      <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg me-4">
                        <img src="{{ $product->the_image}}" width="40" height="40" alt="" />
                      </div>
                      
                      <div>
                        @if($product->type == "simple")
                        <a href="{{ route('dashboard.products.simples.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                        @elseif($product->type == "variant")
                        <a href="{{ route('dashboard.products.variants.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                        @elseif($product->type == "bundle")
                        <a href="{{ route('dashboard.products.bundles.show', $product->id) }}" class="text-reset">{{ $product->name }}</a>
                        @endif
                        
                        <div class="sa-meta mt-0">
                          <ul class="sa-meta__list">
                            <li class="sa-meta__item">SKU: {{ $product->sku->sku }}</li>
                            <li class="sa-meta__item">MFG#: {{ $product->sku->mfg_part_number }}</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </td>
                  <td>
                    <div class="badge badge-sa-success">
                      @if($product->type == "simple") Simple
                      @elseif($product->type == "variant") Variation
                      @elseif($product->type == "bundle") Bundled
                      @endif
                    </div>
                  </td>
                  <td>
                    <div class="badge
                                @if($product->status == "draft") badge-sa-secondary
                                @elseif($product->status == "published") badge-sa-success
                                @endif">
                      @if($product->status == "draft") Draft
                      @elseif($product->status == "published") Published
                      @endif
                    </div>
                  </td>
                  <td>
                    @if($product->type == "simple")
                    <a class="btn btn-primary" href="{{ route('dashboard.products.simples.edit', $product->id) }}">
                      <i class="fas fa-pen"></i>
                    </a>
                    <a class="btn btn-danger"
                       href="{{ route('dashboard.products.simples.destroy', $product->id) }}"
                       onclick="return confirm('Are you sure you want to delete this product?')">
                      <i class="fas fa-trash"></i>
                    </a>
                    @elseif($product->type == "variant")
                    <a class="btn btn-primary" href="{{ route('dashboard.products.variants.edit', $product->id) }}">
                      <i class="fas fa-pen"></i>
                    </a>
                    <a class="btn btn-danger"
                       href="{{ route('dashboard.products.variants.destroy', $product->id) }}"
                       onclick="return confirm('Are you sure you want to delete this product?')">
                      <i class="fas fa-trash"></i>
                    </a>
                    @elseif($product->type == "bundle")
                    <a class="btn btn-primary" href="{{ route('dashboard.products.bundles.edit', $product->id) }}">
                      <i class="fas fa-pen"></i>
                    </a>
                    <a class="btn btn-danger"
                       href="{{ route('dashboard.products.bundles.destroy', $product->id) }}"
                       onclick="return confirm('Are you sure you want to delete this product?')">
                      <i class="fas fa-trash"></i>
                    </a>
                    @endif
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          
        </div>
        
        {{-- SIDEBAR --}}
        <div class="sa-entity-layout__sidebar">
          
          {{-- STATUS --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h2 class="mb-0 fs-exact-18">Status</h2></div>
              
              <div class="mb-4">
                <label class="form-check">
                  <input type="radio"
                         name="status"
                         class="form-check-input" value="ACTIVE"
                         @if(old('status', $brand->status) == "ACTIVE") checked @endif />
                  <span class="form-check-label">Active</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         name="status"
                         class="form-check-input"
                         value="INACTIVE"
                         @if(old('status', $brand->status) == "INACTIVE") checked @endif />
                  <span class="form-check-label">Inactive</span>
                </label>
                
                <label class="form-check">
                  <input type="radio"
                         name="status"
                         class="form-check-input"
                         value="OBSOLETE"
                         @if(old('status', $brand->status) == "OBSOLETE") checked @endif />
                  <span class="form-check-label">Obsolete</span>
                </label>
              </div>
            </div>
          </div>
          
          {{-- IMAGE --}}
          <div class="card w-100 mb-5">
            <div class="card-body p-5">
              <div class="mb-5"><h3 class="mb-0 fs-exact-18">Brand image</h3></div>
              
              @if(!$brand->image)
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="d-none w-100 mb-4" src="" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Add image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-none">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
              </div>
              @else
              <div class="mt-4 mb-n2">
                <img id="image-preview" class="w-100 mb-4" src="{{ $brand->the_image }}" />
                
                <label for="image" class="form-label">
                  <a id="image-add" class="image-link" role="button">Change image</a>
                </label>
                
                <input type="file" id="image" name="image" class="form-control d-none" accept="image/*" />
                
                <div id="image-remove" class="d-inline">
                  / <a class="text-danger" role="button">Remove image</a>
                </div>
                
                <input type="hidden" id="image-action" name="image_action" value="" />
              </div>
              @endif
              
            </div>
          </div>
          
        </div>
        
      </div>
    </div>
    
  </form>
</div>

@endsection


@push('footer-scripts')
<script>
  // DATATABLE
  (function () {
    $.fn.DataTable.ext.pager.numbers_length = 5;
    //$.fn.DataTable.defaults.oLanguage.sInfo = "Showing _START_ to _END_ of _TOTAL_";
    $.fn.DataTable.defaults.oLanguage.sInfo = "";
    $.fn.DataTable.defaults.oLanguage.sLengthMenu = "Rows per page _MENU_";
    const template =
      "" +
      '<"sa-datatables"' +
      '<"sa-datatables__table"t>' +
      '<"sa-datatables__footer"' +
      '<"sa-datatables__pagination"p>' +
      '<"sa-datatables__controls"' +
      '<"sa-datatables__legend"i>' +
      '<"sa-datatables__divider">' +
      '<"sa-datatables__page-size"l>' +
      ">" +
      ">" +
      ">";
    $(".sa-datatables-init").each(function () {
      const tableSearchSelector = $(this).data("sa-search-input");
      const table = $(this).DataTable({
        dom: template,
        paging: false,
        ordering: true,
        drawCallback: function () {
          $(this.api().table().container())
            .find(".pagination")
            .addClass("pagination-sm");
        },
      });

      if (tableSearchSelector) {
        $(tableSearchSelector).on("input", function () {
          table.search(this.value).draw();
        });
      }
    });
  })();
  
  // PREVIEW IMAGE
  $('#image').change(function() {
    var file = $(this).get(0).files[0];
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
        $('#image-action').val('DELETE');
      }
      reader.readAsDataURL(file);
    }
  });
  
  // REMOVE PREVIEW
  $('#image-remove a').click(function() {
    $('#image').val(null);
    $('#image-remove').attr('class', 'd-none');
    $('#image-add').html('Add image');
    $('#image-preview').addClass('d-none').attr('src', '');
    $('#image-action').val('DELETE');
    
    if(file) {
      var reader = new FileReader();
      reader.onload = function() {
        $('#image-preview').attr('src', reader.result).removeClass('d-none');
        $('#image-add').html('Change image');
        $('#image-remove').attr('class', 'd-inline');
      }
      reader.readAsDataURL(file);
      
    }
  });
</script>
@endpush