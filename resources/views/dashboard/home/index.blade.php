@extends('layouts.dashboard.index')

@section('content')

{{-- Breadcrumb --}}
<div class="py-5">



  <div class="row g-4 align-items-center mb-5">
    <div class="col"><h1 class="h3 m-0">Dashboard</h1></div>
  </div>

  <div class="row g-4 g-xl-5">
    @include('dashboard.home.partials.sales-widget')
    @include('dashboard.home.partials.sales-chart')
    @include('dashboard.home.partials.top-sales-widget')
    @include('dashboard.home.partials.orders-widget')
  </div>

</div>


@endsection