<div class="col-12 col-xxl-9 d-flex">
  <div class="card flex-grow-1 saw-table">
      <div class="sa-widget-header saw-table__header">
          <h2 class="sa-widget-header__title">Recent orders 
            <span class="ms-2" style="font-size: smaller;"><a href="#">View all</a></span>
          </h2>
      </div>
      <div class="saw-table__body sa-widget-table text-nowrap">
          <table>
              <thead>
                  <tr>
                      <th>No.</th>
                      <th>Customer</th>
                      <th>Date</th>
                      <th>Total</th>
                      <th>Status</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td><a href="#" class="text-reset">OD2201-0745</a></td>
                      <td>
                          <div class="d-flex align-items-center">
                              <div><a href="#" class="text-reset">Giordano Bruno</a></div>
                          </div>
                      </td>
                      <td>2022-11-02</td>
                      <td>RM 2,742.00</td>
                      <td>
                        <div class="d-flex fs-6"><div class="badge badge-sa-primary">Pending</div></div>
                    </td>
                  </tr>
                  <tr>
                      <td><a href="#" class="text-reset">OD2201-0513</a></td>
                      <td>
                          <div class="d-flex align-items-center">
                              <div><a href="#" class="text-reset">Hans Weber</a></div>
                          </div>
                      </td>
                      <td>2022-09-05</td>
                      <td>RM 204.00</td>
                      <td>
                        <div class="d-flex fs-6"><div class="badge badge-sa-warning">Hold</div></div>
                    </td>
                  </tr>
                  <tr>
                      <td><a href="#" class="text-reset">OD2201-0507</a></td>
                      <td>
                          <div class="d-flex align-items-center">
                              <div><a href="#" class="text-reset">Andrea Rossi</a></div>
                          </div>
                      </td>
                      <td>2022-08-21</td>
                      <td>RM 5,039.00</td>
                      <td>
                        <div class="d-flex fs-6"><div class="badge badge-sa-primary">Pending</div></div>
                      </td>
                  </tr>
                  <tr>
                      <td><a href="#" class="text-reset">OD2201-0104</a></td>
                      <td>
                          <div class="d-flex align-items-center">
                              <div><a href="#" class="text-reset">Richard Feynman</a></div>
                          </div>
                      </td>
                      <td>2020-06-22</td>
                      <td>RM 79.00</td>
                      <td>
                        <div class="d-flex fs-6"><div class="badge badge-sa-danger">Canceled</div></div>
                      </td>
                  </tr>
              </tbody>
          </table>
      </div>
  </div>
</div>