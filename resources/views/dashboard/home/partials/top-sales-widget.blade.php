<div class="col-12 col-lg-6 col-xxl-3 d-flex">
  <div class="card flex-grow-1">
      <div class="card-body">
          <div class="sa-widget-header">
              <h2 class="sa-widget-header__title">Top Sales, January 2022</h2>
          </div>
      </div>

      <ul class="list-group list-group-flush">
          @for ($i = 0; $i < 6; $i++)
          <li class="list-group-item py-2 px-4">
            <div class="d-flex align-items-center py-2">
                <a href="#" class="me-4">
                    <div class="sa-symbol sa-symbol--shape--rounded sa-symbol--size--lg">
                        <img src="{{ asset('dashboard/assets/images/products/product-1-40x40.jpg') }}" width="40" height="40" alt="" />
                    </div>
                </a>
                <div class="d-flex align-items-center flex-grow-1 flex-wrap">
                    <div class="col">
                        <a href="#" class="text-reset fs-exact-14">Wiper Blades Brandix WL2</a>
                    </div>
                    <div class="col-12 col-sm-auto">
                        3,659 sold
                    </div>
                </div>
            </div>
          </li> 
          @endfor
          
      </ul>
  </div>
</div>