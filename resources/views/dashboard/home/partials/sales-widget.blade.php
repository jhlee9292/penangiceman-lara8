<div class="col-12 col-md-3 d-flex">
    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
        <div class="sa-widget-header saw-indicator__header">
            <h2 class="sa-widget-header__title">Toady sales, RM</h2>
        </div>
        <div class="saw-indicator__body">
            <div class="saw-indicator__value">56,799.00</div>
        </div>
    </div>
</div>

<div class="col-12 col-md-3 d-flex">
    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
        <div class="sa-widget-header saw-indicator__header">
            <h2 class="sa-widget-header__title">Average order value, RM</h2>
        </div>
        <div class="saw-indicator__body">
            <div class="saw-indicator__value">1,272.98</div>
        </div>
    </div>
</div>


<div class="col-12 col-md-3 d-flex">
  <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
      <div class="sa-widget-header saw-indicator__header">
          <h2 class="sa-widget-header__title">Shipment</h2>
      </div>
      <div class="saw-indicator__body">
          <div class="saw-indicator__value">12<small class="ms-2" style="font-size: 1.6rem; font-weight: normal;">/918</small></div>
      </div>
  </div>
</div>

<div class="col-12 col-md-3 d-flex">
    <div class="card saw-indicator flex-grow-1" data-sa-container-query='{"340":"saw-indicator--size--lg"}'>
        <div class="sa-widget-header saw-indicator__header">
            <h2 class="sa-widget-header__title">Total orders</h2>
        </div>
        <div class="saw-indicator__body">
            <div class="saw-indicator__value">578</div>
        </div>
    </div>
</div>
