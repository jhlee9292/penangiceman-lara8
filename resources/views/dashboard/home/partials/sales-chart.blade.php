<div class="col-12 col-lg-6 col-xxl-9 d-flex">
  <div class="card flex-grow-1 saw-chart"
    data-sa-data='[{"label":"Jan","value":50},{"label":"Feb","value":130},{"label":"Mar","value":525},{"label":"Apr","value":285},{"label":"May","value":470},{"label":"Jun","value":130},{"label":"Jul","value":285},{"label":"Aug","value":240},{"label":"Sep","value":710},{"label":"Oct","value":470},{"label":"Nov","value":640},{"label":"Dec","value":1110}]'>
      <div class="sa-widget-header saw-chart__header">
        <h2 class="sa-widget-header__title">Income statistics, Year {{ date('Y') }} </h2>
      </div>
      <div class="saw-chart__body">
        <div class="saw-chart__container"><canvas></canvas></div>
      </div>
  </div>
</div>