@extends('layouts.dashboard.index')


@section('content')

{{-- SUBHEADER AND BREADCRUMBS --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  <div class="py-5">
    <div class="row g-4 align-items-center">
      <div class="col">
        <nav class="mb-2" aria-label="breadcrumb">
          <ol class="breadcrumb breadcrumb-sa-simple">
            <li class="breadcrumb-item"><a href="{{ route('dashboard.home.index') }}">Dashboard</a></li>
            <li class="breadcrumb-item active" aria-current="page">Bitly</li>
          </ol>
        </nav>
        <h1 class="h3 mt-5">Bitly</h1>
      </div>
    </div>
  </div>
</div>

{{-- SANDBOX --}}
<div class="mx-xxl-3 px-4 px-sm-5">
  
  <form>
    <div class="sa-entity-layout" data-sa-container-query='{"920":"sa-entity-layout--size--md","1100":"sa-entity-layout--size--lg"}'>
      <div class="sa-entity-layout__body">
        
        <div class="sa-entity-layout__main">
          <div class="card p-5">
            <div class="input-group">
              <input type="text" id="long_url" class="form-control" />
              <button type="button" id="shorten_url" class="btn btn-primary w-25">Shorten</button>
            </div>
            <div class="input-group mt-4">
              <a id="bitly_url" href="" target="_blank"></span>
            </div>
          </div>
        </div>
        
        <div class="sa-entity-layout__sidebar">
          
        </div>
        
      </div>
    </div>
  </form>
  
</div>

@endsection


@push('footer-scripts')
<script>
  $('#shorten_url').click(function() {
    $.ajax({
      method: 'post',
      contentType: 'application/json',
      beforeSend: function(xhr) {
        xhr.setRequestHeader('Authorization', 'Bearer {{ config("services.bitly.access_token") }}');
      },
      url: 'https://api-ssl.bitly.com/v4/shorten',
      data: '{ "long_url": "' + $('#long_url').val() + '" }',
      success: function(data) {
        $('#bitly_url').html('');
        $('#bitly_url').attr('href', data.link);
        $('#bitly_url').html(data.link);
      },
      error: function (xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
        alert(thrownError);
      }
    });
  });
</script>
@endpush
