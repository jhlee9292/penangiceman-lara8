<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Penang Iceman</title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/imgs/penangiceman/favicon.png') }}" />
    <meta name="description" content="Your trusted one stop food delivery">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
      .card{
        border: none;
        box-shadow: rgba(0, 0, 0, 0.1) 0px 4px 12px;
      }
    </style>
  </head>
  <body>
    <div class="container my-5 p-3">
      <div class="row d-flex justify-content-center">
        <div class="col-md-6 col-sm-12">
          <div class="card">
            <div class="card-body text-center py-5">
              <img class="img-fluid" src="{{ asset('assets/logo.png') }}" alt="" style="max-width: 230px;">
              {{-- <h1>Penang Iceman.com</h1> --}}
              <p class="mb-0 text-muted">Launching Soon {{ date('Y') }}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  </body>
</html>