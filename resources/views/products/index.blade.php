@extends('layouts.main')

@section('content')
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="{{ route('home.index') }}" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span>
                {{-- category --}}
                <a href="shop-grid-right.html">Vegetables & tubers</a>
                <span></span>
                {{-- product name --}}
                Seeds of Change Organic
            </div>
        </div>
    </div>


    <div class="container mb-30">
        <div class="row">
            <div class="col-xl-10 col-lg-12 m-auto">
				
                <div class="product-detail accordion-detail">
                    
					{{-- ROW of GALLERY AND SHORT DESCRIPTION --}}
					<div class="row mb-50 mt-30">
                
						{{-- THUMBNAIL --}}
						<div class="col-md-4 col-sm-12 col-xs-12 mb-md-0 mb-sm-5 col-12">
							<div class="detail-gallery">
								<span class="zoom-icon"><i class="fi-rs-search"></i></span>
								{{-- <!-- MAIN SLIDES --> --}}
								<div class="product-image-slider">
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-2.jpg') }}" alt="product image" />
									</figure>
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-1.jpg') }}" alt="product image" />
									</figure>
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-3.jpg') }}" alt="product image" />
									</figure>
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-4.jpg') }}" alt="product image" />
									</figure>
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-5.jpg') }}" alt="product image" />
									</figure>
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-6.jpg') }}" alt="product image" />
									</figure>
									<figure class="border-radius-10">
										<img src="{{ asset('assets/imgs/shop/product-16-7.jpg') }}" alt="product image" />
									</figure>
								</div>

								{{-- <!-- THUMBNAILS --> --}}
								<div class="slider-nav-thumbnails">
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-3.jpg') }}" alt="product image" /></div>
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-4.jpg') }}" alt="product image" /></div>
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-5.jpg') }}" alt="product image" /></div>
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-6.jpg') }}" alt="product image" /></div>
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-7.jpg') }}" alt="product image" /></div>
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-8.jpg') }}" alt="product image" /></div>
									<div><img src="{{ asset('assets/imgs/shop/thumbnail-9.jpg') }}" alt="product image" /></div>
								</div>
							</div>
							{{-- <!-- End Gallery --> --}}
						</div>



						{{-- details / excerpt --}}
						<div class="col-md-8 col-sm-12 col-xs-12 col-12">
							<div class="detail-info pr-30 pl-30">
								<span class="stock-status out-stock"> Sale Off </span>
								<h2 class="title-detail">Seeds of Change Organic Quinoa, Brown</h2>
								<div class="product-detail-rating">
									<div class="product-rate-cover text-end">
										<div class="product-rate d-inline-block">
											<div class="product-rating" style="width: 90%"></div>
										</div>
										<span class="font-small ml-5 text-muted"> (32 reviews)</span>
									</div>
								</div>
								<div class="clearfix product-price-cover">
									<div class="product-price primary-color float-left">
										<span class="current-price text-brand">$38</span>
										<span>
											<span class="save-price font-md color3 ml-15">26% Off</span>
											<span class="old-price font-md ml-15">$52</span>
										</span>
									</div>
								</div>
								<div class="short-desc mb-30">
									<p class="font-lg">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Aliquam rem officia, corrupti reiciendis minima nisi modi, quasi, odio minus dolore impedit fuga eum eligendi.</p>
								</div>
								<div class="attr-detail attr-size mb-30">
									<strong class="mr-10">Size / Weight: </strong>
									<ul class="list-filter size-filter font-small">
										<li><a href="#">50g</a></li>
										<li class="active"><a href="#">60g</a></li>
										<li><a href="#">80g</a></li>
										<li><a href="#">100g</a></li>
										<li><a href="#">150g</a></li>
									</ul>
								</div>

								<div class="detail-extralink mb-50">
									<div class="detail-qty border radius">
										<a href="#" class="qty-down"><i class="fi-rs-angle-small-down"></i></a>
										<span class="qty-val">1</span>
										<a href="#" class="qty-up"><i class="fi-rs-angle-small-up"></i></a>
									</div>
									<div class="product-extra-link2">
										<button type="submit" class="button button-add-to-cart"><i class="fi-rs-shopping-cart"></i>Add to cart</button>
										<a aria-label="Add To Wishlist" class="action-btn hover-up" href="shop-wishlist.html"><i class="fi-rs-heart"></i></a>
									</div>
								</div>

								<div class="font-xs">
									<ul class="mr-50 float-start">
										<li class="mb-5">Type: <span class="text-brand">Organic</span></li>
										<li class="mb-5">MFG:<span class="text-brand"> Jun 4.2021</span></li>
										<li>LIFE: <span class="text-brand">70 days</span></li>
									</ul>
									<ul class="float-start">
										<li class="mb-5">SKU: <a href="#">FWM15VKT</a></li>
										<li class="mb-5">Tags: <a href="#" rel="tag">Snack</a>, <a href="#" rel="tag">Organic</a>, <a href="#" rel="tag">Brown</a></li>
										<li>Stock:<span class="in-stock text-brand ml-5">8 Items In Stock</span></li>
									</ul>
								</div>
							</div>
							<!-- Detail Info -->
						</div>
						{{-- end details / excerpt --}}

			
					</div>
					{{-- end of ROW OF GALLERY AND SHORT DESCRIPTION --}}


					{{-- descriptions --}}
					@include('products.partials.description')

					{{-- related products --}}
					<div class="row mt-60">
						<div class="col-12">
							<h2 class="section-title style-1 mb-30">Related products</h2>
						</div>
			
						<div class="col-12">
							<div class="row related-products">
			
								@for ($i = 0; $i < 5; $i++)
									<div class="col-lg-1-5 col-md-4 col-sm-6 col-6">
										@include('components.products.product-card')
									</div>
								@endfor
			
							</div>
						</div>
					</div>



					{{-- deal of day --}}
					<div class="row mt-60">
						@include('home.partials.deals-of-day')
					</div>


					{{-- category tiles --}}
					<div class="row mt-60">
						@include('home.partials.category-slider')
					</div>

	
                </div>

            </div>
        </div>


		


    </div>
@endsection
