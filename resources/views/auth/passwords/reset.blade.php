@extends('layouts.main-mini')

@section('content')
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-9 col-md-14 col-sm-18">
        <div class="card">
          <div class="card-body">
            <a href="{{ route('home.index') }}">
              <img class="img-fluid" style="max-width: 320px;" src="{{ asset('assets/images/logo-full.png') }}" alt="">
            </a>
            <h4>Senang. Cepat. Baloi.</h4>
            <h6 class="text-muted">Welcome Back to the largest Electrical Hardware platform!</h6>
            <form action="{{ route('login') }}" method="POST">
              @csrf
              <div class="form-group">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" 
                  name="email" value="{{ old('email') }}" 
                  required autocomplete="email" placeholder="Email">
                @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
              
              <div class="form-group">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" 
                  name="password" required autocomplete="current-password" placeholder="Password">
                @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                @enderror
              </div>
              
              <button class="btn btn-block btn-secondary mb-1" type="submit">LOGIN</button>

              @if (Route::has('password.request'))
                <a class="mt-3" href="{{ route('password.request') }}">
                  Forgot Your Password?
                </a>
              @endif
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection