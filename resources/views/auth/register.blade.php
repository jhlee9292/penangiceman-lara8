@extends('layouts.main')

@section('content')
    {{-- Breadcrumb --}}
    <div class="page-header breadcrumb-wrap">
        <div class="container">
            <div class="breadcrumb">
                <a href="{{ route('home.index') }}" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                <span></span> My Account
            </div>
        </div>
    </div>

    <div class="page-content pt-150 pb-150">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-10 col-md-12 m-auto">
                    <div class="row">				

						<div class="col-lg-6 pr-30 d-none d-lg-block">
							<img class="border-radius-15" src="{{ asset('assets/imgs/penangiceman/login-banner-2.png') }}" alt="" />
						</div>
                       
						<div class="col-lg-6 col-md-8">
                            <div class="login_wrap widget-taber-content background-white">
                                <div class="padding_eight_all bg-white">
                                    <div class="heading_s1">
                                        <h1 class="mb-5">Create an Account</h1>
                                        <p class="mb-30">
											Already have an account? <a href="{{ route('login') }}">Login</a>
										</p>
                                    </div>


                                    <form action="{{ route('register') }}" method="POST">
										@csrf
                                        <div class="form-group">
                                            <input class="@error('name') is-invalid @enderror" type="text" 
												value="{{ old('name') }}"
												required name="name" placeholder="Username" />
											@error('name')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
                                        </div>
                                        
										<div class="form-group">
                                            <input class="@error('email') is-invalid @enderror" type="text" 
												value="{{ old('email') }}"
												required name="email" placeholder="Email" />
											@error('email')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
                                        </div>
                                        
										<div class="form-group">
                                            <input class="@error('password') is-invalid @enderror" 
												required type="password" name="password" placeholder="Password" />
											@error('password')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
                                        </div>

                                        <div class="form-group">
                                            <input class="@error('password_confirmation') is-invalid @enderror" 
												required type="password" name="password_confirmation" placeholder="Confirm password" />
											@error('password_confirmation')
												<span class="invalid-feedback" role="alert">
													<strong>{{ $message }}</strong>
												</span>
											@enderror
                                        </div>
                                        
										<div class="form-group mb-30">
                                            <button type="submit"
                                                class="btn btn-fill-out btn-block hover-up font-weight-bold">
												Submit &amp; Register
											</button>
                                        </div>

                                        <p class="font-xs text-muted">
											<strong>Note:</strong> Your personal data will be used
                                            to support your experience throughout this website, to manage access to your
                                            account, and for other purposes described in our privacy policy
										</p>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
