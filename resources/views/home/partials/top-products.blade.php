{{-- Top Products :: 10 items --}}
<section class="product-tabs section-padding position-relative">
    <div class="container">

        <div class="section-title style-2 wow animate__animated animate__fadeIn">
            <h3>Popular Products</h3>
        </div>

        <div class="row product-grid-4">
            @for ($i = 0; $i < 10; $i++)
                <div class="col-lg-1-5 col-md-4 col-6 col-sm-6">
                    @include('components.products.product-card')
                </div>
            @endfor
        </div>

    </div>
</section>
{{-- end Top Products --}}
