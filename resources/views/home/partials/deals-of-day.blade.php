<section class="section-padding pb-5">
    <div class="container">
        <div class="section-title wow animate__animated animate__fadeIn" data-wow-delay="0">
            <h3 class="">Deals Of The Day</h3>
            <a class="show-all" href="{{ route('home.index') }}">
                All Deals
                <i class="fi-rs-angle-right"></i>
            </a>
        </div>

        <div class="row">
			@for ($i = 0; $i < 4; $i++)
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6">
				@include('components.products.product-card-timer')
            </div>
			@endfor
        </div>
		
    </div>
</section>
{{-- <!--End Deals--> --}}
