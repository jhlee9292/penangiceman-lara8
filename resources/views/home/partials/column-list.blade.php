<section class="section-padding mb-30">
    <div class="container">
        <div class="row">

			{{-- Top Selling --}}
			@include('home.partials.column-list--single-col')
            
			{{-- Trending Products --}}
			@include('home.partials.column-list--single-col')

			{{-- Recently Added --}}
			@include('home.partials.column-list--single-col')

			{{-- Top Rated --}}
			@include('home.partials.column-list--single-col')

        </div>
    </div>
</section>
{{-- <!--End 4 columns--> --}}
