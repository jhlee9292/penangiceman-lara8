<section class="section-padding pb-5">
    <div class="container">
        <div class="section-title wow animate__animated animate__fadeIn">
            <h3 class="">Daily Best Sells</h3>
        </div>

        <div class="row">
			{{-- Banner Content --}}
            <div class="col-lg-3 col-12 col-sm-12 col-md-12 d-lg-flex wow animate__animated animate__fadeIn mb-3">
                <div class="banner-img style-2" style="background-image: url({{ asset('assets/imgs/penangiceman/login-banner-2.png') }})">
                    <div class="banner-text">
                        <h2 class="mb-100 text-white">Bring nature into your home</h2>
                        <a href="{{ route('home.index') }}" class="btn btn-xs">
							Shop Now <i class="fi-rs-arrow-small-right"></i>
						</a>
                    </div>
                </div>
            </div>
			{{-- end Banner Content --}}


			{{-- Product Content --}}
            <div class="col-lg-9 col-md-12 wow animate__animated animate__fadeIn" data-wow-delay=".4s">
				<div class="row">
					@for ($i = 0; $i < 4; $i++)
					<div class="col col-md-3 col-sm-6 col-6 mb-2">
						@include('components.products.product-card-stockcount')
					</div>
					@endfor
				</div>
            </div>
			{{-- end Product Content --}}


        </div>
    </div>
</section>
{{-- <!--End Best Sales--> --}}
