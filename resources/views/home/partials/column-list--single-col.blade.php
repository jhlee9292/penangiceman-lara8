<div class="col-xl-3 col-lg-4 col-md-6 mb-sm-5 mb-md-0 wow animate__animated animate__fadeInUp" data-wow-delay="0">

    <h4 class="section-title style-1 mb-30 animated animated">Top Selling</h4>

    <div class="product-list-small animated animated">
		
		{{-- single product list --}}
		@for ($i = 0; $i < 3; $i++)
		@include('components.products.product-list-item')
		@endfor
		
    </div>
</div>
