<section class="popular-categories section-padding">
    <div class="container wow animate__animated animate__fadeIn">
        <div class="section-title">
            <div class="title">
                <h3>Featured Categories</h3>
            </div>
            <div class="slider-arrow slider-arrow-2 flex-right carausel-10-columns-arrow" id="carausel-10-columns-arrows"></div>
        </div>

        <div class="carausel-10-columns-cover position-relative">
            <div class="carausel-10-columns" id="carausel-10-columns">

				@for ($i = 0; $i < 6; $i++)
				{{-- single category card --}}
                <div class="card-2 bg-9 wow animate__animated animate__fadeInUp" data-wow-delay=".1s">
                    <figure class="img-hover-scale overflow-hidden">
                        <a href="shop-grid-right.html">
							<img src="{{ asset('assets/imgs/shop/cat-13.png') }}" alt="" />
						</a>
                    </figure>
                    <h6><a href="shop-grid-right.html">Dim Sum</a></h6>
                    <span>62 items</span>
                </div>
				{{-- end single category card --}}
                
                {{-- single category card --}}
                <div class="card-2 bg-9 wow animate__animated animate__fadeInUp" data-wow-delay=".1s">
                    <figure class="img-hover-scale overflow-hidden">
                        <a href="shop-grid-right.html">
							<img src="{{ asset('assets/imgs/shop/cat-13.png') }}" alt="" />
						</a>
                    </figure>
                    <h6><a href="shop-grid-right.html">Paste</a></h6>
                    <span>53 items</span>
                </div>
				{{-- end single category card --}}

				@endfor

            </div>
        </div>
    </div>
</section>
{{-- End category slide --}}
