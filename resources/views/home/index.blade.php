@extends('layouts.main')

@section('content')

  @include('home.partials.slider')
  @include('home.partials.category-slider')
  @include('home.partials.meta-box')
  @include('home.partials.top-products')
  @include('home.partials.daily-best-seller')
  @include('home.partials.deals-of-day')
  @include('home.partials.column-list')

@endsection