@extends('layouts.main')


@push('header-scripts')
<link rel="stylesheet" href="{{ asset('assets/css/plugins/slider-range.css') }}" />
@endpush



@section('content')

	{{-- breadcrumb header with bg --}}
    <div class="page-header mt-30 mb-50">
        <div class="container">
            <div class="archive-header">
                <div class="row align-items-center">
                    <div class="col-xl-3">
                        <h1 class="mb-15">Shop</h1>
                        <div class="breadcrumb">
                            <a href="{{ route('home.index') }}" rel="nofollow"><i class="fi-rs-home mr-5"></i>Home</a>
                            <span></span> Shop
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


	{{-- The Shop Content --}}
	<div class="container mb-30">
		<div class="row flex-row-reverse">

			{{-- shop grid listing --}}
			<div class="col-lg-4-5">
				<div class="shop-product-fillter">
					<div class="totall-product">
						<p>We found <strong class="text-brand">29</strong> items for you!</p>
					</div>
				</div>
				@include('shop.partials.product-grid')
			</div>


			{{-- sidebar --}}
			<div class="col-lg-1-5 primary-sidebar sticky-sidebar">
				@include('shop.partials.sidebar')
			</div>

		</div>
	</div>


@endsection


@push('footer-scripts')
<script src="{{ asset('assets/js/plugins/slider-range.js') }}"></script>
@endpush