<div class="sidebar-widget widget-category-2 mb-30">
	<h5 class="section-title style-1 mb-30">Category</h5>
	<ul>
		@for ($i = 0; $i < 6; $i++)
		{{-- single category item --}}
		<li>
			<a href="shop-grid-right.html"> 
				<img src="{{ asset('assets/imgs/theme/icons/category-1.svg') }}"alt="" />
				Milks & Dairies
			</a>
			<span class="count text-white">30</span>
		</li>
		{{-- end single category item --}}
		@endfor
	</ul>
</div>


{{-- <!-- Fillter By Price --> --}}
<div class="sidebar-widget price_range range mb-30">
	<h5 class="section-title style-1 mb-0">Brands</h5>
	<div class="list-group">
		<div class="list-group-item mb-10 mt-10">
			<div class="custome-checkbox">
				<input class="form-check-input" type="checkbox" name="checkbox" id="exampleCheckbox1" value="" />
				<label class="form-check-label" for="exampleCheckbox1"><span>Red (56)</span></label>
				<br />
				<input class="form-check-input" type="checkbox" name="checkbox" id="exampleCheckbox2" value="" />
				<label class="form-check-label" for="exampleCheckbox2"><span>Green (78)</span></label>
				<br />
				<input class="form-check-input" type="checkbox" name="checkbox" id="exampleCheckbox3" value="" />
				<label class="form-check-label" for="exampleCheckbox3"><span>Blue (54)</span></label>
			</div>
		</div>
	</div>
	<a href="shop-grid-right.html" class="btn btn-sm btn-default"><i class="fi-rs-filter mr-5"></i> Fillter</a>
</div>


{{-- <!-- Product sidebar Widget --> --}}
<div class="sidebar-widget product-sidebar mb-30 p-30 bg-grey border-radius-10">
	<h5 class="section-title style-1 mb-30">New products</h5>


	@for ($i = 0; $i < 3; $i++)
	{{-- single new product --}}
	<div class="single-post clearfix">
		<div class="image">
			<img src="{{ asset('assets/imgs/shop/thumbnail-3.jpg') }}" alt="#" />
		</div>
		<div class="content pt-10">
			<h6 class="two-lines"><a href="{{ route('home.index') }}">Chen Cardigan</a></h6>
			<p class="price mb-0 mt-5">$99.50</p>
			<div class="product-rate">
				<div class="product-rating" style="width: 90%"></div>
			</div>
		</div>
	</div>
	{{-- end single new product --}}
	@endfor


</div>
<div class="banner-img wow fadeIn mb-lg-0 animated d-lg-block d-none">
	<img src="{{ asset('assets/imgs/banner/banner-11.png') }}" alt="" />
	<div class="banner-text">
		<span>Oganic</span>
		<h4>
			Save 17% <br />
			on <span class="text-brand">Oganic</span><br />
			Juice
		</h4>
	</div>
</div>
