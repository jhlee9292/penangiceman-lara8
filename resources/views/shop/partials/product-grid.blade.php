<div class="row product-grid">


	@for ($i = 0; $i < 20; $i++)
	{{-- single product card --}}
    <div class="col-lg-1-5 col-md-4 col-6 col-sm-6">
        @include('components.products.product-card')
    </div>
    {{-- end single product card --}}
	@endfor
    
</div>
{{-- product grid --}}



{{-- Pagination --}}
<div class="pagination-area mt-20 mb-20">
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-start">
            <li class="page-item">
                <a class="page-link" href="#"><i class="fi-rs-arrow-small-left"></i></a>
            </li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item active"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link dot" href="#">...</a></li>
            <li class="page-item"><a class="page-link" href="#">6</a></li>
            <li class="page-item">
                <a class="page-link" href="#"><i class="fi-rs-arrow-small-right"></i></a>
            </li>
        </ul>
    </nav>
</div>
{{-- end Pagination --}}


{{-- Deal of the day --}}
@include('home.partials.deals-of-day')


<!--End Deals-->
