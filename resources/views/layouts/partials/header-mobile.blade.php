{{-- mobile header --}}
<div class="mobile-header-active mobile-header-wrapper-style">
    <div class="mobile-header-wrapper-inner">
        <div class="mobile-header-top">
            <div class="mobile-header-logo">
                <a href="{{ route('home.index') }}"><img src="{{ asset('assets/logo.png') }}" alt="logo" /></a>
            </div>
            <div class="mobile-menu-close close-style-wrap close-style-position-inherit">
                <button class="close-style search-close">
                    <i class="icon-top"></i>
                    <i class="icon-bottom"></i>
                </button>
            </div>
        </div>
        <div class="mobile-header-content-area">
            <div class="mobile-search search-style-3 mobile-header-border">
                <form action="#">
                    <input type="text" placeholder="Search for items…" />
                    <button type="submit"><i class="fi-rs-search"></i></button>
                </form>
            </div>
            <div class="mobile-menu-wrap mobile-header-border">
                <!-- mobile menu start -->
                <nav>
                    <ul class="mobile-menu font-heading">

                        <li class="menu-item">
                            <a href="{{ route('shop.index') }}">Shop</a>
                        </li>

                        <li class="menu-item-has-children">
                            <a href="#">Brands</a>
                            <ul class="dropdown">
                                <li><a href="{{ route('home.index') }}">Brands 1</a></li>
                                <li><a href="{{ route('home.index') }}">Brands 2</a></li>
                                <li><a href="{{ route('home.index') }}">Brands 3</a></li>
                                <li><a href="{{ route('home.index') }}">Brands 4</a></li>
                            </ul>
                        </li>

                        <li class="menu-item-has-children">
                            <a href="#">Hot Selling</a>
                            <ul class="dropdown">
                                <li class="menu-item-has-children">
                                    <a href="#">Hot Items</a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('home.index') }}">Hot Item 1</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 2</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 3</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 4</a></li>
                                    </ul>
                                </li>
								<li class="menu-item-has-children">
                                    <a href="#">Hot Items</a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('home.index') }}">Hot Item 1</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 2</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 3</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 4</a></li>
                                    </ul>
                                </li>
								<li class="menu-item-has-children">
                                    <a href="#">Hot Items</a>
                                    <ul class="dropdown">
                                        <li><a href="{{ route('home.index') }}">Hot Item 1</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 2</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 3</a></li>
                                        <li><a href="{{ route('home.index') }}">Hot Item 4</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="menu-item-has-children">
                            <a href="blog-category-fullwidth.html">Categories</a>
                            <ul class="dropdown">
                                <li><a href="{{ route('home.index') }}">Category 1</a></li>
                                <li><a href="{{ route('home.index') }}">Category 2</a></li>
                                <li><a href="{{ route('home.index') }}">Category 3</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- mobile menu end -->
            </div>
            <div class="mobile-header-info-wrap">
                <div class="single-mobile-header-info">
                    @if ( Auth::check() )
                    <a href="{{ route('home.index') }}"><i class="fi-rs-user"></i>My Account </a>
                    @else
                    <a href="{{ route('home.index') }}"><i class="fi-rs-user"></i>Log In / Sign Up </a>
                    @endif
                </div>
                <div class="single-mobile-header-info">
                    <a href="{{ route('home.index') }}"><i class="fi-rs-marker"></i> Track Orders </a>
                </div>
            </div>
            <div class="mobile-social-icon mb-50">
                <h6 class="mb-15">Follow Us</h6>
                <a href="#"><img src="{{ asset('assets/imgs/theme/icons/icon-facebook-white.svg') }}" alt="" /></a>
                <a href="#"><img src="{{ asset('assets/imgs/theme/icons/icon-twitter-white.svg') }}" alt="" /></a>
                <a href="#"><img src="{{ asset('assets/imgs/theme/icons/icon-instagram-white.svg') }}" alt="" /></a>
                <a href="#"><img src="{{ asset('assets/imgs/theme/icons/icon-pinterest-white.svg') }}" alt="" /></a>
                <a href="#"><img src="{{ asset('assets/imgs/theme/icons/icon-youtube-white.svg') }}" alt="" /></a>
            </div>
            <div class="site-copyright">
				Copyright {{ date('Y') }} &copy; Penangiceman. All rights reserved. Powered by Niuniuweb
			</div>
        </div>
    </div>
</div>
