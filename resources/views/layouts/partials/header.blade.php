<header class="header-area header-style-1 header-height-2">
    <div class="mobile-promotion">
        <span>Grand opening, <strong>up to 15%</strong> off all items. Only <strong>3 days</strong> left</span>
    </div>
    <div class="header-top header-top-ptb-1 d-none d-lg-block">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-9 col-lg-8">
                    <div class="header-info">
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="{{ route('myaccount.index') }}">My Account</a></li>
                            <li><a href="#">Order Tracking</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xl-3 col-lg-4">
                    <div class="header-info header-info-right">
                        <ul>
                            @if ( Auth::check() )
                            <li>Welcome {{ Auth::user()->name }}</li>
                            @else
                            <li>
                                <a href="{{ route('login') }}">Login</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="{{ route('register') }}">Register</a>
                            </li>   
                            @endif
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="header-middle header-middle-ptb-1 d-none d-lg-block">
        <div class="container">
            <div class="header-wrap">
                <div class="logo logo-width-1">
                    <a href="{{ route('home.index') }}"><img src="{{ asset('assets/logo.png') }}" alt="logo" /></a>
                </div>
                <div class="header-right">
                    <div class="search-style-2">
                        <form action="#">
                            <select class="select-active">
                                <option>All Categories</option>
                                <option>Milks and Dairies</option>
                                <option>Wines & Alcohol</option>
                                <option>Clothing & Beauty</option>
                                <option>Pet Foods & Toy</option>
                                <option>Fast food</option>
                                <option>Baking material</option>
                                <option>Vegetables</option>
                                <option>Fresh Seafood</option>
                                <option>Noodles & Rice</option>
                                <option>Ice cream</option>
                            </select>
                            <input type="text" placeholder="Search for items..." />
                        </form>
                    </div>
                    <div class="header-action-right">
                        <div class="header-action-2">
                            <div class="header-action-icon-2">
                                <a class="mini-cart-icon" href="shop-cart.html">
                                    <img alt="Nest" src="{{ asset('assets/imgs/theme/icons/icon-cart.svg') }}" />
                                    <span class="pro-count blue">2</span>
                                </a>
                                <a href="shop-cart.html"><span class="lable">Cart</span></a>
                                <div class="cart-dropdown-wrap cart-dropdown-hm2">
                                    <ul>
                                        <li>
                                            <div class="shopping-cart-img">
                                                <a href="{{ route('home.index') }}"><img alt="Nest" src="{{ asset('assets/imgs/shop/thumbnail-3.jpg') }}" /></a>
                                            </div>
                                            <div class="shopping-cart-title">
                                                <h4><a href="{{ route('home.index') }}">Daisy Casual Bag</a></h4>
                                                <h4><span>1 x </span>$800.00</h4>
                                            </div>
                                            <div class="shopping-cart-delete">
                                                <a href="#"><i class="fi-rs-cross-small"></i></a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="shopping-cart-img">
                                                <a href="{{ route('home.index') }}"><img alt="Nest" src="{{ asset('assets/imgs/shop/thumbnail-2.jpg') }}" /></a>
                                            </div>
                                            <div class="shopping-cart-title">
                                                <h4><a href="{{ route('home.index') }}">Corduroy Shirts</a></h4>
                                                <h4><span>1 x </span>$3200.00</h4>
                                            </div>
                                            <div class="shopping-cart-delete">
                                                <a href="#"><i class="fi-rs-cross-small"></i></a>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="shopping-cart-footer">
                                        <div class="shopping-cart-total">
                                            <h4>Total <span>$4000.00</span></h4>
                                        </div>
                                        <div class="shopping-cart-button">
                                            <a href="shop-cart.html" class="outline">View cart</a>
                                            <a href="shop-checkout.html">Checkout</a>
                                        </div>
                                    </div>
                                </div>
                            </div>



                            @if ( Auth::check() )
                                {{-- My Account Dropdown --}}
                                <div class="header-action-icon-2">
                                    <a href="page-account.html">
                                        <img class="svgInject" alt="Nest" src="{{ asset('assets/imgs/theme/icons/icon-user.svg') }}" />
                                    </a>
                                    <a href="{{ route('myaccount.index') }}"><span class="lable ml-0">Account</span></a>
                                    <div class="cart-dropdown-wrap cart-dropdown-hm2 account-dropdown">
                                        <ul>
                                            <li>
                                                <a href="{{ route('myaccount.index') }}"><i class="fi fi-rs-user mr-10"></i>My Account</a>
                                            </li>
                                            <li>
                                                <a href="page-account.html"><i class="fi fi-rs-location-alt mr-10"></i>Order Tracking</a>
                                            </li>
                                            <li>
                                                <a href="page-account.html"><i class="fi fi-rs-label mr-10"></i>My Voucher</a>
                                            </li>
                                            <li>
                                                <a href="page-account.html"><i class="fi fi-rs-settings-sliders mr-10"></i>Setting</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                                    <i class="fi fi-rs-sign-out mr-10"></i>
                                                    Sign out
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                                    @csrf
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                {{-- end My Account Dropdown --}}


                            @else
                                <div class="header-action-icon-2">
                                    <a href="{{ route('login') }}">
                                        <img class="svgInject" alt="Nest" src="{{ asset('assets/imgs/theme/icons/icon-user.svg') }}" />
                                    </a>
                                    <a href="{{ route('login') }}"><span class="lable ml-0">Login</span></a>
                                </div>
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="header-bottom header-bottom-bg-color sticky-bar">
        <div class="container">
            <div class="header-wrap header-space-between position-relative">


                <div class="logo logo-width-1 d-block d-lg-none">
                    <a href="{{ route('home.index') }}">
                        <img src="{{ asset('assets/logo.png') }}" alt="logo" />
                    </a>
                </div>


                <div class="header-nav d-none d-lg-flex">

                    <div class="main-menu main-menu-padding-1 main-menu-lh-2 d-none d-lg-block font-heading">
                        <nav>
                            <ul>
                                <li class="hot-deals">
                                    <i class="fas fa-fire-alt" style="color: red;"></i>
                                    <a href="{{ route('home.index') }}">Hot Deals</a>
                                </li>
                                
                                <li>
                                    <a href="{{ route('shop.index') }}">Shop</a>
                                </li>

                                <li class="position-static">
                                    <a href="#">Quick Menu <i class="fi-rs-angle-down"></i></a>
                                    <ul class="mega-menu">
                                        <li class="sub-mega-menu sub-mega-menu-width-22">
                                            <a class="menu-title" href="#">Fruit & Vegetables</a>
                                            <ul>
                                                <li><a href="{{ route('home.index') }}">Meat & Poultry</a></li>
                                                <li><a href="{{ route('home.index') }}">Fresh Vegetables</a></li>
                                                <li><a href="{{ route('home.index') }}">Herbs & Seasonings</a></li>
                                                <li><a href="{{ route('home.index') }}">Cuts & Sprouts</a></li>
                                                <li><a href="{{ route('home.index') }}">Exotic Fruits & Veggies</a></li>
                                                <li><a href="{{ route('home.index') }}">Packaged Produce</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-mega-menu sub-mega-menu-width-22">
                                            <a class="menu-title" href="#">Breakfast & Dairy</a>
                                            <ul>
                                                <li><a href="{{ route('home.index') }}">Milk & Flavoured Milk</a></li>
                                                <li><a href="{{ route('home.index') }}">Butter and Margarine</a></li>
                                                <li><a href="{{ route('home.index') }}">Eggs Substitutes</a></li>
                                                <li><a href="{{ route('home.index') }}">Marmalades</a></li>
                                                <li><a href="{{ route('home.index') }}">Sour Cream</a></li>
                                                <li><a href="{{ route('home.index') }}">Cheese</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-mega-menu sub-mega-menu-width-22">
                                            <a class="menu-title" href="#">Meat & Seafood</a>
                                            <ul>
                                                <li><a href="{{ route('home.index') }}">Breakfast Sausage</a></li>
                                                <li><a href="{{ route('home.index') }}">Dinner Sausage</a></li>
                                                <li><a href="{{ route('home.index') }}">Chicken</a></li>
                                                <li><a href="{{ route('home.index') }}">Sliced Deli Meat</a></li>
                                                <li><a href="{{ route('home.index') }}">Wild Caught Fillets</a></li>
                                                <li><a href="{{ route('home.index') }}">Crab and Shellfish</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-mega-menu sub-mega-menu-width-34">
                                            <div class="menu-banner-wrap">
                                                <a href="{{ route('home.index') }}"><img src="{{ asset('assets/imgs/banner/banner-menu.png') }}" alt="Nest" /></a>
                                                <div class="menu-banner-content">
                                                    <h4>Hot deals</h4>
                                                    <h3>
                                                        Don't miss<br />
                                                        Trending
                                                    </h3>
                                                    <div class="menu-banner-price">
                                                        <span class="new-price text-success">Save to 50%</span>
                                                    </div>
                                                    <div class="menu-banner-btn">
                                                        <a href="{{ route('home.index') }}">Shop now</a>
                                                    </div>
                                                </div>
                                                <div class="menu-banner-discount">
                                                    <h3>
                                                        <span>25%</span>
                                                        off
                                                    </h3>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                                
                                {{-- sample for dropdown --}}
                                <li>
                                    <a href="#">Brands <i class="fi-rs-angle-down"></i></a>
                                    <ul class="sub-menu">
                                        <li><a href="{{ route('home.index') }}">Liumama</a></li>
                                        <li><a href="{{ route('home.index') }}">About Us</a></li>
                                        <li><a href="{{ route('home.index') }}">About Us</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ route('home.index') }}">About</a>
                                </li>

                                <li>
                                    <a href="{{ route('home.index') }}">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="hotline d-none d-lg-flex">
                    <img src="{{ asset('assets/imgs/theme/icons/icon-headphone.svg') }}" alt="hotline" />
                    <p>1900 - 888<span>24/7 Support Center</span></p>
                </div>
                <div class="header-action-icon-2 d-block d-lg-none">
                    <div class="burger-icon burger-icon-white">
                        <span class="burger-icon-top"></span>
                        <span class="burger-icon-mid"></span>
                        <span class="burger-icon-bottom"></span>
                    </div>
                </div>
                <div class="header-action-right d-block d-lg-none">
                    <div class="header-action-2">
                        <div class="header-action-icon-2">
                            <a href="{{ route('home.index') }}">
                                <img alt="Nest" src="{{ asset('assets/imgs/theme/icons/icon-heart.svg') }}" />
                                <span class="pro-count white">4</span>
                            </a>
                        </div>
                        <div class="header-action-icon-2">
                            <a class="mini-cart-icon" href="#">
                                <img alt="Nest" src="{{ asset('assets/imgs/theme/icons/icon-cart.svg') }}" />
                                <span class="pro-count white">2</span>
                            </a>
                            <div class="cart-dropdown-wrap cart-dropdown-hm2">
                                <ul>
                                    <li>
                                        <div class="shopping-cart-img">
                                            <a href="{{ route('home.index') }}"><img alt="Nest" src="{{ asset('assets/imgs/shop/thumbnail-3.jpg') }}" /></a>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="{{ route('home.index') }}">好旺饺-韭菜猪肉饺</a></h4>
                                            <h3><span>1 x </span>RM120.00</h3>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="fi-rs-cross-small"></i></a>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="shopping-cart-img">
                                            <a href="{{ route('home.index') }}"><img alt="Nest" src="{{ asset('assets/imgs/shop/thumbnail-4.jpg') }}" /></a>
                                        </div>
                                        <div class="shopping-cart-title">
                                            <h4><a href="{{ route('home.index') }}">懒惰鸡扒/胸 (盐焗药材)</a></h4>
                                            <h3><span>1 x </span>RM189.00</h3>
                                        </div>
                                        <div class="shopping-cart-delete">
                                            <a href="#"><i class="fi-rs-cross-small"></i></a>
                                        </div>
                                    </li>
                                </ul>
                                <div class="shopping-cart-footer">
                                    <div class="shopping-cart-total">
                                        <h4>Total <span>RM383.00</span></h4>
                                    </div>
                                    <div class="shopping-cart-button">
                                        <a href="{{ route('home.index')}}">View cart</a>
                                        <a href="{{ route('home.index') }}">Checkout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


{{-- Mobile Menu --}}
@include('layouts.partials.header-mobile')