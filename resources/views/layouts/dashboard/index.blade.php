<!DOCTYPE html>
<html lang="en" dir="ltr" data-scompiler-id="0">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="format-detection" content="telephone=no" />
		<title>Admin Dashboard</title>
		{{-- CSRF --}}
		<meta name="csrf-token" content="{{ csrf_token() }}">
        {{-- NOTE:: MUST INCLUDE :: NON INDEXING --}}
		<meta name="robots" content="noindex,nofollow">
        <meta name="googlebot" content="noindex">
        {{-- Icon --}}
        <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}" />
        {{-- Fonts --}}
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" />
        {{-- CSS --}}
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/bootstrap/css/bootstrap.ltr.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/highlight.js/styles/github.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/simplebar/simplebar.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/quill/quill.snow.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/air-datepicker/css/datepicker.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/select2/css/select2.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/nouislider/nouislider.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/fullcalendar/main.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('dashboard/assets/vendor/datatables/css/dataTables.bootstrap5.min.css') }}" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css">
        {{-- Header Scripts --}}
        @stack('header-scripts')
        <link rel="stylesheet" href="{{ asset('dashboard/assets/css/style.css') }}" />
		
    </head>
    <body>
        {{-- Body Scripts --}}
        @stack('body-scripts')

        {{-- sa-app --}}
        <div class="sa-app sa-app--desktop-sidebar-shown sa-app--mobile-sidebar-hidden sa-app--toolbar-fixed">

			{{-- sidebar --}}
			@include('layouts.dashboard.partials.sidebar')

			{{-- sa-app__content --}}
			<div class="sa-app__content">

				{{-- sa-app__toolbar --}}
				@include('layouts.dashboard.partials.toolbar')
				{{-- sa-app__toolbar / end --}}

				{{-- sa-app__body --}}
				<div id="top" class="sa-app__body px-2 px-lg-4">
					<div class="container pb-6">

						{{-- THE CONTENT --}}
						@yield('content')

					</div>
				</div>

				{{-- sa-app__footer --}}
				<div class="sa-app__footer d-block d-md-flex">
					Admin Dashboard by Niuniuweb &copy; {{ date('Y') }}
					<div class="m-auto"></div>
				</div>
				{{-- sa-app__footer / end --}}
				
			</div>
			{{-- sa-app__content / end --}}

			{{-- sa-app__toasts --}}
			<div class="sa-app__toasts toast-container bottom-0 end-0"></div>
			{{-- sa-app__toasts / end --}}
			
		</div>
        {{-- sa-app / end --}}

        {{-- scripts --}}
        <script src="{{ asset('dashboard/assets/vendor/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/feather-icons/feather.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/simplebar/simplebar.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/highlight.js/highlight.pack.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/quill/quill.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/air-datepicker/js/datepicker.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/air-datepicker/js/i18n/datepicker.en.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/select2/js/select2.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/fontawesome/js/all.min.js') }}" data-auto-replace-svg="" async=""></script>
        <script src="{{ asset('dashboard/assets/vendor/chart.js/chart.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/nouislider/nouislider.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/fullcalendar/main.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('dashboard/assets/vendor/datatables/js/dataTables.bootstrap5.min.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>
        {{-- Footer Scripts --}}
        
        @stack('footer-scripts')
        <script src="{{ asset('dashboard/assets/js/stroyka.js') }}"></script>
        <script src="{{ asset('dashboard/assets/js/custom.js') }}"></script>
        <script src="{{ asset('dashboard/assets/js/calendar.js') }}"></script>
        <script src="{{ asset('dashboard/assets/js/demo.js') }}"></script>
        <script src="{{ asset('dashboard/assets/js/demo-chart-js.js') }}"></script>
		
    </body>
</html>
