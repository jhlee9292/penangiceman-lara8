<!DOCTYPE html>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge" />
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/imgs/penangiceman/favicon.png') }}" />
        {{-- CSRF --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- Meta --}}
        <title>Penang Iceman</title>
        <meta name="description" content="Penang Iceman | Your trusted delivery partner" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        {{-- Meta --}}
        @if( config('app.ALLOW_META') == true ) {
            <meta property="og:title" content="Penangiceman" />
            <meta property="og:type" content="website" />
            <meta property="og:url" content="{{ route('home.index') }}" />
            <meta property="og:image" content="{{ asset('assets/imgs/penangiceman/favicon.png') }}" />
            @stack('header-meta')
        @else
            <meta name="robots" content="noindex,nofollow">
            <meta name="googlebot" content="noindex">
        @endif
        
        {{-- Header Scripts --}}
        <link rel="stylesheet" href="{{ asset('assets/css/plugins/animate.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}" />
        <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}" />
        @stack('header-scripts')        
    </head>


    <body>

        {{-- Modals --}}

        {{-- Header --}}
        @include('layouts.partials.header')
        
        {{-- Page Content --}}
        <main class="main">
            @yield('content')
        </main>

        {{-- Footer --}}
        @include('layouts.partials.footer')

         {{-- Preloader --}}
         <div id="preloader-active">
            <div class="preloader d-flex align-items-center justify-content-center">
                <div class="preloader-inner position-relative">
                    <div class="text-center">
                        <img class="heartbeat" src="{{ asset('assets/imgs/penangiceman/loader.png') }}" alt="" />
                    </div>
                </div>
            </div>
        </div>
        
        {{-- Vendor JS --}}
        <script src="{{ asset('assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
        <script src="{{ asset('assets/js/vendor/jquery-3.6.0.min.js') }}"></script>
        <script src="{{ asset('assets/js/vendor/jquery-migrate-3.3.0.min.js') }}"></script>
        <script src="{{ asset('assets/js/vendor/bootstrap.bundle.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/slick.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/jquery.syotimer.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/waypoints.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/wow.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/perfect-scrollbar.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/magnific-popup.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/select2.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/counterup.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/jquery.countdown.min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/images-loaded.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/isotope.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/scrollup.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/jquery.vticker-min.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/jquery.theia.sticky.js') }}"></script>
        <script src="{{ asset('assets/js/plugins/jquery.elevatezoom.js') }}"></script>
        <script defer src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" integrity="sha384-rOA1PnstxnOBLzCLMcre8ybwbTmemjzdNlILg8O7z1lUkLXozs4DHonlDtnE7fpc" crossorigin="anonymous"></script>
        {{-- Footer Scripts --}}
        @stack('footer-scripts')
        {{-- Template JS --}}
        <script src="{{ asset('assets/js/main.js?v=4.0') }}"></script>
        <script src="{{ asset('assets/js/shop.js?v=4.0') }}"></script>
    </body>
</html>