<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Storage;

class DOSpacesController extends Controller {
  
  // DO SPACES
  public function index() {
    $files = Storage::disk('do')->files('files');
    
    return view('dashboard.dospaces.index', compact('files'));
  }
  
  // DO SPACES UPLOAD
  public function upload(Request $request) {
     Storage::disk('do')->put('files', $request->file('file'), 'public');
    
    return redirect()->route('dashboard.dospaces.index')->with('success', 'File uploaded.');
  }
  
}
