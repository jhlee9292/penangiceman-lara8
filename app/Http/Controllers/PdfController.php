<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Product;

use Dompdf\Dompdf;

class PdfController extends Controller {
  
  // PDF
  public function index() {
    $product = Product::find(1);
    
    return view('dashboard.pdf.index', compact('product'));
  }
  
  // PDF DOWNLOAD
  public function download(Request $request) {
    $product = Product::find($request->id);
    
    $view = view('dashboard.pdf.product', compact('product'))->render();
    
    $pdf = new Dompdf();
    $pdf->loadHtml($view);
    $pdf->setPaper('A4', 'landscape');
    $pdf->render();
    $pdf->stream();
  }
  
}
