<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// MODELS
use App\Models\ProductGalleries;

class ProductGalleriesController extends Controller {
  
  // DESTROY
  public function destroy($id) {
    // GET GALLERY BY ID
    $gallery = ProductGalleries::findOrFail($id);
    
    // REMOVE IMAGE
    unlink(public_path('storage/' . $gallery->image)); // REMOVE PROCESSED IMAGE
    unlink(public_path('storage/' . substr_replace($gallery->image, "", -5, 1))); // REMOVE ORIGINAL IMAGE
    
    // DELETE GALLERY
    $gallery->delete();

    return redirect()->back()->with('success', 'Image deleted.');
  }
  
}
