<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Exports\ProductsExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller {
  
  // PDF
  public function index() {
    return view('dashboard.export.index');
  }
  
  // PDF CREATE
  public function create() {
    return Excel::download(new ProductsExport, 'Products.xlsx');
  }
  
}
