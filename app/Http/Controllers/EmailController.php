<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use SendGrid;
use SendGrid\Mail\Mail;

class EmailController extends Controller {
  
  // EMAIL
  public function index() {
    $response = array();
    return view('dashboard.email.index', compact('response'));
  }
  
  // EMAIL SEND
  public function send(Request $request) {
    $sendgrid = new SendGrid(config('services.sendgrid.api_key'));
    
    $email = new Mail();
    $email->setFrom("test@myletrik.com", "MyLetrik");
    $email->addTo($request->email, $request->name, ['name' => $request->name]);
    $email->setTemplateId('d-13fe83f184d04d87a87aecdc359f798c');
    
    $response = $sendgrid->send($email);
    
    return view('dashboard.email.index', compact('response'));
  }
  
}
