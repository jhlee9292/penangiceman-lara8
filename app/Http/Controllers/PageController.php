<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Models
use App\Models\Product;

class PageController extends Controller {

    public function index() {
        return view('home.index');
    }

    public function renderComingSoonPage() {
        return view('comingsoon.index');
    }


    // ----------------------------------------------------------------
    // SHOP PAGES
    // ----------------------------------------------------------------
    
    public function renderShopPage() {
        return view('shop.index');
    }


    // ----------------------------------------------------------------
    // PRODUCT PAGES
    // ----------------------------------------------------------------

    public function renderSingleProductPage() {
        // $product = Product::where('slug', $slug)->first();
        // if( ! $product ) abort(404, "Whoops... resource not found");

        // return view('products.single', compact('product'));
        return view('products.index');
    }


    // ----------------------------------------------------------------
    // MY ACCOUNT PAGES
    // ----------------------------------------------------------------

    public function renderMyAccountPage() {
        return view('myaccount.index');
    }


}
