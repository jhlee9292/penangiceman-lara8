<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Image\Image;
use Spatie\Image\Manipulations;

// MODELS
use App\Models\Product;
use App\Models\ProductSku;
use App\Models\ProductGalleries;;
use App\Models\ProductVariant;
use App\Models\ProductAttribute;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Tag;

class ProductVariantController extends Controller {
  
  // INDEX
  public function index() {
    // GET VARIANT PRODUCTS
    $variant_products = Product::where('type', 'variant')->latest()->paginate(25);

    return view('dashboard.products.variants.index', compact('variant_products'));
  }
  
  // CREATE
  public function create() {
    // GET BRANDS
    $brands = Brand::select('id', 'name')->orderBy('name')->get();
    
    // GET TAGS
    $tags = Tag::select('name')->orderBy('name')->get();
    
    return view('dashboard.products.variants.create', compact('brands', 'tags'));
  }
  
  // STORE
  public function store(Request $request) {
    // VALIDATION
    $request->validate([
      'name' => 'required|max:255',
      'slug' => 'required|alpha_dash|unique:products,slug',
      'image' => 'mimes:jpg,png,jpeg|max:2048',
    ]);
    
    // HANDLE WYSIWYG
    $content = "";
    
    if($request->content) {
      $html_content = mb_convert_encoding($request->content, 'HTML-ENTITIES', 'UTF-8');

      $dom = new \DomDocument();
      $dom->loadHtml($html_content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $dom->encoding = 'utf-8';
      
      $content_images = $dom->getElementsByTagName('img');
      
      foreach($content_images as $image){
        $data = $image->getattribute('src');
        
        if(strpos($data, 'base64') == true) {
          list($type, $data) = explode(";", $data);
          list(, $data) = explode(",", $data);

          $data = base64_decode($data);
          $image_name = uniqid() . 'a.png';
          $path = public_path('/storage/products/' . $image_name);

          file_put_contents($path, $data);

          $image->removeattribute('src');
          $image->setattribute('src', '/storage/products/' . $image_name);
        }
      }
      $content = $dom->saveHTML();
    }
    
    // STORE SKU
    $sku = new ProductSku;
    $sku->brand_id = $request->brand;
    $sku->name = $request->name;
    $sku->sku = $request->sku;
    $sku->mfg_part_number = $request->mfg_part_number;
    $sku->stock = $request->stock;
    $sku->selling_price = $request->selling_price;
    $sku->sale_price = $request->sale_price;
    $sku->weight = $request->weight;
    $sku->save();
    
    // HANDLE IMAGE
    $image = "";
    
    if($request->file('image')) {
      // STORE IMAGE
      $image = $request->file('image')->store('products', 'public');
      
      // RENAME STORED IMAGE
      $new_image = str_replace(".", rand(0, 9) . ".", $image);
      
      // RESIZE IMAGE AND STORE AS NEW IMAGE
      Image::load(public_path('storage/' . $image))
        ->width(600)
        ->height(600)
        ->save(public_path('storage/' . $new_image));
      
      // ADD WATERMARK TO IMAGE
      Image::load(public_path('storage/' . $new_image))
        ->fit(Manipulations::FIT_FILL, 600, 600)
        ->watermark(public_path('storage/watermark.png'))
        ->watermarkPosition(Manipulations::POSITION_CENTER)
        ->watermarkFit(Manipulations::FIT_FILL)
        ->optimize()
        ->save();
      
      $image = $new_image;
    }
    
    // STORE PRODUCT
    $product = new Product;
    $product->sku_id = $sku->id;
    $product->name = $request->name;
    $product->slug = $request->slug;
    $product->type = "variant";
    $product->content = $content;
    $product->image = $image;
    $product->status = $request->status;
    $product->save();
    
    // STORE PRODUCT ATTRIBUTES
    foreach($request->option as $key => $option) {
      if(!empty($request->option[$key]) && !empty($request->value[$key])) { // PREVENT EMPTY VALUE
        $product_attribute = new ProductAttribute;
        $product_attribute->product_id = $product->id;
        $product_attribute->option = $option;
        $product_attribute->value = $request->value[$key];
        $product_attribute->save();
      }
    }
    
    // ATTACH PRODUCT TO CATEGORIES
    if($request->include_categories) {
      $include_categories = explode(",", $request->include_categories);
      
      foreach($include_categories as $key => $value) {
        $category = Category::find($value);
        $category->products()->attach($product->id);
      }
    }
    
    // STORE TAGS AND ATTACH TAGS TO PRODUCT
    if($request->tags) {
      foreach($request->tags as $key => $value) {
        $tag = Tag::firstOrCreate([
          'name' => $value,
          'slug' => unicode_slugify($value)
        ]);
        
        $product->tags()->syncWithoutDetaching($tag->id);
      }
    }
    
    // HANDLE GALLERY
    if($request->file('gallery')) {
      foreach($request->file('gallery') as $image) {
        // STORE IMAGE
        $gallery_image = $image->store('products', 'public');

        // RENAME STORED IMAGE
        $new_gallery_image = str_replace(".", rand(0, 9) . ".", $gallery_image);

        // RESIZE IMAGE AND STORE AS NEW IMAGE
        Image::load(public_path('storage/' . $gallery_image))
          ->width(600)
          ->height(600)
          ->save(public_path('storage/' . $new_gallery_image));

        // ADD WATERMARK TO IMAGE
        Image::load(public_path('storage/' . $new_gallery_image))
          ->fit(Manipulations::FIT_FILL, 600, 600)
          ->watermark(public_path('storage/watermark.png'))
          ->watermarkPosition(Manipulations::POSITION_CENTER)
          ->watermarkFit(Manipulations::FIT_FILL)
          ->optimize()
          ->save();

        $gallery = new ProductGalleries;
        $gallery->product_id = $product->id;
        $gallery->image = $new_gallery_image;
        $gallery->save();
      }
    }

    return redirect()->route('dashboard.products.variants.show', $product->id)->with('success', 'Product added.');
  }
  
  // SHOW
  public function show($id) {
    // GET VARIANT PRODUCT BY ID
    $variant_product = Product::findOrFail($id);
    
    return view('dashboard.products.variants.show', compact('variant_product'));
  }
  
  // EDIT
  public function edit($id) {
    // GET VARIANT PRODUCT BY ID
    $variant_product = Product::findOrFail($id);
    
    // GET BRANDS
    $brands = Brand::select('id', 'name')->orderBy('name')->get();
    
    // GET TAGS
    $tags = Tag::select('name')->orderBy('name')->get();
    
    // CALCULATE VARIATION COMBINATIONS
    $combinations = 1;
    
    $attributes = ProductAttribute::where('product_id', $id)->get();
    
    foreach($attributes as $attribute) {
      $combinations *= count(explode(",", $attribute->value));
    }
    
    return view('dashboard.products.variants.edit', compact('variant_product', 'brands', 'tags', 'attributes', 'combinations'));
  }
  
  // UPDATE
  public function update(Request $request, $id) {
    // FIND VARIANT PRODUCT BY ID
    $product = Product::findOrFail($id);
    
    // VALIDATION
    $request->validate([
      'name' => 'required|max:255',
      'slug' => 'required|alpha_dash|unique:products,slug,' . $id,
      'image' => 'mimes:jpg,png,jpeg|max:2048',
    ]);
    
    // HANDLE WYSIWYG
    $content = $product->content;
    
    if($request->content) {
      $html_content = mb_convert_encoding($request->content, 'HTML-ENTITIES', 'UTF-8');

      $dom = new \DomDocument();
      $dom->loadHtml($html_content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
      $dom->encoding = 'utf-8';
      
      $content_images = $dom->getElementsByTagName('img');
      
      foreach($content_images as $image){
        $data = $image->getattribute('src');
        
        if(strpos($data, 'base64') == true) {
          list($type, $data) = explode(";", $data);
          list(, $data) = explode(",", $data);

          $data = base64_decode($data);
          $image_name = uniqid() . 'a.png';
          $path = public_path('/storage/products/' . $image_name);

          file_put_contents($path, $data);

          $image->removeattribute('src');
          $image->setattribute('src', '/storage/products/' . $image_name);
        }
      }
      $content = $dom->saveHTML();
    }
    
    // UPDATE SKU
    $product->sku->brand_id = $request->brand;
    $product->sku->name = $request->name;
    $product->sku->sku = $request->sku;
    $product->sku->mfg_part_number = $request->mfg_part_number;
    $product->sku->stock = $request->stock;
    $product->sku->selling_price = $request->selling_price;
    $product->sku->sale_price = $request->sale_price;
    $product->sku->weight = $request->weight;
    $product->sku->save();
    
    // REMOVE IMAGE
    if($request->image_action == "DELETE") {
      unlink(public_path('storage/' . $product->image)); // REMOVE PROCESSED IMAGE
      unlink(public_path('storage/' . substr_replace($product->image, "", -5, 1))); // REMOVE ORIGINAL IMAGE
      $product->image = "";
    }
    
    // HANDLE IMAGE
    if($request->file('image')) {
      // STORE IMAGE
      $image = $request->file('image')->store('products', 'public');
      
      // RENAME STORED IMAGE
      $new_image = str_replace(".", rand(0, 9) . ".", $image);
      
      // RESIZE IMAGE AND STORE AS NEW IMAGE
      Image::load(public_path('storage/' . $image))
        ->width(600)
        ->height(600)
        ->save(public_path('storage/' . $new_image));
      
      // ADD WATERMARK TO IMAGE
      Image::load(public_path('storage/' . $new_image))
        ->fit(Manipulations::FIT_FILL, 600, 600)
        ->watermark(public_path('storage/watermark.png'))
        ->watermarkPosition(Manipulations::POSITION_CENTER)
        ->watermarkFit(Manipulations::FIT_FILL)
        ->optimize()
        ->save();
      
      $product->image = $new_image;
    }
    
    // UPDATE VARIANT PRODUCT
    $product->name = $request->name;
    $product->slug = $request->slug;
    $product->content = $content;
    $product->image = $product->image;
    $product->status = $request->status;
    $product->save();
    
    // STORE NEW PRODUCT ATTRIBUTES
    foreach($request->new_option as $key => $option) {
      if(!empty($request->new_option[$key]) && !empty($request->new_value[$key])) { // PREVENT EMPTY VALUE
        $product_attribute = new ProductAttribute;
        $product_attribute->product_id = $product->id;
        $product_attribute->option = $option;
        $product_attribute->value = $request->new_value[$key];
        $product_attribute->save();
      }
    }
    
    // UPDATE EXISTING PRODUCT ATTRIBUTES
    if($request->option) {
      foreach($request->option as $key => $option) {
        if(!empty($request->option[$key]) && !empty($request->value[$key])) { // PREVENT EMPTY VALUE
          $product->attributes[$key]->option = $option;
          $product->attributes[$key]->value = $request->value[$key];
          $product->attributes[$key]->save();
        }
      }
    }
    
    // STORE NEW VARIANT-SKU
    if($request->new_variant_value) {
      $new_var = $request->variant_value ? count($request->variant_value) : 0 ; // DETERMINE STARTING KEY VALUE FOR NEW VARIANT
      
      foreach($request->new_variant_value as $variants) {
        $variant = "";

        foreach($variants as $key => $value) {
          $variant .= $value . ",";
        }

        $sku = new ProductSku;
        $sku->name = $request->new_variant_name[$new_var] ? $request->new_variant_name[$new_var] : $request->name ;
        $sku->sku = $request->new_variant_sku[$new_var];
        $sku->mfg_part_number = $request->new_variant_mfg_part_number[$new_var];
        $sku->stock = $request->new_variant_stock[$new_var];
        $sku->selling_price = $request->new_variant_selling_price[$new_var];
        $sku->sale_price = $request->new_variant_sale_price[$new_var];
        $sku->weight = $request->new_variant_weight[$new_var];
        $sku->save();

        $product_variant = new ProductVariant;
        $product_variant->product_id = $id;
        $product_variant->sku_id = $sku->id;
        $product_variant->variant = rtrim($variant, ",");
        $product_variant->image = $request->file("new_variant_image") ? $request->file("new_variant_image")[$new_var]->store('products', 'public') : "" ;
        $product_variant->save();

        $new_var++;
      }
    }
    
    // UPDATE EXISTING VARIANT-SKU
    if($request->variant_value) {
      $var = 0; // ARRAY KEY FOR EXISTING VARIANT
      
      foreach($request->variant_value as $variants) {
        $variant = "";

        foreach($variants as $key => $value) {
          $variant .= $value . ",";
        }
        
        // FIND VARIANT-SKU
        $variation = ProductVariant::find($request->variant_id[$var]);
        $variation->variant = rtrim($variant, ",");
        $variation->image = $request->file("variant_image") ? $request->file("variant_image")[$var]->store('products', 'public') : $variation->image ;
        $variation->save();
        
        $variation->sku->name = $request->variant_name[$var];
        $variation->sku->sku = $request->variant_sku[$var];
        $variation->sku->mfg_part_number = $request->variant_mfg_part_number[$var];
        $variation->sku->stock = $request->variant_stock[$var];
        $variation->sku->selling_price = $request->variant_selling_price[$var];
        $variation->sku->sale_price = $request->variant_sale_price[$var];
        $variation->sku->weight = $request->variant_weight[$var];
        $variation->sku->save();
        
        $var++;
      }
    }
    
    // SYNC PRODUCT<>CATEGORIES
    if($request->include_categories) {
      $include_categories = explode(",", $request->include_categories);
      
      $product->categories()->sync($include_categories);
    }
    else {
      $product->categories()->sync($request->include_categories);
    }
    
    // STORE TAGS AND SYNC PRODUCT<>TAGS
    if($request->tags) {
      $tags = array();
      
      foreach($request->tags as $key => $value) {
        $tag = Tag::firstOrCreate([
          'name' => $value,
          'slug' => unicode_slugify($value)
        ]);
        
        array_push($tags, $tag->id);
      }
      
      $product->tags()->sync($tags);
    }
    else {
      $product->tags()->sync($request->tags);
    }
    
    // STORE GALLERY
    if($request->file('gallery')) {
      foreach($request->file('gallery') as $image) {
        // STORE IMAGE
        $gallery_image = $image->store('products', 'public');

        // RENAME STORED IMAGE
        $new_gallery_image = str_replace(".", rand(0, 9) . ".", $gallery_image);

        // RESIZE IMAGE AND STORE AS NEW IMAGE
        Image::load(public_path('storage/' . $gallery_image))
          ->width(600)
          ->height(600)
          ->save(public_path('storage/' . $new_gallery_image));

        // ADD WATERMARK TO IMAGE
        Image::load(public_path('storage/' . $new_gallery_image))
          ->fit(Manipulations::FIT_FILL, 600, 600)
          ->watermark(public_path('storage/watermark.png'))
          ->watermarkPosition(Manipulations::POSITION_CENTER)
          ->watermarkFit(Manipulations::FIT_FILL)
          ->optimize()
          ->save();
        
        $gallery = new ProductGalleries;
        $gallery->product_id = $product->id;
        $gallery->image = $new_gallery_image;
        $gallery->save();
      }
    }

    return redirect()->route('dashboard.products.variants.show', $id)->with('success', 'Product updated.');
  }
  
  // DESTROY
  public function destroy($id) {
    // GET PRODUCT BY ID
    $product = Product::findOrFail($id);
    
    // DETACH PRODUCT FROM CATEGORIES
    $product->categories()->detach();
    
    // DETACH PRODUCT FROM TAGS
    $product->tags()->detach();
    
    // DELETE IMAGE FROM PRODUCT
    if($product->image) {
      unlink(public_path('storage/' . $product->image)); // REMOVE PROCESSED IMAGE
      unlink(public_path('storage/' . substr_replace($product->image, "", -5, 1))); // REMOVE ORIGINAL IMAGE
    }
    
    // DELETE GALLERY
    if($product->galleries) {
      foreach($product->galleries as $gallery) {
        unlink(public_path('storage/' . $gallery->image)); // REMOVE PROCESSED IMAGE
        unlink(public_path('storage/' . substr_replace($gallery->image, "", -5, 1))); // REMOVE ORIGINAL IMAGE
      }
    }
    
    // DELETE VARIANT AND THEIR IMAGES
    if($product->variants) {
      foreach($product->variants as $variant) {
        unlink(public_path('storage/' . $variant->image)); // REMOVE PROCESSED IMAGE
        unlink(public_path('storage/' . substr_replace($variant->image, "", -5, 1))); // REMOVE ORIGINAL IMAGE
        $variant->sku->delete();
      }
    }
    
    // FINALLY, DELETE MAIN SKU, PRODUCT WILL ALSO BE DELETED
    $product->sku->delete();

    return redirect()->route('dashboard.products.variants.index')->with('success', 'Product deleted.');
  }
  
  // DESTROY VARIATION
  public function destroy_variation($id) {
    // GET VARIATION BY ID
    $variation = ProductVariant::findOrFail($id);
    
    // DELETE SKU
    $variation->sku->delete();
    
    // DELETE IMAGE FROM VARIATION
    if($variation->image) unlink(public_path('storage/' . $variation->image));
    
    // DELETE SKU, PRODUCT WILL ALSO BE DELETED
    $variation->sku->delete();

    return redirect()->back()->with('success', 'Variation deleted.');
  }
  
  // FETCH VARIANT PRODUCTS VIA AJAX
  public function fetch_data(Request $request) {
    if($request->ajax()) {
      $sort_by = ($request->get('sort_by')) ? $request->get('sort_by') : 'id' ;
      $sort_type = ($request->get('sort_type')) ? $request->get('sort_type') : 'desc' ;
      $query = $request->get('query');
      
      // GET VARIANT PRODUCTS
      $variant_products = Product::where('type', 'variant')->where('name', 'like', '%' . $query . '%')->orderBy($sort_by, $sort_type)->paginate(25);
      
      return view('dashboard.products.variants.datatable', compact('variant_products'))->render();
    }
  }
  
}