<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// MODELS
use App\Models\Voucher;
use App\Models\VoucherProductRestriction;
use App\Models\VoucherCategoryRestriction;
use App\Models\VoucherBrandRestriction;
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;

class VoucherController extends Controller {
  
  //INDEX
  public function index() {
    // GET VOUCHERS
    $vouchers = Voucher::latest()->paginate(25);

    return view('dashboard.vouchers.index', compact('vouchers'));
  }
  
  // CREATE
  public function create() {
    return view('dashboard.vouchers.create');
  }
  
  // STORE
  public function store(Request $request) {
    // VALIDATION
    $request->validate([
      'code' => 'required|alpha_num|unique:vouchers,code',
    ]);

    // STORE VOUCHER
    $voucher = new Voucher;
    $voucher->code = strtolower($request->code);
    $voucher->description = $request->description;
    $voucher->type = $request->type;
    $voucher->value = $request->value;
    $voucher->start_date = ($request->start_date) ? date_format(date_create($request->start_date), "Y-m-d") : null ;
    $voucher->end_date = ($request->end_date) ? date_format(date_create($request->end_date), "Y-m-d") : null ;
    $voucher->minimum_spend = $request->minimum_spend;
    $voucher->limit_per_coupon = $request->limit_per_coupon;
    $voucher->limit_per_user = $request->limit_per_user;
    $voucher->status = ($request->status) ? $request->status : "draft" ;
    $voucher->save();

    // INCLUDE PRODUCTS
    if($request->include_products) {
      $include_products = explode(",", $request->include_products);
      
      foreach($include_products as $key => $value) {
        $restriction = new VoucherProductRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->product_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // EXCLUDE PRODUCTS
    if($request->exclude_products) {
      $exclude_products = explode(",", $request->exclude_products);
      
      foreach($exclude_products as $key => $value) {
        $restriction = new VoucherProductRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->product_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    // INCLUDE PRODUCT CATEGORIES
    if($request->include_categories) {
      $include_categories = explode(",", $request->include_categories);
      
      foreach($include_categories as $key => $value) {
        $restriction = new VoucherCategoryRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->category_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // EXCLUDE PRODUCT CATEGORIES
    if($request->exclude_categories) {
      $exclude_categories = explode(",", $request->exclude_categories);
      
      foreach($exclude_categories as $key => $value) {
        $restriction = new VoucherCategoryRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->category_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    // INCLUDE BRANDS
    if($request->include_brands) {
      $include_brands = explode(",", $request->include_brands);
      
      foreach($include_brands as $key => $value) {
        $restriction = new VoucherBrandRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->brand_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // EXCLUDE BRANDS
    if($request->exclude_brands) {
      $exclude_brands = explode(",", $request->exclude_brands);
      
      foreach($exclude_brands as $key => $value) {
        $restriction = new VoucherBrandRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->brand_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    //return redirect()->route('dashboard.vouchers.show', $voucher->id)->with('success', 'Voucher added.');
    return redirect()->route('dashboard.vouchers.index')->with('success', 'Voucher added.');
  }
  
  // SHOW
  public function show($id) {
    // GET VOUCHER BY ID
    $voucher = Voucher::findOrFail($id);
    
    return view('dashboard.vouchers.show', compact('voucher'));
  }
  
  // EDIT
  public function edit($id) {
    // GET VOUCHER BY ID
    $voucher = Voucher::findOrFail($id);

    return view('dashboard.vouchers.edit', compact('voucher'));
  }
  
  // UPDATE
  public function update(Request $request, $id) {
    // FIND VOUCHER BY ID
    $voucher = Voucher::findOrFail($id);
    
    // VALIDATION
    $request->validate([
      'code' => 'required|alpha_num|unique:vouchers,code,' . $voucher->id,
    ]);

    // UPATE VOUCHER
    $voucher->code = strtolower($request->code);
    $voucher->description = $request->description;
    $voucher->type = $request->type;
    $voucher->value = $request->value;
    $voucher->start_date = ($request->start_date) ? date_format(date_create($request->start_date), "Y-m-d") : null ;
    $voucher->end_date = ($request->end_date) ? date_format(date_create($request->end_date), "Y-m-d") : null ;
    $voucher->minimum_spend = $request->minimum_spend;
    $voucher->limit_per_coupon = $request->limit_per_coupon;
    $voucher->limit_per_user = $request->limit_per_user;
    $voucher->status = ($request->status) ? $request->status : "draft" ;
    $voucher->save();
    
    // "SYNC" VOUCHER<>PRODUCT RESTRICTIONS
    VoucherProductRestriction::where('voucher_id', $id)->where('include', 1)->delete();
    
    if($request->include_products) {
      $include_products = explode(",", $request->include_products);
      
      foreach($include_products as $key => $value) {
        $restriction = new VoucherProductRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->product_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // "SYNC" VOUCHER<>PRODUCT RESTRICTIONS
    VoucherProductRestriction::where('voucher_id', $id)->where('include', 0)->delete();
    
    if($request->exclude_products) {
      $exclude_products = explode(",", $request->exclude_products);
      
      foreach($exclude_products as $key => $value) {
        $restriction = new VoucherProductRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->product_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    // "SYNC" VOUCHER<>CATEGORY RESTRICTIONS
    VoucherCategoryRestriction::where('voucher_id', $id)->where('include', 1)->delete();
    
    if($request->include_categories) {
      $include_categories = explode(",", $request->include_categories);
      
      foreach($include_categories as $key => $value) {
        $restriction = new VoucherCategoryRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->category_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // "SYNC" VOUCHER<>CATEGORY RESTRICTIONS
    VoucherCategoryRestriction::where('voucher_id', $id)->where('include', 0)->delete();
    
    if($request->exclude_categories) {
      $exclude_categories = explode(",", $request->exclude_categories);
      
      foreach($exclude_categories as $key => $value) {
        $restriction = new VoucherCategoryRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->category_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    // "SYNC" VOUCHER<>CATEGORY RESTRICTIONS
    VoucherBrandRestriction::where('voucher_id', $id)->where('include', 1)->delete();
    
    if($request->include_brands) {
      $include_brands = explode(",", $request->include_brands);
      
      foreach($include_brands as $key => $value) {
        $restriction = new VoucherBrandRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->brand_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // "SYNC" VOUCHER<>CATEGORY RESTRICTIONS
    VoucherBrandRestriction::where('voucher_id', $id)->where('include', 0)->delete();
    
    if($request->exclude_brands) {
      $exclude_brands = explode(",", $request->exclude_brands);
      
      foreach($exclude_brands as $key => $value) {
        $restriction = new VoucherBrandRestriction;
        $restriction->voucher_id = $voucher->id;
        $restriction->brand_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    //return redirect()->route('dashboard.vouchers.show', $voucher->id)->with('success', 'Voucher updated.');
    return redirect()->route('dashboard.vouchers.index')->with('success', 'Voucher updated.');
  }
  
  // DESTROY
  public function destroy($id) {
    // GET VOUCHER BY ID
    $voucher = Voucher::findOrFail($id);
    
    // DELETE VOUCHER
    $voucher->delete();

    return redirect()->route('dashboard.vouchers.index')->with('success', 'Voucher deleted.');
  }
  
  // FETCH VOUCHERS
  public function fetch_data(Request $request) {
    if($request->ajax()) {
      $sort_by = ($request->get('sort_by')) ? $request->get('sort_by') : 'id' ;
      $sort_type = ($request->get('sort_type')) ? $request->get('sort_type') : 'desc' ;
      $query = $request->get('query');
      
      // GET VOUCHERS
      $vouchers = Voucher::where('code', 'like', '%' . $query . '%')->orWhere('type', 'like', '%' . $query . '%')->orderBy($sort_by, $sort_type)->paginate(25);
      
      return view('dashboard.vouchers.datatable', compact('vouchers'))->render();
    }
  }
  
}
