<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// MODELS
use App\Models\Category;

class CategoryController extends Controller {
  
  // INDEX
  public function index() {
    // GET TIER 1 CATEGORIES
    $t1_categories = Category::where('parent_id', 0)->get();
    
    return view('dashboard.categories.index', compact('t1_categories'));
  }
  
  // CREATE
  public function create() {
    // GET TIER 1 CATEGORIES
    $t1_categories = Category::select('id', 'name')->where('parent_id', 0)->get();
    
    return view('dashboard.categories.create', compact('t1_categories'));
  }
  
  // STORE
  public function store(Request $request) {
    // VALIDATION
    $request->validate([
      'name' => 'required|max:255',
      'slug' => 'required|alpha_dash|unique:categories,slug',
      'image' => 'mimes:jpg,png,jpeg|max:2048',
    ]);
    
    // STORE CATEGORY
    $category = new Category;
    $category->name = $request->name;
    $category->slug = $request->slug;
    $category->parent_id = $request->parent_category;
    $category->image = $request->file('image') ? $request->file('image')->store('categories', 'public') : "" ;
    $category->save();

    return redirect()->route('dashboard.categories.edit', $category->id)->with('success', 'Category added.');
  }
  
  // EDIT
  public function edit($id) {
    // GET CATEGORY BY ID
    $category = Category::findOrFail($id);
    
    // GET TIER 1 CATEGORIES
    $t1_categories = Category::select('id', 'name')->where('parent_id', 0)->get();
    
    return view('dashboard.categories.edit', compact('category', 't1_categories'));
  }
  
  // UPDATE
  public function update(Request $request, $id) {
    // FIND CATEGORY BY ID
    $category = Category::findOrFail($id);
    
    // VALIDATION
    $request->validate([
      'name' => 'required|max:255',
      'slug' => 'required|alpha_dash|unique:categories,slug,' . $id,
      'image' => 'mimes:jpg,png,jpeg|max:2048',
    ]);
    
    // REMOVE IMAGE
    if($request->image_action == "DELETE") {
      unlink(public_path('storage/' . $category->image));
      $category->image = "";
    }
    
    // UPDATE CATEGORY
    $category->name = $request->name;
    $category->slug = $request->slug;
    $category->parent_id = $request->parent_category;
    $category->image = $request->file('image') ? $request->file('image')->store('categories', 'public') : $category->image ;
    $category->save();

    return redirect()->route('dashboard.categories.edit', $id)->with('success', 'Category updated.');
  }
  
  // DESTROY
  public function destroy($id) {
    // GET CATEGORY BY ID
    $category = Category::findOrFail($id);
    
    // DETACH PRODUCT FROM CATEGORY(S)
    $category->products()->detach();
    
    // DELETE IMAGE FROM CATEGORY
    if($category->image) unlink(public_path('storage/' . $category->image));
    
    // FINALLY, DELETE CATEGORY
    $category->delete();

    return redirect()->route('dashboard.categories.index')->with('success', 'Category deleted.');
  }

}
