<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// MODELS
use App\Models\Product;
use App\Models\Category;

class ProductController extends Controller {
  
  // INDEX
  public function index() {
    $products = Product::paginate(25);
    
    return view('dashboard.products.index', compact('products'));
  }
  
  // CREATE
  public function create() {
    
    return view('dashboard.products.create');
  }
  
  // EDIT
  public function edit($id) {
    // GET PRODUCT BY ID
    $product = Product::findOrFail($id);
    
    // GET CATEGORIES
    $categories = Category::all('id', 'name');
    
    // GET SELECTED CATEGORIES
    $selected_categories = Product::find($id)->categories;
    
    return view('dashboard.products.edit', compact('product', 'categories', 'selected_categories'));
  }
  
  // UPDATE
  public function update(Request $request, $id) {
    // VALIDATION
    $request->validate([
      'name' => 'required',
      'slug' => 'required|alpha_dash|unique:product,slug',
      'sku' => 'required',
    ]);
    
    // product values - name, slug, content, image, status
    // sku values - name, sku, mfg, selling_price, sale_price, weight

    // COUPON
    $coupon = Coupon::findOrFail($id);
    $coupon->code = strtolower($request->code);
    $coupon->description = $request->description;
    $coupon->type = $request->type;
    $coupon->value = $request->value;
    $coupon->expiry_date = ($request->expiry_date) ? date_format(date_create($request->expiry_date), "Y-m-d") : null ;
    $coupon->limit = ($request->limit) ? $request->limit : 0 ;
    $coupon->save();

    // INCLUDE PRODUCTS
    if($request->include_products) {
      foreach($request->include_products as $key => $value) {
        $restriction = new CouponRestriction;
        $restriction->coupon_id = $coupon->id;
        $restriction->product_id = $value;
        $restriction->include = 1;
        $restriction->save();
      }
    }

    // EXCLUDE PRODUCTS
    if($request->exclude_products) {
      foreach($request->exclude_products as $key => $value) {
        $restriction = new CouponRestriction;
        $restriction->coupon_id = $coupon->id;
        $restriction->product_id = $value;
        $restriction->include = 0;
        $restriction->save();
      }
    }

    return redirect()->route('dashboard.coupons.index')->with('success', 'Coupon updated.');
  }

}
