<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// MODELS
use App\Models\Product;
use App\Models\Category;
use App\Models\Brand;

class DataTableController extends Controller {
  
  // FETCH PRODUCTS
  public function fetch_products(Request $request) {
    if($request->ajax()) {
      // GET PRODUCTS
      if($request->get('query')) {
        $products = Product::where('name', 'like', '%' . $query = $request->get('query') . '%')->where('status', 'published')->paginate(10);
      }
      else {
        $products = Product::where('status', 'published')->orderBy('name', 'asc')->paginate(10);
      }
      
      // SET SELECTED AS ARRAY
      $selected = explode(",", $request->get('selected'));

      return view('dashboard.datatables.products', compact('products', 'selected'))->render();
    }
  }
  
  // FETCH SELECTED PRODUCTS
  public function fetch_selected_products(Request $request) {
    if($request->ajax()) {
      // SET SAVED AS ARRAY
      $saved = explode(",", $request->get('saved'));

      // GET PRODUCTS
      $products = Product::whereIn('id', $saved)->get();

      return view('dashboard.datatables.selected_products', compact('products'))->render();
    }
  }
  
  // FETCH CATEGORIES
  public function fetch_categories(Request $request) {
    if($request->ajax()) {
      // GET CATEGORIES
      if($request->get('query')) {
        $categories = Category::where('name', 'like', '%' . $request->get('query') . '%')->paginate(10);
      }
      else {
        $categories = Category::paginate(10);
      }
      
      // SET SELECTED AS ARRAY
      $selected = explode(",", $request->get('selected'));

      return view('dashboard.datatables.categories', compact('categories', 'selected'))->render();
    }
  }
  
  // FETCH SELECTED CATEGORIES
  public function fetch_selected_categories(Request $request) {
    if($request->ajax()) {
      // SET SAVED AS ARRAY
      $saved = explode(",", $request->get('saved'));

      // GET CATEGORIES
      $categories = Category::whereIn('id', $saved)->get();

      return view('dashboard.datatables.selected_categories', compact('categories'))->render();
    }
  }
  
  // FETCH BRANDS
  public function fetch_brands(Request $request) {
    if($request->ajax()) {
      // GET BRANDS
      if($request->get('query')) {
        $brands = Brand::where('name', 'like', '%' . $request->get('query') . '%')->paginate(10);
      }
      else {
        $brands = Brand::paginate(10);
      }
      
      // SET SELECTED AS ARRAY
      $selected = explode(",", $request->get('selected'));

      return view('dashboard.datatables.brands', compact('brands', 'selected'))->render();
    }
  }
  
  // FETCH SELECTED BRANDS
  public function fetch_selected_brands(Request $request) {
    if($request->ajax()) {
      // SET SAVED AS ARRAY
      $saved = explode(",", $request->get('saved'));

      // GET BRANDS
      $brands = Brand::whereIn('id', $saved)->get();

      return view('dashboard.datatables.selected_brands', compact('brands'))->render();
    }
  }
  
}