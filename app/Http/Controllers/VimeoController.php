<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Vimeo Helper
use Vimeo\Vimeo;

class VimeoController extends Controller {
  
  // VIMEO
  public function index() {
    $response = array();
    return view('dashboard.vimeo.index', compact('response'));
  }
  
  // VIMEO UPLOAD
  public function upload(Request $request) {
    $vimeo = new Vimeo(
      config('services.vimeo.client_id'),
      config('services.vimeo.client_secret'),
      config('services.vimeo.access_token')
    );
    
    $response = $vimeo->request(
      '/me/videos',
      [
        'upload' => [
          'approach' => 'pull',
          'link' => $request->url
        ],
        'privacy.embed' => "private"
      ],
      'POST'
    );
    
    return view('dashboard.vimeo.index', compact('response'));
  }
  
}
