<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// MODELS
use App\Models\Brand;

class BrandController extends Controller {
  
  // INDEX
  public function index() {
    // GET BRANDS
    $brands = Brand::orderByDesc('id')->latest()->paginate(25);
    
    return view('dashboard.brands.index', compact('brands'));
  }
  
  // CREATE
  public function create() {
    return view('dashboard.brands.create');
  }
  
  // STORE
  public function store(Request $request) {
    // VALIDATION
    $request->validate([
      'name' => 'required|max:255',
      'slug' => 'required|alpha_dash|unique:brands,slug',
      'image' => 'mimes:jpg,png,jpeg|max:2048',
    ]);
    
    // STORE BRAND
    $brand = new Brand;
    $brand->name = $request->name;
    $brand->slug = $request->slug;
    $brand->status = $request->status;
    $brand->image = $request->file('image') ? $request->file('image')->store('brands', 'public') : "" ;
    $brand->save();

    return redirect()->route('dashboard.brands.edit', $brand->id)->with('success', 'Brand added.');
  }
  
  // EDIT
  public function edit($id) {
    // GET BRAND BY ID
    $brand = Brand::findOrFail($id);
    
    return view('dashboard.brands.edit', compact('brand'));
  }
  
  // UPDATE
  public function update(Request $request, $id) {
    // FIND BRAND BY ID
    $brand = Brand::findOrFail($id);
    
    // VALIDATION
    $request->validate([
      'name' => 'required|max:255',
      'slug' => 'required|alpha_dash|unique:categories,slug,' . $id,
      'image' => 'mimes:jpg,png,jpeg|max:2048',
    ]);
    
    // REMOVE IMAGE
    if($request->image_action == "DELETE") {
      unlink(public_path('storage/' . $brand->image));
      $brand->image = "";
    }
    
    // UPDATE BRAND
    $brand->name = $request->name;
    $brand->slug = $request->slug;
    $brand->status = $request->status;
    $brand->image = $request->file('image') ? $request->file('image')->store('brands', 'public') : $brand->image ;
    $brand->save();

    return redirect()->route('dashboard.brands.edit', $id)->with('success', 'Brand updated.');
  }
  
  // DESTROY
  public function destroy($id) {
    // GET BRAND BY ID
    $brand = Brand::findOrFail($id);
    
    // DELETE IMAGE FROM BRAND
    if($brand->image) unlink(public_path('storage/' . $brand->image));
    
    // FINALLY, DELETE BRAND
    $brand->delete();

    return redirect()->route('dashboard.brands.index')->with('success', 'Brand deleted.');
  }
  
  // FETCH BRANDS VIA AJAX
  public function fetch_data(Request $request) {
    if($request->ajax()) {
      $sort_by = ($request->get('sort_by')) ? $request->get('sort_by') : 'id' ;
      $sort_type = ($request->get('sort_type')) ? $request->get('sort_type') : 'desc' ;
      $query = $request->get('query');
      
      // GET BRANDS
      $brands = Brand::where('name', 'like', '%' . $query . '%')->orderBy($sort_by, $sort_type)->paginate(25);
      
      return view('dashboard.brands.datatable', compact('brands'))->render();
    }
  }
  
}
