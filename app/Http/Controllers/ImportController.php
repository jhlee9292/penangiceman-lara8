<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Imports\ProductsImport;
use Maatwebsite\Excel\Facades\Excel;

class ImportController extends Controller {
  
  // PDF
  public function index() {
    return view('dashboard.import.index');
  }
  
  // PDF CREATE
  public function create(Request $request) {
    Excel::import(new ProductsImport, $request->file('file'));

    return redirect()->route('dashboard.import.index')->with('success', 'Products imported.');
  }
  
}
