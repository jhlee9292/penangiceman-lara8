<?php

namespace App\Imports;

use App\Models\SampleProduct;
use Maatwebsite\Excel\Concerns\ToModel;

class ProductsImport implements ToModel {
  
  public function model(array $row) {
    return new SampleProduct([
      'name' => $row[0],
      'quantity' => $row[1],
    ]);
  }
  
}
