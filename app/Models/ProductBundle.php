<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductBundle extends Model {
  
  use HasFactory;
  
  public $timestamps = false;

  protected $fillable = [
    'product_id',
    'bundled_product_id',
  ];
  
  public function product() {
    return $this->belongsTo(Product::class, 'bundled_product_id');
  }
  
}
