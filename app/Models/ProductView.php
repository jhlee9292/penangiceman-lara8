<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductView extends Model {
  
  use HasFactory;
  
  public $timestamps = false;

  protected $fillable = [
    'product_id',
    'ip_address',
    'count',
    'last_seen',
  ];
  
  public function product() {
    return $this->belongsTo(Product::class);
  }
  
}
