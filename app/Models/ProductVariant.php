<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model {
  
  use HasFactory;
  
  public $table = "product_variations";
  public $timestamps = false;

  protected $fillable = [
    'product_id',
    'sku_id',
    'variant',
    'image',
  ];
  
  public function product() {
    return $this->belongsTo(Product::class);
  }

  public function sku() {
    return $this->belongsTo(ProductSku::class);
  }
  
  
  // ACCESSORS
  public function getTheImageAttribute() {
    return !$this->image ? asset('dashboard/assets/images/placeholder.jpeg') : asset('storage') . "/" . $this->image ;
  }
  
}
