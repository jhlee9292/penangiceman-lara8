<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
  
  use HasFactory;

  protected $fillable = [
    'sku_id',
    'name',
    'slug',
    'type',
    'content',
    'image',
    'status',
  ];
  
  public function categories() {
    return $this->belongsToMany(Category::class);
  }
  
  public function tags() {
    return $this->belongsToMany(Tag::class);
  }
  
  public function sku() {
    return $this->belongsTo(ProductSku::class);
  }
  
  public function brand() {
    return $this->hasOneThrough(Brand::class, ProductSku::class, 'brand_id', 'id', 'sku_id', 'id');
  }
  
  public function bundles() {
    return $this->hasMany(ProductBundle::class);
  }
  
  public function attributes() {
    return $this->hasMany(ProductAttribute::class);
  }
  
  public function variants() {
    return $this->hasMany(ProductVariant::class);
  }

  public function galleries() {
    return $this->hasMany(ProductGalleries::class);
  }

  public function voucher_restrictions() {
    return $this->hasMany(VoucherProductRestriction::class);
  }

  public function views() {
    return $this->hasMany(ProductView::class);
  }

  public function user_relations() {
    return $this->hasMany(ProductUserRelation::class);
  }
  
  
  // ACCESSORS
  public function getTheImageAttribute() {
    return !$this->image ? asset('dashboard/assets/images/placeholder.jpeg') : asset('storage') . "/" . $this->image ;
  }
  
}
