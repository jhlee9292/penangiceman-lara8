<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherCategoryRestriction extends Model {
  
  use HasFactory;
  
  public $timestamps = false;

  protected $fillable = [
    'coupon_id',
    'category_id',
    'include',
  ];

  public function voucher() {
    return $this->belongsTo(Voucher::class);
  }
  
  public function category() {
    return $this->belongsTo(Category::class, 'category_id');
  }
  
}
