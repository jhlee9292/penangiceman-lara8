<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherProductRestriction extends Model {
  
  use HasFactory;
  
  public $timestamps = false;

  protected $fillable = [
    'coupon_id',
    'product_id',
    'include',
  ];

  public function voucher() {
    return $this->belongsTo(Voucher::class);
  }
  
  public function product() {
    return $this->belongsTo(Product::class, 'product_id');
  }
  
}
