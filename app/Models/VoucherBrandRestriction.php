<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VoucherBrandRestriction extends Model {
  
  use HasFactory;
  
  public $timestamps = false;

  protected $fillable = [
    'coupon_id',
    'brand_id',
    'include',
  ];

  public function voucher() {
    return $this->belongsTo(Voucher::class);
  }
  
  public function brand() {
    return $this->belongsTo(Brand::class, 'brand_id');
  }
  
}
