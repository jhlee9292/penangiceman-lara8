<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductGalleries extends Model {
  
    use HasFactory;
    
    protected $table = "product_galleries";
    public $timestamps = false;

    protected $fillable = [
        "product_id",
        "image"
    ];
    
    public function product() {
        return $this->belongsTo(Product::class);
    }


    // ACCESSORS
    public function getTheImageAttribute() {
      $imgURL = "";
      if( ! $this->image ) $imgURL = asset('dashboard/assets/images/placeholder.jpeg');
      else $imgURL = asset('storage') . "/" . $this->image;
      return $imgURL;
    }
}
