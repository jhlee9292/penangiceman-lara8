<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model {
    
  use HasFactory;

  protected $fillable = [
    'name',
    'slug',
    'status',
    'image',
  ];

  public function skus() {
    return $this->hasMany(ProductSku::class);
  }

  public function products() {
    return $this->hasManyThrough(Product::class, ProductSku::class, 'brand_id', 'sku_id', 'id', 'id');
  }
  
  
  // ACCESSORS
  public function getTheImageAttribute() {
    return (!$this->image) ? asset('dashboard/assets/images/placeholder.jpeg') : asset('storage') . "/" . $this->image ;
  }
  
}
