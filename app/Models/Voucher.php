<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Voucher extends Model {
  
  use HasFactory;

  protected $fillable = [
    'code',
    'description',
    'type',
    'value',
    'start_date',
    'end_date',
    'minimum_spend',
    'limit_per_coupon',
    'limit_per_user',
    'status',
  ];

  public function product_restrictions() {
    return $this->hasMany(VoucherProductRestriction::class);
  }

  public function category_restrictions() {
    return $this->hasMany(VoucherCategoryRestriction::class);
  }

  public function brand_restrictions() {
    return $this->hasMany(VoucherBrandRestriction::class);
  }
  
}
