<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
  
  use HasFactory;

  protected $fillable = [
    'name',
    'slug',
    'parent_id',
    'image',
  ];

  public function parent() {
    return $this->belongsTo(Category::class, 'parent_id');
  }

  public function products() {
    return $this->belongsToMany(Product::class);
  }

  public function voucher_restrictions() {
    return $this->hasMany(VoucherProductCategoryRestriction::class);
  }
  
  
  // ACCESSORS
  public function getTheImageAttribute() {
    return (!$this->image) ? asset('dashboard/assets/images/placeholder.jpeg') : asset('storage') . "/" . $this->image ;
  }
  
  public function getTheNameAttribute() {
    return ($this->parent) ? $this->parent->name . " — " . $this->name : $this->name ;
  }
  
}
