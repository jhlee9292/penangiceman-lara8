<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductSku extends Model {
  
  use HasFactory;

  protected $fillable = [
    'brand_id',
    'name',
    'sku',
    'mfg_part_number',
    'stock',
    'selling_price',
    'sale_price',
    'weight',
  ];
  
  public function product() {
    return $this->hasOne(Product::class, 'sku_id');
  }
  
  public function variations() {
    return $this->belongsToMany(ProductVariant::class);
  }
  
  public function brand() {
    return $this->belongsTo(Brand::class);
  }
  
}
