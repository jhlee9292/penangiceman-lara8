<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

// Helper
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider {
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set defauly varchar length
        Schema::defaultStringLength(255); // to resolve max string error
      
        // Paginator
        Paginator::useBootstrap();
    }
}
