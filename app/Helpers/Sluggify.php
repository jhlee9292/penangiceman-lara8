<?php

function unicode_slugify($string, $delimiter = '-') {
    $string = preg_replace("/[~`{}.'\"\!\@\#\$\%\^\&\*\(\)\_\=\+\/\?\>\<\,\[\]\:\;\|\\\]/", "", $string);
    $string = preg_replace("/[\/_|+ -]+/", $delimiter, $string);
    $string = strtolower($string);
    return $string;
}