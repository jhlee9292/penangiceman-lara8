<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductGalleriesTable extends Migration {
    public function up() {
        Schema::create('product_galleries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('product_id');
            $table->string('image');

            // foreign key
            // ------------------------------------------
            // NOTE:: no need cascade delete 
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    public function down() {
        // drop foreign first before drop table
        Schema::table('product_galleries', function (Blueprint $table) {
            $table->dropForeign('product_galleries_product_id_foreign');
            $table->dropColumn('product_id');
        });
        Schema::dropIfExists('product_galleries');
    }
}
