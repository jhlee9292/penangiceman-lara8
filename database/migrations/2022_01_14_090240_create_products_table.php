<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration {

    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name");
            $table->string("slug");
            $table->string("sku")->nullable();
            $table->string("mfgPartNumber")->nullable();
            $table->string("image")->nullable();
            $table->decimal("selling_price", 13, 4);
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->timestamps();

            // foreign key
            // ------------------------------------------
            // NOTE:: no need cascade delete 
            $table->foreign('brand_id')->references('id')->on('brands');
        });
    }

    public function down() {

        // drop foreign first before drop table
        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('products_brand_id_foreign');
            $table->dropColumn('brand_id');
        });
        Schema::dropIfExists('products');
    }
    
}
