<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {
    
    public function run() {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        // main super admin
        $user = User::create([
            'name'      => "Joo Han",
            'email'     => "jhlee25122@gmail.com",
            'password'  => bcrypt(env('admin_password')),
        ]);
        $user->assignRole('SUPER_ADMIN');
    }
}