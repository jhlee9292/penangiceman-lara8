<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder {

    public function run() {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
                
        DB::table('roles')->truncate();
        DB::table('model_has_roles')->truncate();
  
        Role::create( [ 'name' => 'SUBSCRIBER'      , 'description' => 'Email verified user'] );
        Role::create( [ 'name' => 'SUPER_ADMIN'     , 'description' => 'Super Admin'] ) ;
        Role::create( [ 'name' => 'SHOP_MANAGER'    , 'description' => 'Shop Manager'] ) ;
        Role::create( [ 'name' => 'ADMIN'           , 'description' => 'Admin Staff'] ) ;
 
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
