<?php

use Illuminate\Support\Facades\Route;

// CONTROLLERS
use App\Http\Controllers\DataTableController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProductSimpleController;
use App\Http\Controllers\ProductBundleController;
use App\Http\Controllers\ProductVariantController;
use App\Http\Controllers\ProductGalleriesController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\VoucherController;
use App\Http\Controllers\BitlyController;
use App\Http\Controllers\VimeoController;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\ExportController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\DOSpacesController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

// Limit User Registration Entry
if( config('app.LIVE_MODE') == true ) {
  Auth::routes([ 'register' => true, 'verify'  => true, ]);
} else {
  Auth::routes([ 'register' => false, 'verify' => false, ]); // CHANGE BACK TO FALSE WHEN READY
}

// In some cases laravel 301 redirect user to /home
// Chrome issue not laravel issue
Route::get('/home', function() {
  return redirect()->route('home.index');
});

// Frontend routes
if( config('app.LIVE_MODE') == true ) {
  
  // Home Page
  Route::get('/', [PageController::class, 'index'])->name('home.index');

  // Shop Page
  Route::get('/shop', [PageController::class, 'renderShopPage'])->name('shop.index');

  // Single Product Page
  Route::get('/products/single', [PageController::class, 'renderSingleProductPage'])->name('products.single.index');


  
  // My Account Page
  Route::prefix('my-account')->middleware(['auth'])->group( function() {

    //  My Account Dashboard
    Route::get('/', [PageController::class, 'renderMyAccountPage'])->name('myaccount.index');

  });
  


} else {
  // HIDE accessible routes if coming soon / under maintenance
  Route::get('/', [PageController::class, 'renderComingSoonPage'])->name('home.index');
}


/*
|--------------------------------------------------------------------------
| Dashboard Routes
|--------------------------------------------------------------------------
*/

if ( config('app.ALLOW_DASHBOARD') == true ) {

  Route::prefix('my-dashboard')->middleware(['auth', 'role:SUPER_ADMIN'])->group( function() {

    // DASHBOARD
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard.home.index');
    
    // SLUGIFY :: AJAX Only
    Route::get('/slugify/{name}', function($name) {
      if( $request->ajax() ) {
        return unicode_slugify($name);
      } 
      die();
    })->name('dashboard.slugify');
    

    /*
    |--------------------------------------------------------------------------
    | PRODUCTS MANAGEMENT
    |--------------------------------------------------------------------------
    */

    // PRODUCTS
    Route::get('/products', [ProductController::class, 'index'])->name('dashboard.products.index');
    Route::get('/products/create', [ProductController::class, 'create'])->name('dashboard.products.create');
    Route::post('/products/store', [ProductController::class, 'store'])->name('dashboard.products.store');
    Route::get('/products/{id}/edit', [ProductController::class, 'edit'])->name('dashboard.products.edit');
    Route::put('/products/{id}/update', [ProductController::class, 'update'])->name('dashboard.products.update');
    Route::get('/products/{id}/destroy', [ProductController::class, 'destroy'])->name('dashboard.products.destroy');
    
    // PRODUCTS - SIMPLE
    Route::get('/products/simples', [ProductSimpleController::class, 'index'])->name('dashboard.products.simples.index');
    Route::get('/products/simples/create', [ProductSimpleController::class, 'create'])->name('dashboard.products.simples.create');
    Route::post('/products/simples/store', [ProductSimpleController::class, 'store'])->name('dashboard.products.simples.store');
    Route::get('/products/simples/{id}/show', [ProductSimpleController::class, 'show'])->name('dashboard.products.simples.show');
    Route::get('/products/simples/{id}/edit', [ProductSimpleController::class, 'edit'])->name('dashboard.products.simples.edit');
    Route::put('/products/simples/{id}/update', [ProductSimpleController::class, 'update'])->name('dashboard.products.simples.update');
    Route::get('/products/simples/{id}/destroy', [ProductSimpleController::class, 'destroy'])->name('dashboard.products.simples.destroy');
    // AJAX DATA TABLE
    Route::get('/products/simples/fetch_data', [ProductSimpleController::class, 'fetch_data'])->name('dashboard.products.simples.fetch_data');
    
    // PRODUCTS - BUNDLED
    Route::get('/products/bundles', [ProductBundleController::class, 'index'])->name('dashboard.products.bundles.index');
    Route::get('/products/bundles/create', [ProductBundleController::class, 'create'])->name('dashboard.products.bundles.create');
    Route::post('/products/bundles/store', [ProductBundleController::class, 'store'])->name('dashboard.products.bundles.store');
    Route::get('/products/bundles/{id}/show', [ProductBundleController::class, 'show'])->name('dashboard.products.bundles.show');
    Route::get('/products/bundles/{id}/edit', [ProductBundleController::class, 'edit'])->name('dashboard.products.bundles.edit');
    Route::put('/products/bundles/{id}/update', [ProductBundleController::class, 'update'])->name('dashboard.products.bundles.update');
    Route::get('/products/bundles/{id}/destroy', [ProductBundleController::class, 'destroy'])->name('dashboard.products.bundles.destroy');
    // AJAX DATA TABLE
    Route::get('/products/bundles/fetch_data', [ProductBundleController::class, 'fetch_data'])->name('dashboard.products.bundles.fetch_data');
    
    // PRODUCTS - VARIANT
    Route::get('/products/variants', [ProductVariantController::class, 'index'])->name('dashboard.products.variants.index');
    Route::get('/products/variants/create', [ProductVariantController::class, 'create'])->name('dashboard.products.variants.create');
    Route::post('/products/variants/store', [ProductVariantController::class, 'store'])->name('dashboard.products.variants.store');
    Route::get('/products/variants/{id}/show', [ProductVariantController::class, 'show'])->name('dashboard.products.variants.show');
    Route::get('/products/variants/{id}/edit', [ProductVariantController::class, 'edit'])->name('dashboard.products.variants.edit');
    Route::put('/products/variants/{id}/update', [ProductVariantController::class, 'update'])->name('dashboard.products.variants.update');
    Route::get('/products/variants/{id}/destroy', [ProductVariantController::class, 'destroy'])->name('dashboard.products.variants.destroy');
    Route::get('/products/variations/{id}/destroy', [ProductVariantController::class, 'destroy_variation'])->name('dashboard.products.variations.destroy');
    // AJAX DATA TABLE
    Route::get('/products/variants/fetch_data', [ProductVariantController::class, 'fetch_data'])->name('dashboard.products.variants.fetch_data');
    
    // PRODUCT - GALLERY
    Route::get('/products/galleries/{id}/destroy', [ProductGalleriesController::class, 'destroy'])->name('dashboard.products.galleries.destroy');
    
    
    /*
    |--------------------------------------------------------------------------
    | BRANDS MANAGEMENT
    |--------------------------------------------------------------------------
    */
    
    // BRANDS
    Route::get('/brands', [BrandController::class, 'index'])->name('dashboard.brands.index');
    Route::get('/brands/create', [BrandController::class, 'create'])->name('dashboard.brands.create');
    Route::post('/brands/store', [BrandController::class, 'store'])->name('dashboard.brands.store');
    Route::get('/brands/{id}/edit', [BrandController::class, 'edit'])->name('dashboard.brands.edit');
    Route::put('/brands/{id}/update', [BrandController::class, 'update'])->name('dashboard.brands.update');
    Route::get('/brands/{id}/destroy', [BrandController::class, 'destroy'])->name('dashboard.brands.destroy');
    // AJAX DATA TABLE
    Route::get('/brands/fetch_data', [BrandController::class, 'fetch_data'])->name('dashboard.brands.fetch_data');
    
    
    /*
    |--------------------------------------------------------------------------
    | CATEGORIES MANAGEMENT
    |--------------------------------------------------------------------------
    */
    
    // CATEGORIES
    Route::get('/categories', [CategoryController::class, 'index'])->name('dashboard.categories.index');
    Route::get('/categories/create', [CategoryController::class, 'create'])->name('dashboard.categories.create');
    Route::post('/categories/store', [CategoryController::class, 'store'])->name('dashboard.categories.store');
    Route::get('/categories/{id}/edit', [CategoryController::class, 'edit'])->name('dashboard.categories.edit');
    Route::put('/categories/{id}/update', [CategoryController::class, 'update'])->name('dashboard.categories.update');
    Route::get('/categories/{id}/destroy', [CategoryController::class, 'destroy'])->name('dashboard.categories.destroy');
    // AJAX DATA TABLE
    Route::get('/categories/fetch_data', [CategoryController::class, 'fetch_data'])->name('dashboard.categories.fetch_data');
    
    
    /*
    |--------------------------------------------------------------------------
    | VOUCHERS MANAGEMENT
    |--------------------------------------------------------------------------
    */
    
    // VOUCHERS
    Route::get('/vouchers', [VoucherController::class, 'index'])->name('dashboard.vouchers.index');
    Route::get('/vouchers/create', [VoucherController::class, 'create'])->name('dashboard.vouchers.create');
    Route::post('/vouchers/store', [VoucherController::class, 'store'])->name('dashboard.vouchers.store');
    Route::get('/vouchers/{id}/show', [VoucherController::class, 'show'])->name('dashboard.vouchers.show');
    Route::get('/vouchers/{id}/edit', [VoucherController::class, 'edit'])->name('dashboard.vouchers.edit');
    Route::put('/vouchers/{id}/update', [VoucherController::class, 'update'])->name('dashboard.vouchers.update');
    Route::get('/vouchers/{id}/destroy', [VoucherController::class, 'destroy'])->name('dashboard.vouchers.destroy');
    // AJAX DATA TABLE
    Route::get('/vouchers/fetch_data', [VoucherController::class, 'fetch_data'])->name('dashboard.vouchers.fetch_data');
    
    // VOUCHERS - FLASH SALES
    Route::get('/flashsales', [FlashsaleController::class, 'index'])->name('dashboard.flashsales.index');
    Route::get('/flashsales/create', [FlashsaleController::class, 'create'])->name('dashboard.flashsales.create');
    Route::post('/flashsales/store', [FlashsaleController::class, 'store'])->name('dashboard.flashsales.store');
    Route::get('/flashsales/{id}/show', [FlashsaleController::class, 'show'])->name('dashboard.flashsales.show');
    Route::get('/flashsales/{id}/edit', [FlashsaleController::class, 'edit'])->name('dashboard.flashsales.edit');
    Route::put('/flashsales/{id}/update', [FlashsaleController::class, 'update'])->name('dashboard.flashsales.update');
    Route::get('/flashsales/{id}/destroy', [FlashsaleController::class, 'destroy'])->name('dashboard.flashsales.destroy');
    
    
    /*
    |--------------------------------------------------------------------------
    | DATA TABLES
    |--------------------------------------------------------------------------
    */
    Route::get('/datatables/fetch_products', [DataTableController::class, 'fetch_products'])->name('dashboard.datatables.fetch_products');
    Route::get('/datatables/fetch_selected_products', [DataTableController::class, 'fetch_selected_products'])->name('dashboard.datatables.fetch_selected_products');
    Route::get('/datatables/fetch_categories', [DataTableController::class, 'fetch_categories'])->name('dashboard.datatables.fetch_categories');
    Route::get('/datatables/fetch_selected_categories', [DataTableController::class, 'fetch_selected_categories'])->name('dashboard.datatables.fetch_selected_categories');
    Route::get('/datatables/fetch_brands', [DataTableController::class, 'fetch_brands'])->name('dashboard.datatables.fetch_brands');
    Route::get('/datatables/fetch_selected_brands', [DataTableController::class, 'fetch_selected_brands'])->name('dashboard.datatables.fetch_selected_brands');
    
    
    /*
    |--------------------------------------------------------------------------
    | BITLY
    |--------------------------------------------------------------------------
    */
    Route::get('/bitly', [BitlyController::class, 'index'])->name('dashboard.bitly.index');
    
    
    /*
    |--------------------------------------------------------------------------
    | VIMEO
    |--------------------------------------------------------------------------
    */
    Route::get('/vimeo', [VimeoController::class, 'index'])->name('dashboard.vimeo.index');
    Route::post('/vimeo/upload', [VimeoController::class, 'upload'])->name('dashboard.vimeo.upload');
    
    
    /*
    |--------------------------------------------------------------------------
    | EMAIL
    |--------------------------------------------------------------------------
    */
    Route::get('/email', [EmailController::class, 'index'])->name('dashboard.email.index');
    Route::get('/email/send', [EmailController::class, 'send'])->name('dashboard.email.send');
    
    
    /*
    |--------------------------------------------------------------------------
    | PDF
    |--------------------------------------------------------------------------
    */
    Route::get('/pdf', [PdfController::class, 'index'])->name('dashboard.pdf.index');
    Route::get('/pdf/download', [PdfController::class, 'download'])->name('dashboard.pdf.download');
    
    
    /*
    |--------------------------------------------------------------------------
    | EXPORT
    |--------------------------------------------------------------------------
    */
    Route::get('/export', [ExportController::class, 'index'])->name('dashboard.export.index');
    Route::get('/export/create', [ExportController::class, 'create'])->name('dashboard.export.create');
    
    
    /*
    |--------------------------------------------------------------------------
    | IMPORT
    |--------------------------------------------------------------------------
    */
    Route::get('/import', [ImportController::class, 'index'])->name('dashboard.import.index');
    Route::post('/import/create', [ImportController::class, 'create'])->name('dashboard.import.create');
    
    
    /*
    |--------------------------------------------------------------------------
    | DO SPACES
    |--------------------------------------------------------------------------
    */
    Route::get('/dospace', [DOSpacesController::class, 'index'])->name('dashboard.dospaces.index');
    Route::post('/dospace/upload', [DOSpacesController::class, 'upload'])->name('dashboard.dospaces.upload');

  });

}